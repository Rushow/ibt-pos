<?php

use Illuminate\Database\Seeder;
use App\DiscountAmountType;

class DiscountAmountTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $discountAmountType = array(
            'Amount ','Percentage'
        );

        foreach( $discountAmountType as $key => $value ){
            $discountAmountTypeObj = new DiscountAmountType();
            $discountAmountTypeObj->type_name = $value;
            $discountAmountTypeObj->save();
        }
    }
}
