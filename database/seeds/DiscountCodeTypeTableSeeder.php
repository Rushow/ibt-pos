<?php

use Illuminate\Database\Seeder;
use App\DiscountCodeType;

class DiscountCodeTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $discountCodeType = array(
            'One-user-single-time ','Multiple-user-single-time','Multiple-user-multiple-times'
        );

        foreach( $discountCodeType as $key => $value ){
            $discountTypeObj = new DiscountCodeType();
            $discountTypeObj->type_name = $value;
            $discountTypeObj->save();
        }
    }
}
