<?php

use App\Company;
use App\User;
use App\UserDetail;
use App\Module;
use App\Permission;
use App\Role;
use App\UserRole;
use App\Category;
use Illuminate\Database\Seeder;

class PosSetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = [
            'Home' => ['access-dashboard' => 'Access Dashboard', 'access-my-profile' => 'Access My Profile'],

            'Register( Cash Drawer )' => ['access-register-page' => 'Access Register Page', 'access-discount-at-register' => 'Access Discount at Register',
                'remove-tax-in-register' => 'Remove Tax in Register','allow-refund' => 'Allow Refund'],

            'Customer' => ['create-view-customer' => 'Create / View Customer', 'edit-update-customer' => 'Edit / Update Customer',
                'delete-customer' => 'Delete Customer', 'activate-deactivate-customer' => 'Activate / Deactivate Customer',
                'create-view-customer-notes' => 'Create / View Customer Notes', 'edit-update-customer-notes' => 'Edit / Update Customer Notes',
                'delete-customer-notes' => 'Delete Customer Notes', 'edit-credit-balance-reward' => 'Edit credit/balance/reward',
                'access-purchase-history' => 'Access Purchase History'],

            'Sync' => ['sync-challan' => 'Sync Challan', 'sync-sales-report' => 'Sync Sales Report', 'sync-all' => 'Sync All( Challan and Sales Report )'],

            'Company Settings' => ['access-company-info' => 'Access Company info', 'access-global-vat-settings' => 'Access Global VAT Settings',
                'access-global-payment-method-settings' => 'Access Global Payment Method Settings', 'create-view-outlets' => 'Create / View Outlets',
                'edit-update-outlets' => 'Edit / Update Outlets', 'delete-outlets' => 'Delete Outlets', 'activate-deactivate-outlets' => 'Activate / Deactivate Outlets'],

            'Users' => ['access-position-title' => 'Access Position / Title', 'add-edit-access-permission' => 'Add / Edit Access Permission( Create Permission Name Also
                            )', 'create-view-users' => 'Create / View Users', 'edit-update-user' => 'Edit / Update User',
                'delete-user' => 'Delete User', 'activate-deactivate-user' => 'Activate / Deactivate User'],
            'Customers' => ['create-view-customers' => 'Create / View Customers',],

            'Catalog' => ['access-category' => 'Access Category', 'create-view-products' => 'Create / View Products',
                'edit-update-products' => 'Edit / Update Products', 'delete-products' => 'Delete Products','access-product-prices' => 'Access Product Prices','access-product-attributes' =>'Access Product Attributes',
                'access-product-combination' => 'Access Product Combination'],

            'Inventory' => ['access-warehouse' => 'Access Warehouse','access-vendor-supplier' => 'Access Vendor / Supplier', 'access-warehouse-store-in' => 'Access Warehouse Store-in', 'access-warehouse-store-out' => 'Access Warehouse Store-out'],
        
            'Discount' => ['access-discount-module' => 'Access Discount'],
            'Task'=>['access-task-module'=>'Access Task'],
            'Invoice'=>['access-invoice-module'=>'Access Invoice'],
            'Inventory'=>['access-inventory-module'=>'Access Inventory'],
            'Sales Report'=>['access-sales-report-module'=>'Access Sales Report'],
            'API'=>['access-product-api'=>'Product API','Challan API'=>'access-challan-api','access-validatecoupon-api'=>'Coupon API','access-discount-rules-api
'=>'Discount Rules API',"access-invoice-api"=>'Invoice API','access-task-api'=>'Task API','sync-task-api'=>'Sync Task','access-inventory-api'=>'Inventory API','access-sales-report-api'=>'Sales Report API'],


        ];

        $companyObj = new Company();
        $companyObj->country_id = '23'; // Bangladesh
        $companyObj->currency_id = '9'; // Bangladeshi Taka ( BDT )
        $companyObj->time_zone_id = '327'; // Asia/Dhaka( (UTC+06:00) Dhaka )
        $companyObj->business_category_id = '1';
        $companyObj->name = 'Infrablue Tech';
        $companyObj->phone = '01729010683';
        $companyObj->email = 'sajib@bluebd.com';
        $companyObj->save();

        $userObj = new User();
        $userObj->company_id = '1';
        $userObj->name = 'admin';
        $userObj->email = 'sajib@bluebd.com';
        $userObj->password = '$2y$10$wtUlc2Rx5j3z8/e5moNOVu/gDjNxCtTvXchUD3d4fY9KF3E9PSlAC'; // pass = 123456
        $userObj->is_pos_admin = '1';
        $userObj->save();

        $userDetailObj = new UserDetail();
        $userDetailObj->user_id = '1';
        $userDetailObj->country_id = '23';
        $userDetailObj->first_name = 'Sajib';
        $userDetailObj->last_name = 'Ahasan';
        $userDetailObj->save();

        $role = new Role();
        $role->company_id = 1;
        $role->name = 'Company Administrator( Read Only )';
        $role->slug = 'administrator';
        $role->save();
        $role->users()->attach(1);

        $sequence = 1;
        $module_id = 1;
        foreach( $modules as $key => $module_access ){
            $modulesObj = new Module();
            $modulesObj->name = $key;
            $modulesObj->sequence = $sequence;
            $modulesObj->save();

            foreach( $module_access as $key => $value ){
                $permision = new Permission();
                $permision->module_id = $module_id;
                $permision->slug = $key;
                $permision->name = $value;
                $permision->save();
                $role->permissions()->attach($permision);
            }
            $sequence += 1;
            $module_id += 1;
        }
        $categoryObj = new Category();
        $categoryObj->company_id = '1';
        $categoryObj->parent_id = '0';
        $categoryObj->name = 'Home';
        $categoryObj->is_shop_default = '1';
        $categoryObj->level_depth = '0';
        $categoryObj->nleft = '0';
        $categoryObj->nright = '0';
        $categoryObj->active = '1';
        $categoryObj->position = '1';
        $categoryObj->is_root_category = '1';
        $categoryObj->save();
    }
}
