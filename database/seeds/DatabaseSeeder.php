<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('BusinessCategorySeeder');
        $this->call('CountrySeeder');
        $this->call('CurrencySeeder');
        $this->call('OutletTypeSeeder');
        $this->call('ScheduleTypeSeeder');
        $this->call('StatusSeeder');
        $this->call('TimeZoneSeeder');
//        $this->call('WarehouseStoreOutTypeSeeder');
        $this->call('PosSetupSeeder');
    }
}
