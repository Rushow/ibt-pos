<?php

use Illuminate\Database\Seeder;
use App\DiscountType;

class DiscountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $discountType = array(
            'General','By Coupon','By Day'
        );

        foreach( $discountType as $key => $value ){
            $discountTypeObj = new DiscountType();
            $discountTypeObj->discount_type_name = $value;
            $discountTypeObj->has_dependency=0;
            $discountTypeObj->save();
        }
    }
}
