<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['Draft','Pending','Approved','Active'];

        foreach($statuses as $status){
            $status_obj = new \App\Status();
            $status_obj->name = $status;
            $status_obj->save();
        }
    }
}
