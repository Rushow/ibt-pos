<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('sale_products', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('outlet_product_id')->unsigned();
           $table->integer('sale_id')->unsigned();
           $table->integer('product_discount_id')->unsigned();
           $table->integer('product_price_id')->unsigned();
           $table->integer('quantity');
           $table->decimal('unit_price',10,2);
           $table->decimal('vat_percent',10,2)->nullable();
           $table->softDeletes();
           $table->timestamps();

           $table->foreign('outlet_product_id')->references('id')->on('outlet_products');
           $table->foreign('sale_id')->references('id')->on('sales');
           $table->foreign('product_discount_id')->references('id')->on('product_discounts');
           $table->foreign('product_price_id')->references('id')->on('product_prices');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_products');
    }
}
