<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('out_let_id')->unsigned();
            $table->integer('sales_by')->unsigned()->comment('Sales Person');
            $table->integer('customer_id')->unsigned();
            $table->integer('discount_coupon_id')->unsigned()->nullable();
            $table->integer('invoice_prefix_id')->unsigned();
            $table->bigInteger('invoice_no');
            $table->decimal('price_exclusive_vat',10,2);
            $table->decimal('vat_amount',10,2)->nullable();
            $table->decimal('product_discount_amount',10,2)->nullable();
            $table->decimal('coupon_discount_amount',10,2)->nullable();
            $table->decimal('total_amount',10,2)->nullable();
            $table->string('credit_card_type',100)->nullable();
            $table->string('credit_card_number',100)->nullable();
            $table->decimal('card_received',10,2)->nullable();
            $table->decimal('cash_received',10,2)->nullable();
            $table->decimal('total_received',10,2)->nullable();
            $table->integer('is_sales_to_member')->default('0')->comment('1=Sales to member, 0=Sales to non-member');
            $table->text('sales_notes')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('out_let_id')->references('id')->on('out_lets')->onDelete('cascade');
            $table->foreign('sales_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->foreign('discount_coupon_id')->references('id')->on('discount_coupons')->onDelete('cascade');
            $table->foreign('invoice_prefix_id')->references('id')->on('invoice_prefixes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
