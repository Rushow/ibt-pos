<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('outlet_id')->unsigned();
            $table->integer('sales_by')->unsigned()->comment('Sales Person');
            $table->integer('customer_id')->unsigned()->nullable();
            $table->bigInteger('invoice_no');
            $table->decimal('product_price_exclusive_vat',10,2);
            $table->decimal('vat_amount',10,2)->nullable();
            $table->decimal('product_discount_amount',10,2)->nullable();
            $table->decimal('total_amount',10,2)->nullable();
            $table->string('credit_card_type',100)->nullable();//dbbl,brc...etc
            $table->string('credit_card_number',100)->nullable();
            $table->decimal('card_received',10,2)->nullable();
            $table->decimal('cash_received',10,2)->nullable();
            $table->decimal('total_received',10,2);
            $table->text('sales_notes')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('outlet_id')->references('id')->on('out_lets');
            $table->foreign('sales_by')->references('id')->on('users');
            $table->foreign('customer_id')->references('id')->on('customers');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
        


    }
}
