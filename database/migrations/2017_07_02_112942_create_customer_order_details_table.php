<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->integer('order_by')->unsigned()->comment('Sales Person');
            $table->integer('serial_no')->unique()->comment('Outlet wise serial no will be unique; each year it will start from 1');
            $table->date('order_date');
            $table->date('expected_delivery_date');
            $table->date('delivered_date');
            $table->decimal('unit_price', 10, 2)->comment('Per Bhori');
            $table->bigInteger('weight_rotti');
            $table->bigInteger('weight_milli')->nullable();
            $table->decimal('material_price');
            $table->decimal('making_charge');
            $table->integer('discount_coupon_id')->nullable();
            $table->integer('invoice_prefix_id')->unsigned();
            $table->bigInteger('invoice_no')->nullable()->comment('Sometimes Cash Memo No');
            $table->decimal('price_exclusive_vat',10,2);
            $table->decimal('vat_amount',10,2)->nullable();
            $table->decimal('price_inclusive_vat',10,2);
            $table->decimal('order_discount_amount',10,2)->nullable();
            $table->decimal('coupon_discount_amount',10,2)->nullable();
            $table->decimal('total_amount',10,2)->nullable();
            $table->text('order_details')->nullable();
            $table->string('status',20)->default('Not-assigned');
            $table->integer('is_sales_to_member')->default('0')->comment('1=Sales to member, 0=Sales to non-member');
            $table->softDeletes();
            $table->timestamps();

//            $table->string('credit_card_type',100)->nullable();
//            $table->string('credit_card_number',100)->nullable();
//            $table->decimal('card_received',10,2)->nullable();
//            $table->decimal('cash_received',10,2)->nullable();
//            $table->decimal('total_received',10,2)->nullable();

            $table->foreign('order_by')->references('id')->on('users');
            $table->foreign('customer_id')->references('id')->on('customers');
//            $table->foreign('discount_coupon_id')->references('id')->on('discount_coupon_id');
            $table->foreign('invoice_prefix_id')->references('id')->on('invoice_prefixes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_order_details');
    }
}
