<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->integer('parent_id')->default('0');
            $table->string('name', 250);
            $table->integer('is_shop_default')->default('0');
            $table->integer('level_depth');
            $table->integer('nleft')->nullable();
            $table->integer('nright')->nullable();
            $table->integer('active')->default('1');
            $table->integer('position');
            $table->integer('is_root_category')->default('0');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
