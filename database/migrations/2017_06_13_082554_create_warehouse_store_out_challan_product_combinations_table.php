<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseStoreOutChallanProductCombinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_store_out_challan_product_combinations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_store_out_challan_product_id')->unsigned();
            $table->integer('product_combination_id')->unsigned();
            $table->integer('outlet_id')->unsigned();
            $table->integer('quantity');
            $table->timestamps();
            $table->softDeletes();
            //$table->foreign('outlet_id')->references('id')->on('out_lets')->onDelete('cascade');
            $table->foreign('warehouse_store_out_challan_product_id','fk_warehouse_store_out_challan_product_id')->references('id')->on('warehouse_store_out_challan_products')->onDelete('cascade');
            $table->foreign('product_combination_id','fk_store_out_product_combination_id')->references('id')->on('product_combination_attributes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_store_out_challan_product_combinations');
    }
}
