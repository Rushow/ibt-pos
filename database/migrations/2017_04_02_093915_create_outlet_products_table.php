<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutletProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('outlet_products', function (Blueprint $table) {
////            $table->increments('id');
////            $table->integer('company_id')->unsigned();
////            $table->integer('outlet_challanin_id')->unsigned();
////            $table->integer('product_attribute_combination_id')->unsigned();
////            $table->integer('product_id')->unsigned();
////            $table->integer('outlet_id')->unsigned();
////            $table->bigInteger('quantity');
////            $table->bigInteger('quantity_left');
////            $table->softDeletes();
////            $table->timestamps();
////
////            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
////            $table->foreign('outlet_challanin_id')->references('id')->on('outlet_challanins')->onDelete('cascade');
////            $table->foreign('product_attribute_combination_id')->references('id')->on('product_attribute_combinations')->onDelete('cascade');
////            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
////            $table->foreign('outlet_id')->references('id')->on('outlets')->onDelete('cascade');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outlet_products');
    }
}
