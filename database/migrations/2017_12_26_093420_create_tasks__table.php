<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name',100);
            $table->integer('task_type_id');
            $table->integer('outlet_id');
            $table->integer('warehouse_store_out_challan_id')->nullable();
            $table->integer('discount_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->tinyInteger('status');
            $table->softDeletes();
            $table->timestamps();

            //$table->foreign('outlet_id')->references('id')->on('out_lets');
            //$table->foreign('warehouse_store_out_challan_id')->references('id')->on('warehouse_store_out_challans');
            //$table->foreign('discount_id')->references('id')->on('discounts');
            //$table->foreign('product_id')->references('id')->on('products');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("tasks");
    }
}
