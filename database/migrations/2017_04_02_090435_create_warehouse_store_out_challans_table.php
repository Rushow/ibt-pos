<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseStoreOutChallansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_store_out_challans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned();
            $table->integer('outlet_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->integer('warehouse_store_out_type_id')->unsigned();
            $table->string('challan_no', 32)->unique();
            $table->string('status', 16);
            $table->integer('user_id')->unsigned();
            $table->date('request_date');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('warehouse_id')->references('id')->on('warehouses')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('outlet_id')->references('id')->on('out_lets')->onDelete('cascade');
            $table->foreign('warehouse_store_out_type_id')->references('id')->on('warehouse_store_out_types')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_store_out_challans');
    }
}
