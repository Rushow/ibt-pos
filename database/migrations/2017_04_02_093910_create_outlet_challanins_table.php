<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutletChallaninsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('outlet_challanins', function (Blueprint $table) {
//            $table->increments('id');
//            $table->integer('warehouse_store_out_challan_id')->unsigned();
//            $table->integer('user_id')->unsigned();
//            $table->integer('warehouse_id')->unsigned();
//            $table->integer('outlet_id')->unsigned();
//            $table->text('challan_no');
//            $table->time('in_date');
//            $table->string('status');
//            $table->softDeletes();
//            $table->timestamps();
//
//            $table->foreign('warehouse_store_out_challan_id')->references('id')->on('warehouse_storeout_challans')->onDelete('cascade');
//            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
//            $table->foreign('warehouse_id')->references('id')->on('warehouses')->onDelete('cascade');
//            $table->foreign('outlet_id')->references('id')->on('outlets')->onDelete('cascade');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outlet_challanins');
    }
}
