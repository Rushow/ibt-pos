<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseStoreInChallanProductCombinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_store_in_challan_product_combinations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_store_in_challan_product_id')->unsigned();
            $table->integer('product_combination_id')->unsigned();
            $table->integer('quantity');
            $table->softDeletes();
            $table->timestamps();
            //$table->foreign('warehouse_store_in_challan_product_id','fk_warehouse_store_in_challan_product_id')->references('id')->on('warehouse_store_in_challan_products')->onDelete('cascade');
            $table->foreign('product_combination_id','fk_product_combination_id')->references('id')->on('product_combinations')->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('warehouse_store_in_challan_product_combinations');
    }
}
