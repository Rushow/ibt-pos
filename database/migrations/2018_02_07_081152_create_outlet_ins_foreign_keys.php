<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutletInsForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('outlet_ins', function (Blueprint $table) {
            $table->foreign('outlet_id')->references('id')->on('out_lets')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('product_combination_id')->references('id')->on('product_combinations')->onDelete('cascade');
            $table->foreign('warehouse_store_out_challans_id')->references('id')->on('warehouse_store_out_challans')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outlet_ins', function (Blueprint $table) {
            //
        });
    }
}
