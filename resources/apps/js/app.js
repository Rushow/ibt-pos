
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component('barcode-generate', require('./components/barcode-generate.vue'));

Vue.component('warehouse-store-in-challan-edit', require('./components/warehouse_store_in_challan_edit.vue'));
Vue.component('warehouse-store-in-challan-create', require('./components/warehouse_store_in_challan_create.vue'));

Vue.component('warehouse-store-out-challan-edit', require('./components/warehouse_store_out_challan_edit.vue'));
Vue.component('warehouse-store-out-challan-create', require('./components/warehouse_store_out_challan_create.vue'));


const app = new Vue({
    el: '#app',
});