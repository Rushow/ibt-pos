$(document).ready(function() {
    var table = $('#tableWithSearch').DataTable( {
        dom: 'Bfrtip',
        buttons: [
             'csv', 'excel', 'pdf', 'print'
        ]
    } );
    if(table){
        table.buttons().container()
            .appendTo( '#tableWithSearch_wrapper .col-sm-6:eq(0)' );
    }


} );


