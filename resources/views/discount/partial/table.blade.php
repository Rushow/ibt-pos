{{ Form::open(['url' => 'discount/remove_discount', 'method' => 'DELETE']) }}
<button class="btn re-confirm"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">No</th>
        <th style="width:20%">Discount Name</th>
        <th style="width:20%">Amount Type</th>
        <th style="width:20%">Start Date</th>
        <th style="width:20%">End Date</th>
        <th style="width:20%">Discount Type</th>
        <th style="width:20%">Status</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $discounts as $discount )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input class="store_in_challan_ids" type="checkbox" name="discount_ids[{{$discount->id}}]" value={{$discount->id}} data-id="checkbox" id="checkbox{{$discount->id}}">
                    <label for="checkbox{{$discount->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                {{ $discount->id }}
            </td>
            <td class="v-align-middle">
                {{ $discount->discount_name }}
            </td>
            <td class="v-align-middle">
                {{$discount->DiscountAmountType->type_name}}
            </td>
            <td class="v-align-middle">
                {{ $discount->start_date }}
            </td>
            <td class="v-align-middle">
                {{ $discount->end_date }}
            </td>
            <td class="v-align-middle">
                {{ $discount->DiscountType->discount_type_name }}
            </td>
            <td class="v-align-middle">
                @if($discount->active==1)
                    <p>On</p>
                @else
                    <p>Off</p>
                @endif
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn btn-success" href="{{url('discount/'.$discount->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" param-first="{{$discount->id}}" param-second="" type="button" class="btn btn-danger remove-item"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ Form::close() }}

{{ Form::open(['url' => '', 'method' => 'DELETE', 'class'=>'hide','id'=>'single-item-delete','action-url-first'=>url('discount')]) }}

{{ Form::close() }}
