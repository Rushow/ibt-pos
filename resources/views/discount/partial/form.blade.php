<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Discount </h4>
        </div>
        <div class="panel-body">
            <div class="col-md-6 col-product-left-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('name', 'Discount Name') }}
                            {{ Form::text('discount_name', isset($discount->discount_name) ? $discount->discount_name : '', ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('discount', 'Discount') }}
                            {{ Form::text('discount', isset($discount->discount) ? $discount->discount : '', ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('amount_type', 'Amount Type') }}
                            {{ Form::select('amount_type', $amount_types,isset($discount->amount_type) ? $discount->amount_type : '1', ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('start_date', 'Start Date') }}
                            {{ Form::text('start_date', isset($discount->start_date) ? $discount->start_date : '', ['class' => 'form-control date', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('end_date', 'End Date') }}
                            {{ Form::text('end_date', isset($discount->end_date) ? $discount->end_date : '', ['class' => 'form-control date', 'required']) }}
                        </div>
                        <div class="form-group form-group-default form-group-default-select2 required">
                            {{ Form::label('outlets', 'Pick Outlet') }}
                            <select class="full-width" id="outlets" required multiple="multiple" data-init-plugin="select2" name="outlets[]">
                                @foreach($outlets as $key => $outlet)
                                    @foreach($discount->DiscountTypeConfigurationOutlet as $discountedOutlet)
                                        @if($key == $discountedOutlet->outlet_id)
                                            <option value="{{ $key }}" selected>{{ $outlet }}</option>
                                        @else
                                            <option value="{{ $key }}">{{ $outlet }}</option>
                                        @endif
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-product-right-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default form-group-default-select2 required">
                            {{ Form::label('categories', 'Pick Categories') }}
                            <select class="full-width" id="categories" required multiple="multiple" data-init-plugin="select2" name="categories[]">
                                @foreach($categories as $key => $category)
                                    @foreach($discount->DiscountTypeConfigurationCategory as $discountedCategory)
                                        @if($key == $discountedCategory->category_id)
                                            <option value="{{ $key }}" selected>{{ $category }}</option>
                                        @else
                                            <option value="{{ $key }}">{{ $category }}</option>
                                        @endif
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('discount_type_id', 'Discount Type') }}
                            {{ Form::select('discount_type_id',$discount_types, isset($discount->discount_type_id) ? $discount->discount_type_id : null, ['class' => 'form-control discount_type_id', 'required']) }}
                        </div>
                        <div class="form-group form-group-default  required">
                            {{ Form::label('discount_day_id', 'Discount Day') }}
                            {{ Form::select('discount_day_id[]',$day_lists, isset($discount->DiscountTypeConfigurationDay) ? $discount->DiscountTypeConfigurationDay->pluck('discount_day_id') : null, ['class' => 'form-control', 'required','placeholder'=>'Select', 'multiple' => 'multiple',"data-init-plugin"=>"select2","id"=>'discount_type_id']) }}
                        </div>

                        <div class="form-group form-group-default required">
                            {{ Form::label('active', 'Status') }}
                            {{ Form::checkbox('active', isset($discount->active) ? $discount->active : 0, isset($discount->active) ? $discount->active : 0, ['class' => 'attributes-checkbox','data-init-plugin="switchery"']) }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <button class="btn btn-success btn-cons pull-right" type="submit">
        <span>Save</span>
    </button>
</div>
@section('page-script')
   <script>
//       $('select.discount_type_id').on('change', function(){
//
//           alert("h"+$(this).val());
//       });
    </script>
@endsection

