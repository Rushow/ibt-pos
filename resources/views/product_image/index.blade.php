@extends('layouts.master')
@section('style')
        <link href="{{asset('theme/backend/assets/plugins/dropzone/css/dropzone.css')}}" rel="stylesheet" type="text/css" media="screen" />
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <!-- START WIDGET -->
            <div class="row-xs-height">
                <div class="col-xs-height">
                    <div class="p-l-20 p-r-20">
                        <div class="row">
                            <div class="col-lg-3 visible-xlg">
                                <div class="widget-14-chart-legend bg-transparent text-black no-padding pull-right"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-xs-height">
                <div class="col-xs-height relative bg-master-lightest">
                    <div class="widget-14-chart_y_axis"></div>
                    <div class="widget-14-chart rickshaw-chart top-left top-right bottom-left bottom-right"></div>
                </div>
            </div>
            <!-- END WIDGET -->
        </div>

        <div id="rootwizard" class="m-t-50">

        <!-- Success Messages -->
            @include('partial/success_message')
        <!-- END Success Messages -->
        <!-- Error Messages -->
            @include('partial/error_message')
        <!-- END Error Messages -->
            <!-- START CONTAINER FLUID -->
            <div id="rootwizard" class="m-t-50">
                <!-- Nav tabs -->
                    @include('product.partial.product-nav')
                <!-- Tab panes -->
                <div class="tab-content">
                    <div>
                        <div class="row row-same-height">
                            <div class="panel-body">
                                <!-- START PANEL -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            Product Photos
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;"></a>
                                            <a class="config" data-toggle="modal" href="#grid-config"></a>
                                            <a class="reload" href="javascript:;"></a>
                                            <a class="remove" href="javascript:;"></a>
                                        </div>
                                    </div>
                                    <div class="panel-body no-scroll no-padding">
                                        {!! Form::open([ 'url' => 'product/'.$product->id.'/product_image', 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'image-upload','method' => 'post' ]) !!}
                                            @include('product.partial.photo-upload-form')
                                        {{ Form::close() }}
                                    </div>
                                </div>
                                <!-- END PANEL -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTAINER FLUID -->
        </div>
    </div>
@endsection

@section('plugin-script')
        <script src="{{asset('theme/backend/assets/plugins/dropzone/dropzone.min.js')}}" type="text/javascript"></script>
        <script type="text/javascript">
            Dropzone.options.imageUpload = {
                maxFiles: 20,
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                addRemoveLinks: true,
                dictRemoveFile: 'Remove',
                uploadMultiple: true,
                createImageThumbnails: true,

                init:function(){
                    var self = this;
                    // config
                    self.options.addRemoveLinks = true;
                    self.options.dictRemoveFile = "Remove";
                    //New file added
                    self.on("addedfile", function (file, response ) {
                        console.log('new file added ', response);
                    });
                    // Send file starts
                    self.on("sending", function (file) {
                        console.log('upload started', file);
                        $('.meter').show();
                    });

                    // File upload Progress
                    self.on("totaluploadprogress", function (progress) {
                        console.log("progress ", progress);
                        $('.roller').width(progress + '%');
                    });

                    self.on("queuecomplete", function (progress) {
                        $('.meter').delay(999).slideUp(999);
                    });

                    // On removing file
                    self.on("removedfile", function (file) {
                        console.log("Removed file : "+file.name);
                        $.ajax({
                            url: '/api/product/remove_image',
                            data: { image_name : file.name } ,
                            type: "GET",
                            async: false,
                            dataType: "json",
                            success:function(response) {
                                alert(response.error);
                            }
                        });
                    });
                }
            };
        </script>

@endsection



