<ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm">

    @if( $product )
        <li @if( $tab_active == 'product' ) class="active" @endif >
            <a href="{{url('product/'.$product->id.'/edit')}}"><i class="fa tab-icon"></i> <span>Your Product</span></a>
        </li>
        <li @if( $tab_active == 'media' ) class="active" @endif>
            <a href="{{url('product/'.$product->id.'/product_image')}}"><i class="fa tab-icon"></i> <span>Media</span></a>
        </li>
        @can('access-product-prices')
            <li @if( $tab_active == 'price' ) class="active" @endif>
                <a href="{{url('product/'.$product->id.'/product_price')}}"><i class="fa tab-icon"></i> <span>Product Price</span></a>
            </li>
        @endcan
        @can('access-product-prices')
        <li @if( $tab_active == 'discount' ) class="active" @endif>
            <a href="{{url('product/'.$product->id.'/product_discount_price')}}"><i class="fa tab-icon"></i> <span>Product Discount Price</span></a>
        </li>
        @endcan
        @can('access-product-combination')
            <li @if( $tab_active == 'combination' ) class="active" @endif>
                <a href="{{url('product/'.$product->id.'/product_combination')}}"><i class="fa tab-icon"></i> <span>Product Combination</span></a>
            </li>
        @endcan
    @else
        <li @if( $tab_active == 'product' ) class="active" @endif >
            <a href="{{url('product/create')}}"><i class="fa tab-icon"></i> <span>Your Product</span></a>
        </li>

    @endif
</ul>