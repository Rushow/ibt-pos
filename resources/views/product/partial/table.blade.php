{{ Form::open(['url' => 'product/remove_products', 'method' => 'DELETE']) }}
<button class="btn re-confirm"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Image</th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Item Code</th>
        <th style="width:20%">Bar Code</th>
        <th style="width:20%">Category</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach( $products as $product )
            <tr>
                <td class="v-align-middle">
                    <div class="checkbox ">
                        <input type="checkbox" name="product_ids[]" value={{ $product->id }} data-id="checkbox" id="checkbox{{ $product->id }}">
                        <label for="checkbox{{ $product->id }}"></label>
                    </div>
                </td>
                <td class="v-align-middle">
                    {{ $product->name }}
                </td>
                <td class="v-align-middle">
                    {{ $product->name }}
                </td><td class="v-align-middle">
                    {{ $product->item_code }}
                </td><td class="v-align-middle">
                    @if( $product->categories )
                        @foreach( $product->combinations as $combination)
                            {{$combination->barcode}},
                        @endforeach
                    @endif
                </td>
                <td class="v-align-middle">
                    <ul>
                        @if( $product->categories )
                            @foreach( $product->categories as $categorie )
                                <li> {{ $categorie->name  }} </li>
                            @endforeach
                        @endif
                    </ul>
                </td>
                <td class="v-align-middle">
                    <div class="btn-group btn-actions">
                        <a title="Edit" class="btn btn-success" href="{{url('product/'.$product->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                        <button title="Delete" param-first="{{$product->id}}" param-second="" type="button" class="btn btn-danger remove-item"><i class="fa fa-trash-o"></i></button>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
{{ Form::close() }}

{{ Form::open(['url' => '', 'method' => 'DELETE', 'class'=>'hide','id'=>'single-item-delete','action-url-first'=>url('product')]) }}

{{ Form::close() }}


