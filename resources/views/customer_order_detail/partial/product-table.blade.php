<div class="row row-same-height">
    <!-- START PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Products</h4>
            <div class="pull-right">
                {{--<div class="col-xs-12">--}}
                    {{--<input type="text" id="search-table" class="form-control pull-right" placeholder="Search">--}}
                {{--</div>--}}
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <table class="table table-hover" id="tableWithSearch">
                <thead>
                <tr>
                    <th style="width:20%">Image</th>
                    <th style="width:20%">Name</th>
                    <th style="width:20%">Item Code</th>
                    <th style="width:20%">Bar Code</th>
                    <th style="width:20%">Category</th>
                    <th style="width:20%">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach( $products as $product )
                    <tr>
                        <td class="v-align-middle">
                        {{ $product->name }}
                        </td>
                        <td class="v-align-middle">
                            {{ $product->name }}
                        </td><td class="v-align-middle">
                            {{ $product->item_code }}
                        </td><td class="v-align-middle">
                            {{ $product->bar_code }}
                        </td><td class="v-align-middle">
                            Women
                        </td>
                        <td class="v-align-middle">
                            <div class="btn-group btn-actions">
                                <a title="Add-to-challan" class="btn btn-success product_add_btn" id="{{$product->id}}">Add To Challan</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END PANEL -->
</div>