@extends('layouts.master')
@section('style')
    <link href="{{asset('backend_old')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('theme/backend/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{asset('theme/backend/assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet"
          type="text/css" media="screen"/>
@stop
@section('content')

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <!-- START WIDGET -->
            <div class="row-xs-height">
                <div class="col-xs-height">
                    <div class="p-l-20 p-r-20">
                        <div class="row">
                            <div class="col-lg-3 visible-xlg">
                                <div class="widget-14-chart-legend bg-transparent text-black no-padding pull-right"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-xs-height">
                <div class="col-xs-height relative bg-master-lightest">
                    <div class="widget-14-chart_y_axis"></div>
                    <div class="widget-14-chart rickshaw-chart top-left top-right bottom-left bottom-right"></div>
                </div>
            </div>
            <!-- END WIDGET -->
        </div>
        <div id="rootwizard" class="m-t-50">
            <!-- Success Messages -->
        @include('partial/success_message')
        <!-- END Success Messages -->
            <!-- Error Messages Section-->
        @include('partial/error_message')
        <!-- END Error Messages -->
            <!-- START CONTAINER FLUID -->
            <div class="container-fluid container-fixed-lg bg-white">
                <!-- START PANEL -->
                <div class="panel panel-transparent">
                    <div class="panel-heading">
                        <div class="panel-title panel-title-margin">Customers</div><br/>
                        <div class="pull-left">
                            <a href="{{ url('customer/create') }}">
                                <button id="show-modal" class="btn btn-success btn-cons"><i class="fa fa-plus"></i> Create Customer</button>
                            </a>
                        </div>
                        <div class="pull-right">
                            <div class="col-xs-12">
                                <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        @include('customer.partial.table')
                    </div>
                </div>
                <!-- END PANEL -->
            </div>
            <!-- END CONTAINER FLUID -->
        </div>
    </div>
@endsection

@section('plugin-script')
    <script src="{{asset('theme/backend/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"
            type="text/javascript"></script>
    <script src="{{asset('theme/backend/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"
            type="text/javascript"></script>
    <script src="{{asset('backend_old') }}"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="{{asset('theme/backend/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{asset('theme/backend/assets/plugins/datatables-responsive/js/lodash.min.js') }}"></script>
@endsection

@section('page-script')
    <script src="{{asset('backend_old')}}" type="text/javascript"></script>
@endsection

