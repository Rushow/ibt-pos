{{ Form::open(['route' =>  ['category.destroy', '1'], 'method' => 'post', 'id' => 'outletDelete']) }}
<button class="btn" onclick="return confirm('Are you sure you want to delete this item?');"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">ID</th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Position</th>
        <th style="width:20%">Displayed</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>

    @foreach( $categories as $category )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input  @if( $category->parent_id == 0 ) disabled @endif type="checkbox" name="category_ids[]" value={{$category->id}} data-id="checkbox" id="checkbox{{$category->id}}">
                    <label for="checkbox{{$category->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                <p>{{$category->id}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$category->name}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$category->position}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$category->active}}</p>
            </td>
            <td class="v-align-middle">
                    <div class="btn-group btn-actions">
                        <button @if( $category->parent_id == 0 ) disabled @endif onclick="location.href='{{ route('category.edit', $category->id) }}'" type="button" class="btn btn-success"><i class="fa fa-pencil"></i></button>
                        {{ Form::open(['route' => ['category.destroy', $category->id], 'method' => 'post']) }}
                            <button @if( $category->parent_id == 0 ) disabled @endif type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash-o"></i></button>
                            @if( $category->parent_id != 0 )
                                {{ method_field('DELETE') }}
                                {{ Form::hidden('category_ids[]', $category->id) }}
                            @endif
                        {{ Form::close() }}
                    </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ method_field('DELETE') }}
{{ Form::close() }}