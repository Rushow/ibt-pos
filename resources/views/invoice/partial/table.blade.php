{{ Form::open(['url' => 'invoice/remove_invoice', 'method' => 'DELETE']) }}
<button class="btn re-confirm"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Invoice No</th>
        <th style="width:20%">Outlet Name</th>
        <th style="width:20%">Product List</th>
        <th style="width:20%">Combination</th>
        <th style="width:20%">Price</th>
        <th style="width:20%">Discount</th>
        <th style="width:20%">Vat</th>
        <th style="width:20%">Total Amount</th>
        <th style="width:20%">Salesman</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $invoices as $invoice )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input class="store_in_challan_ids" type="checkbox" name="invoice_ids[{{$invoice->id}}]" value={{$invoice->id}} data-id="checkbox" id="checkbox{{$invoice->id}}">
                    <label for="checkbox{{$invoice->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                {{$invoice->invoice_no}}
            </td>
            <td class="v-align-middle">
                {{$invoice->Outlet->name}}
            </td>
            <td class="v-align-middle">
                @foreach($invoice->InvoiceProduct as $k=>$product)
                    @if($k>0) , @endif{{$product->Product->name}}
                @endforeach
            </td>
            <td class="v-align-middle">
                @foreach($invoice->InvoiceProduct as $k=>$productCombo)
                    @if($k>0) , @endif{{$productCombo->ProductCombination->name}}
                @endforeach
            </td>
            <td class="v-align-middle">
                {{$invoice->product_price_exclusive_vat}}
            </td>
            <td class="v-align-middle">
                {{$invoice->product_discount_amount}}
            </td>

            <td class="v-align-middle">
                {{$invoice->vat}}
            </td>
            <td class="v-align-middle">
                {{$invoice->total_amount}}
            </td>
            <td class="v-align-middle">
                {{$invoice->User->email}}
            </td>

            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <button title="Delete" param-first="{{$invoice->id}}" param-second="" type="button" class="btn btn-danger remove-item"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ Form::close() }}

{{ Form::open(['url' => '', 'method' => 'DELETE', 'class'=>'hide','id'=>'single-item-delete','action-url-first'=>url('invoice')]) }}

{{ Form::close() }}
