
<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Product Discount</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('discount_amount', 'Discount Amount') }}
                            {{ Form::text('discount_amount',isset($product_discount) ? $product_discount->discount_amount :null,['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required form-group-default-selectFx">
                            {{ Form::label('discount_type', 'Type') }}
                            {{Form::select('discount_type',$amount_lists,null,['class' => 'cs-select cs-skin-slide cs-transparent form-control attribute-group-select','data-init-plugin' => 'cs-select']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('active_date', 'Active date') }}
                            {{ Form::date('active_date',isset($product_discount->active_date) ?  date('d-m-Y',strtotime($product_discount->expire_date)):null, ['class' => 'form-control', 'required']) }}
                        </div>

                        <div class="form-group form-group-default required">
                            {{ Form::label('expire_date', 'Expire Date') }}
                            {{ Form::date('expire_date', isset($product_discount->expire_date) ?  date('d-m-Y',strtotime($product_discount->expire_date)):null, ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>
                    <div class="form-group form-group-default required form-group-default-selectFx">
                        {{ Form::label('status', 'Active') }}
                        {{Form::select('status',$status_lists,null,['class' => 'cs-select cs-skin-slide cs-transparent form-control attribute-group-select','data-init-plugin' => 'cs-select']) }}
                    </div>
                    <div class="form-group-attached pull-right padding-top">
                        {{ form::hidden('product_price_id', isset($product_discount) ? $product_discount->id : '', ['id' => 'product_price_id']) }}
                        <button class="btn btn-success btn-cons" type="submit">
                            <span>Save</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('plugin-script')

@endsection