{{ Form::open(['url' => 'product/'.$product->id.'/product_discount_price/remove_product_prices', 'method' => 'DELETE']) }}
<button class="btn re-confirm"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Discount Amount</th>
        <th style="width:20%">Amount Type</th>
        <th style="width:20%">Active Date</th>
        <th style="width:20%">Expire Date</th>
        <th style="width:20%">Status</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @if($product->discount)
        @foreach( $product->discount as $value)
            <tr>
                <td class="v-align-middle">
                    <div class="checkbox ">
                        <input type="checkbox" name="product_discount_ids[]" value={{ $value->id }} data-id="checkbox" id="checkbox{{ $value->id }}">
                        <label for="checkbox{{ $value->id }}"></label>
                    </div>
                </td>
                <td class="v-align-middle">
                    {{ $value->discount_amount }}
                </td>
                <td class="v-align-middle">
                    {{ $value->DiscountAmountType->type_name }}
                </td>
                <td class="v-align-middle">
                    {{ date('F d, Y', strtotime($value->active_date)) }}
                </td>
                <td class="v-align-middle">
                    {{ date('F d, Y', strtotime($value->expire_date)) }}
                </td>
                <td class="v-align-middle">
                    {{ $value->status ? 'Active' : 'Inactive'}}
                </td>

                <td class="v-align-middle">
                    <div class="btn-group btn-actions">
                        <a title="Edit" class="btn btn-success" href="{{url('product/'.$product->id.'/product_discount_price/'.$value->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                        <button title="Delete" param-first="{{$product->id}}" param-second="{{$value->id}}" type="button" class="btn btn-danger remove-item"><i class="fa fa-trash-o"></i></button>
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
{{ Form::close() }}

{{ Form::open(['url' => '', 'method' => 'DELETE', 'class'=>'hide','id'=>'single-item-delete','action-url-first'=>url('product'),'action-url-second'=>'product_discount_price']) }}

{{ Form::close() }}


