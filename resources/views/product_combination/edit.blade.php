@extends('layouts.master')
@section('style')
    <link href="{{asset('backend_old')}}" rel="stylesheet" type="text/css" media="screen" />
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <!-- START WIDGET -->
            <div class="row-xs-height">
                <div class="col-xs-height">
                    <div class="p-l-20 p-r-20">
                        <div class="row">
                            <div class="col-lg-3 visible-xlg">
                                <div class="widget-14-chart-legend bg-transparent text-black no-padding pull-right"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-xs-height">
                <div class="col-xs-height relative bg-master-lightest">
                    <div class="widget-14-chart_y_axis"></div>
                    <div class="widget-14-chart rickshaw-chart top-left top-right bottom-left bottom-right"></div>
                </div>
            </div>
            <!-- END WIDGET -->
        </div>

        <div id="rootwizard" class="m-t-50">
            <!-- Success Messages -->
        @include('partial/success_message')
        <!-- END Success Messages -->
            <!-- Error Messages -->
        @include('partial/error_message')
        <!-- END Error Messages -->
        @include('product.partial.product-nav')
            <!-- START CONTAINER FLUID -->
            <div id="rootwizard" class="m-t-50">
                <!-- Nav tabs -->
            @include('product_combination.partial.form')
            <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane padding-20 active slide-left" id="tab1">
                        {{ Form::model($product_combination, [ 'url' => 'product/'.$product->id.'/product_price/'.$product_combination->id,'method' => 'put', 'class' => 'dropzone no-margin', 'id' => 'form-project']) }}
                        {{--{{ csrf_field() }}--}}
                        @include('product_combination.partial.form')
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            <!-- END CONTAINER FLUID -->
        </div>
    </div>
@endsection

@section('plugin-script')
    <script src="{{asset('theme/backend/assets/plugins/jquery-dynatree/jquery.dynatree.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend_old')}}" type="text/javascript"></script>
@endsection



