{{ Form::open(['url' => 'product_combination/remove_product_combinations', 'method' => 'DELETE']) }}
<button class="btn re-confirm"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Product Name</th>
        <th style="width:20%">Combination Name</th>
        <th style="width:20%">Values</th>
        <th style="width:20%">Barcode</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @if($product->combinations)
        @foreach( $product->combinations as $combination)
            <tr>
                <td class="v-align-middle">
                    <div class="checkbox ">
                        <input class="product_combination_ids" type="checkbox" name="product_combination_ids[{{$combination->id}}]" value={{$combination->id}} data-id="checkbox" id="checkbox{{$combination->id}}">
                        <label for="checkbox{{$combination->id}}"></label>
                    </div>
                </td>
                <td class="v-align-middle">
                    {{ $combination->product->name }}
                </td>
                <td class="v-align-middle">
                    {{ $combination->name }}
                </td>
                <td class="v-align-middle">
                    @php
                        $total = count($combination->combinationAttributeValues);
                        $i= 1;
                    @endphp
                    @foreach( $combination->combinationAttributeValues as $attribute)
                        {{ $attribute->attributeValue->value }}
                        {{ ($i < $total)? ', ':'' }}
                        @php
                            $i++;
                        @endphp
                    @endforeach
                </td>
                <td class="v-align-middle">
                    {{ $combination->barcode }}
                </td>
                <td class="v-align-middle">
                    <div class="btn-group btn-actions">
                        <a title="Edit" class="btn btn-success" href="{{url('product/'.$product->id.'/product_combination/'.$combination->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                        <button title="Delete" param-first="{{$product->id}}" param-second="{{$combination->id}}" type="button" class="btn btn-danger remove-item"><i class="fa fa-trash-o"></i></button>
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
{{ Form::close() }}

{{ Form::open(['url' => '', 'method' => 'DELETE', 'class'=>'hide','id'=>'single-item-delete','action-url-first'=>url('attribute')]) }}

{{ Form::close() }}
