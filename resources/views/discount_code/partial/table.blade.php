{{ Form::open(['url' => 'discountcode/remove_discount_code', 'method' => 'DELETE']) }}
<button class="btn re-confirm"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">No</th>
        <th style="width:20%">Discount Name</th>
        <th style="width:20%">Code</th>
        <th style="width:20%">Code Type</th>
        <th style="width:20%">Status</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $discountcodes as $discountcode )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input class="store_in_challan_ids" type="checkbox" name="discountcode_ids[{{$discountcode->id}}]" value={{$discountcode->id}} data-id="checkbox" id="checkbox{{$discountcode->id}}">
                    <label for="checkbox{{$discountcode->id}}"></label>
                </div>
            </td>
            <td>{{$discountcode->id}}</td>
            <td class="v-align-middle">
                {{ $discountcode->Discount->discount_name }}
            </td>
            <td class="v-align-middle">
                {{ $discountcode->code }}
            </td>
            <td class="v-align-middle">
                {{$discountcode->DiscountCodeType->type_name}}
            </td>

            <td class="v-align-middle">
                @if($discountcode->active==1)
                    <p>On</p>
                @else
                    <p>Off</p>
                @endif
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn btn-success" href="{{url('discountcode/'.$discountcode->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" param-first="{{$discountcode->id}}" param-second="" type="button" class="btn btn-danger remove-item"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ Form::close() }}

{{ Form::open(['url' => '', 'method' => 'DELETE', 'class'=>'hide','id'=>'single-item-delete','action-url-first'=>url('discountcode')]) }}

{{ Form::close() }}
