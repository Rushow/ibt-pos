
<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Discount Code</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-6 col-product-left-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('discount_id', 'Discount Name') }}
                            {{ Form::select('discount_id',$discount ,isset($discountcode->discount_id) ? $discountcode->discount_id : '', ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('code', 'Discount Code') }}
                            {{ Form::text('code', isset($discountcode->code) ? $discountcode->code : '', ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('discount_code_type_id', 'Discount Code Type') }}
                            {{ Form::select('discount_code_type_id',$discount_code_types,isset($discountcode->discount_code_type_id) ? $discountcode->discount_code_type_id : '1', ['class' => 'form-control', 'required']) }}
                        </div>

                        <div class="form-group form-group-default required">
                            {{ Form::label('active', 'Status') }}
                            {{ Form::checkbox('active', 0, isset($discountcode->active) ? $discountcode->active : '0', ['class' => 'attributes-checkbox','data-init-plugin="switchery"']) }}
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>

    <button class="btn btn-success btn-cons pull-right" type="submit">
        <span>Save</span>
    </button>
</div>
