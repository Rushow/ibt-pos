{{ Form::open(['route' => ['payment_method.destroy', '1'], 'method' => 'post']) }}
<button class="btn" onclick="return confirm('Are you sure you want to delete this item?');"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Last Updated</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>

    @foreach( $payment_methods as $payment_method )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input type="checkbox" name="payment_method_ids[]" value={{$payment_method->id}} data-id="checkbox" id="checkbox{{$payment_method->id}}">
                    <label for="checkbox{{$payment_method->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                <p>{{ $payment_method->name }} </p>
            </td>
            <td class="v-align-middle">
                <p>{{ date('F d, Y H:i:s', strtotime($payment_method->updated_at)) }}</p>
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <button onclick="location.href='{{ route('payment_method.edit', $payment_method->id) }}'" type="button" class="btn btn-success"><i class="fa fa-pencil"></i></button>
                    {{ Form::open(['route' => ['payment_method.destroy', $payment_method->id], 'method' => 'post']) }}
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash-o"></i></button>
                        {{ method_field('DELETE') }}
                        {{ Form::hidden('payment_method_ids[]', $payment_method->id) }}
                    {{ Form::close() }}
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ method_field('DELETE') }}
{{ Form::close() }}