{{ Form::open(['url' => 'customer/remove_customers', 'method' => 'DELETE']) }}
<button class="btn re-confirm"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Mobile</th>
        {{--<th style="width:20%">Work Phone</th>--}}
        <th style="width:20%">Email</th>
        <th style="width:20%">Gender</th>
        <th style="width:20%">Address</th>
        <th style="width:20%">City</th>
        <th style="width:20%">Zip</th>
        {{--<th style="width:20%">Address2</th>--}}
        <th style="width:20%">Outlet</th>

    </tr>
    </thead>
    <tbody>
    @foreach( $customers as $customer )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input class="store_in_challan_ids" type="checkbox" name="store_in_challan_ids[{{$customer->id}}]" value={{$customer->id}} data-id="checkbox" id="checkbox{{$customer->id}}">
                    <label for="checkbox{{$customer->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                {{ $customer ? $customer->first_name.' '. $customer->last_name : ''}}
            </td>
            <td class="v-align-middle">
                {{ $customer->cell_phone ? $customer->cell_phone:null }}
            </td>
            {{--<td class="v-align-middle">--}}
                {{--{{ $customer->work_phone ? $customer->work_phone:null }}--}}
            {{--</td>--}}
            <td class="v-align-middle">
                {{ $customer->email ? $customer->email:null }}
            </td>
            <td class="v-align-middle">
                {{ $customer->gender ? $customer->gender:null }}
            </td>
            <td class="v-align-middle">
                {{ $customer->address ? $customer->address:null }}
            </td>
            <td class="v-align-middle">
                {{ $customer->city ? $customer->city:null }}
            </td>
            <td class="v-align-middle">
                {{ $customer->zip ? $customer->zip:null }}
            </td>
            {{--<td class="v-align-middle">--}}
                {{--{{ $customer->address2 ? $customer->address2:null }}--}}
            {{--</td>--}}
            <td class="v-align-middle">
                {{ App\OutLet::find($customer->out_let_id)->name }}
            </td>

        </tr>
    @endforeach
    </tbody>
</table>
{{ Form::close() }}

{{ Form::open(['url' => '', 'method' => 'DELETE', 'class'=>'hide','id'=>'single-item-delete','action-url-first'=>url('store_in_challan')]) }}

{{ Form::close() }}
