@extends('layouts.master')
@section('style')
    <link href="{{asset('backend_old')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('theme/backend/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{asset('theme/backend/assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet"
          type="text/css" media="screen"/>
@stop
@section('content')
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <!-- START WIDGET -->
            <div class="row-xs-height">
                <div class="col-xs-height">
                    <div class="p-l-20 p-r-20">
                        <div class="row">
                            <div class="col-lg-3 visible-xlg">
                                <div class="widget-14-chart-legend bg-transparent text-black no-padding pull-right"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-xs-height">
                <div class="col-xs-height relative bg-master-lightest">
                    <div class="widget-14-chart_y_axis"></div>
                    <div class="widget-14-chart rickshaw-chart top-left top-right bottom-left bottom-right"></div>
                </div>
            </div>
            <!-- END WIDGET -->
        </div>

        <div id="rootwizard" class="m-t-50">
        <!-- Success Messages -->
            @include('partial/success_message')
        <!-- END Success Messages -->
        <!-- Error Messages -->
            @include('partial/error_message')
        <!-- END Error Messages -->
            <div class="tab-pane padding-20 active slide-left" id="app">
                {{ Form::open(['route' => ['outlet.store'],'method' => 'post', 'id' => 'form-project','files' => true]) }}
                    <warehouse-store-in-challan-create></warehouse-store-in-challan-create>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@endsection

@section('plugin-script')
    <script src="{{asset('theme/backend/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"
            type="text/javascript"></script>
    <script src="{{asset('theme/backend/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"
            type="text/javascript"></script>
    <script src="{{asset('assets_old') }}"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="{{asset('theme/backend/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{asset('theme/backend/assets/plugins/datatables-responsive/js/lodash.min.js') }}"></script>
@endsection

@section('page-script')
    <script src="{{asset('assets_old')}}" type="text/javascript"></script>
@endsection