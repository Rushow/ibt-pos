@if (count($errors) > 0)
    <div class="alert alert-danger text-center">
        @foreach ($errors->all() as $error)
            {{ $error }} <br/>
        @endforeach
    </div>
@endif