<nav class="page-sidebar" data-pages="sidebar">
    <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
    <div class="sidebar-overlay-slide from-top" id="appMenu">
        <div class="row">
            <div class="col-xs-6 no-padding">
                <a href="#" class="p-l-40">
                    <img src="{{ asset('images/social_app.svg') }}" alt="socail" data-src="{{ asset('images/social_app.svg') }}" data-src-retina="{{ asset('images/social_app.svg') }}" width="78" height="22" >
                </a>
            </div>
            <div class="col-xs-6 no-padding">
                <a href="#" class="p-l-10">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 m-t-20 no-padding">
                <a href="#" class="p-l-40">
                    <img src="{{ asset('images/calendar_app.svg') }}" alt="socail" data-src="{{ asset('images/calendar_app.svg') }}" data-src-retina="{{ asset('images/calendar_app.svg') }}" >
                </a>
            </div>
            <div class="col-xs-6 m-t-20 no-padding">
                <a href="#" class="p-l-10">
                    <img src="{{ asset('images/add_more.svg') }}" alt="socail" data-src="{{ asset('images/add_more.svg') }}" data-src-retina="{{ asset('images/add_more.svg') }}" >
                </a>
            </div>
        </div>
    </div>
    <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
    <!-- BEGIN SIDEBAR MENU HEADER-->
    <div class="sidebar-header">
        <img src="{{ asset('images/logo_white.png') }}" alt="logo" class="brand" data-src="{{ asset('images/logo_white.png') }}" data-src-retina="{{ asset('images/logo_white.png') }}" width="78" height="22">
        <div class="sidebar-header-controls">
            {{--<button type="button" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" data-pages-toggle="#appMenu"><i class="fa fa-angle-down fs-16"></i> </button>--}}
            <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i></button>
        </div>
    </div>
    <!-- END SIDEBAR MENU HEADER-->
    <!-- START SIDEBAR MENU -->
    <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
        @can('access-dashboard')
                <li class="m-t-30 open">
                    <a href="{{url('dashboard')}}" class="detailed">
                        <span class="title">Dashboard</span>
                    </a>
                    <span class="icon-thumbnail bg-success"><i class="pg-home"></i></span>
                </li>
            @endcan
            <li class="">
                <a href="javascript:;"><span class="title">Settings</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-cog"></i></span>
                <ul class="sub-menu">
                    @can('access-company-info')
                        <li class="">
                            <a href="{{ route('company.edit', Auth::user()->company_id ) }}">Company Info</a>
                            <span class="icon-thumbnail">Ci</span>
                        </li>
                    @endcan
                    @can('access-warehouse')
                        <li class="">
                            <a href="{{url('warehouse')}}">Warehouse</a>
                            <span class="icon-thumbnail">S</span>
                        </li>
                    @endcan
                    @can('access-vendor-supplier')
                        <li class="">
                            <a href="{{url('supplier')}}">Supplier</a>
                            <span class="icon-thumbnail">S</span>
                        </li>
                    @endcan
                    @can('access-global-vat-settings')
                        <li class="">
                            <a href="{{url('vat')}}">Vat Settings</a>
                            <span class="icon-thumbnail">Vs</span>
                        </li>
                    @endcan
                    @can('access-global-payment-method-settings')
                        <li class="">
                            <a href="{{url('payment_method')}}">Payment Methods Settings</a>
                            <span class="icon-thumbnail">Pms</span>
                        </li>
                    @endcan
                    @can('create-view-outlets')
                        <li class="">
                            <a href="{{url('out_let')}}">Out Lets</a>
                            <span class="icon-thumbnail">Ol</span>
                        </li>
                    @endcan
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Users</span>
                    <span class="arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-user"></i></span>
                <ul class="sub-menu">
                    @can('create-view-users')
                        <li class="">
                            <a href="{{url('user')}}">Users</a>
                            <span class="icon-thumbnail">U</span>
                        </li>
                    @endcan
                    @can('add-edit-access-permission')
                        <li class="">
                            <a href="{{url('role')}}">User Roles</a>
                            <span class="icon-thumbnail">Ol</span>
                        </li>
                    @endcan
                    @can('access-position-title')
                        <li class="">
                            <a href="{{url('user_title')}}">Position/Title</a>
                            <span class="icon-thumbnail">Pt</span>
                        </li>
                    @endcan
                </ul>
            </li>
            @can('create-view-customers')
                <li class="">
                    <a href="javascript:;">
                        <span class="title">Customers</span>
                        <span class="arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
                    <ul class="sub-menu">
                        <li class="">
                            <a href="{{url('customer')}}">Customers</a>
                            <span class="icon-thumbnail">U</span>
                        </li>
                    </ul>
                </li>
            @endcan
            @can('create-view-products')
            <li class="">
                <a href="javascript:;">
                    <span class="title">Discount</span>
                    <span class="arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-money"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="{{url('discount')}}">Discount Rule</a>
                        <span class="icon-thumbnail">D</span>
                    </li>
                    <li class="">
                        <a href="{{url('discountcode')}}">Coupon/Code</a>
                        <span class="icon-thumbnail">C</span>
                    </li>

                </ul>
            </li>
            @endcan
            <li class="">
                <a href="javascript:;"><span class="title">Catalog</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i class="fa fa-book"></i></span>
                <ul class="sub-menu">
                    @can('create-view-products')
                        <li class="">
                            <a href="{{url('product')}}">Product</a>
                            <span class="icon-thumbnail">P</span>
                        </li>
                    @endcan
                    @can('access-category')
                        <li class="">
                            <a href="{{url('category')}}">Product Category</a>
                            <span class="icon-thumbnail">Pc</span>
                        </li>
                    @endcan
                    @can('access-product-attributes')
                        <li class="">
                            <a href="{{url('attribute')}}">Product Attribute</a>
                            <span class="icon-thumbnail">Pa</span>
                        </li>
                    @endcan
                    @can('access-product-combination')
                        <li class="">
                            <a href="{{url('product/generate_barcode')}}">Product Barcode</a>
                            <span class="icon-thumbnail">Pb</span>
                        </li>
                    @endcan
                </ul>
            </li>
            <li class="">
                <a href="javascript:;"><span class="title">Inventory</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail "><i class="fa fa-th"></i></span>
                <ul class="sub-menu">
                    @can('access-warehouse')
                        <li class="">
                            <a href="{{url('warehouse_inventory')}}">Warehouse Inventory</a>
                            <span class="icon-thumbnail">WI</span>
                        </li>
                    @endcan
                    @can('access-warehouse')
                    <li class="">
                        <a href="{{url('outlet_inventory')}}">Outlet Inventory</a>
                        <span class="icon-thumbnail">OI</span>
                    </li>
                    @endcan
                    @can('access-warehouse-store-in')
                        <li class="">
                            <a href="{{url('warehouse_store_in_challan')}}">Warehouse Store In-challan</a>
                            <span class="icon-thumbnail">Wsi</span>
                        </li>
                    @endcan
                    <li class="">
                        <a href="{{url('warehouse_store_out_challan')}}">Warehouse Store Out</a>
                        <span class="icon-thumbnail">Wso</span>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;"><span class="title">Sales Reports</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail "><i class="fa fa-shopping-cart"></i></span>
                <ul class="sub-menu">
                    @can('access-warehouse')
                    <li class="">
                        <a  href="{{url('invoice')}}">Invoice Data</a>
                        <span></span>
                    </li>
                    @endcan
                    @can('access-sales-report')
                    <li class="">
                        <a  href="{{url('sales_report')}}">Sales Report</a>
                        <span ></span>
                    </li>
                    @endcan
                    @can('access-sales-report')
                        <li class="">
                            <a  href="{{url('daywise_sales_report')}}">Daywise Sales Report</a>
                            <span ></span>
                        </li>
                    @endcan
                    @can('access-sales-report')
                        <li class="">
                            <a  href="{{url('store_daywise_cumulative_sales_report')}}">Store & Daywise Cumulative Sales Report</a>
                            <span ></span>
                        </li>
                    @endcan
                    @can('access-sales-report')
                        <li class="">
                            <a  href="{{url('store_monthly_sales_report')}}">Store Monthly Sales Report</a>
                            <span ></span>
                        </li>
                    @endcan
                </ul>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- END SIDEBAR MENU -->
</nav>