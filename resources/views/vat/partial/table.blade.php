{{ Form::open(['route' => ['vat.destroy', '1'], 'method' => 'post']) }}
<button class="btn" onclick="return confirm('Are you sure you want to delete this item?');"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Percent</th>
        <th style="width:20%">Last Updated</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>

    @foreach( $vats as $vat )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input type="checkbox" name="vat_ids[]" value={{$vat->id}} data-id="checkbox" id="checkbox{{$vat->id}}">
                    <label for="checkbox{{$vat->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                {{ $vat->name }}
            </td> <td class="v-align-middle">
                {{ $vat->percent }}
            </td>
            <td class="v-align-middle">
                <p>{{ date('F d, Y H:i:s', strtotime($vat->updated_at)) }}</p>
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <button onclick="location.href='{{ route('vat.edit', $vat->id) }}'" type="button" class="btn btn-success"><i class="fa fa-pencil"></i></button>
                    {{ Form::open(['route' => ['vat.destroy', $vat->id], 'method' => 'post']) }}
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash-o"></i></button>
                        {{ method_field('DELETE') }}
                        {{ Form::hidden('vat_ids[]', $vat->id) }}
                    {{ Form::close() }}
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ method_field('DELETE') }}
{{ Form::close() }}