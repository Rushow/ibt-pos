{{ Form::open(['url' => 'store_in_challan/remove_challans', 'method' => 'DELETE']) }}
<button class="btn re-confirm"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Challan No</th>
        <th style="width:20%">To Warehouse</th>
        <th style="width:20%">Supplier</th>
        <th style="width:20%">Product</th>
        <th style="width:20%">Total Quantity</th>
        <th style="width:20%">Created By</th>
        <th style="width:20%">Request Date</th>
        <th style="width:20%">Status</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $store_in_challans as $store_in_challan )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input class="store_in_challan_ids" type="checkbox" name="store_in_challan_ids[{{$store_in_challan->id}}]" value={{$store_in_challan->id}} data-id="checkbox" id="checkbox{{$store_in_challan->id}}">
                    <label for="checkbox{{$store_in_challan->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                {{ $store_in_challan->challan_no }}
            </td>
            <td class="v-align-middle">
                {{ App\Warehouse::find($store_in_challan->warehouse_id)->name }}
            </td>
            <td class="v-align-middle">
                {{ App\Supplier::find($store_in_challan->challanInProducts[0]->supplier_id)->name }}
            </td>
            <td class="v-align-middle">
                <ul>
                    @foreach( $store_in_challan->challanInProducts as $challan_product )
                        <li>{{ App\Product::find($challan_product->product_id)->name }} </li>
                    @endforeach
                </ul>
            </td>
            <td class="v-align-middle">
                @php
                    $total_quantity = 0;
                @endphp
                @foreach( $store_in_challan->challanInProducts as $challan_product )
                    @foreach($challan_product->storeInCombinations as $product_combinations)
                        @php
                        $total_quantity = $total_quantity + $product_combinations->quantity;
                        @endphp
                    @endforeach
                @endforeach
                {{ $total_quantity }}
            </td>
            <td class="v-align-middle">
                @php
                    $user = App\User::with('userDetail')->where('id', $store_in_challan->created_by )->get();
                    echo $user[0]->userDetail->first_name ." ". $user[0]->userDetail->last_name;
                @endphp
            </td>
            <td class="v-align-middle">
                {{ $store_in_challan->request_date }}
            </td>
            <td class="v-align-middle">
                {{ $store_in_challan->status }}
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="final" class="btn btn-default" href="{{url('warehouse_store_in_challan/'.$store_in_challan->id.'/final')}}"><i class="fa fa-check-square"></i></a>
                    <a title="view" class="btn btn-primary" href="{{url('warehouse_store_in_challan/'.$store_in_challan->id)}}"><i class="fa fa-eye"></i></a>
                    <a title="Edit" class="btn btn-success" href="{{url('warehouse_store_in_challan/'.$store_in_challan->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" param-first="{{$store_in_challan->id}}" param-second="" type="button" class="btn btn-danger remove-item"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ Form::close() }}

{{ Form::open(['url' => '', 'method' => 'DELETE', 'class'=>'hide','id'=>'single-item-delete','action-url-first'=>url('store_in_challan')]) }}

{{ Form::close() }}
