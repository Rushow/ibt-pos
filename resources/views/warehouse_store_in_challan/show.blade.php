@extends('layouts.master')
@section('style')
    <link href="{{mix('/theme/backend/assets/css/datatables.css')}}" type="text/css" rel="stylesheet"/>
@stop
@section('content')

    <div class="container-fluid container-fixed-lg">
        <div class="page-content-wrapper">
            <!-- START PAGE CONTENT -->
            <div class="content">
                <nav class="navbar navbar-default bg-master-lighter sm-padding-10" role="navigation">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sub-nav">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="sub-nav">
                            <div class="row">
                                <div class="col-sm-4">
                                    <ul class="nav navbar-nav">
                                        <li>
                                            <span>Fortuna Bangladesh</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul class="nav navbar-nav navbar-center">
                                        <li><a href="#">Open</a>
                                        </li>
                                        <li><a href="{{ url("warehouse_challan_in_pdf/".$store_in_challan->id) }}" data-toggle="tooltip" data-placement="bottom" title="PDF"><i class="fa fa-file-pdf-o"></i></a>
                                        </li>
                                        <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="PDF"><i class="fa fa-file-excel-o"></i></a>
                                        </li>
                                        <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Print"><i class="fa fa-print"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li>
                                            <a href="#" class="p-r-10"><img width="25" height="25" alt="" class="icon-pdf" data-src-retina="{{ asset('resources/assets/img/invoice/pdf2x.png') }}" data-src="{{ asset('resources/assets/img/invoice/pdf.png') }}" src="{{ asset('resources/assets/img/invoice/pdf2x.png') }}">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="p-r-10"><img width="25" height="25" alt="" class="icon-image" data-src-retina="{{ asset('img/invoice/image2x.png') }}" data-src="{{ asset('img/invoice/image.png') }}" src="{{ asset('img/invoice/image2x.png') }}">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="p-r-10"><img width="25" height="25" alt="" class="icon-doc" data-src-retina="{{ asset('img/invoice/doc2x.png') }}" data-src="{{ asset('img/invoice/doc.png') }}" src="{{ asset('img/invoice/doc2x.png') }}">
                                            </a>
                                        </li>
                                        <li><a href="#" class="p-r-10" onclick="$.Pages.setFullScreen(document.querySelector('html'));"><i class="fa fa-expand"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container-fluid -->
                </nav>
                <!-- START CONTAINER FLUID -->
                <div class="container-fluid container-fixed-lg">
                    <!-- START PANEL -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="invoice padding-50 sm-padding-10">
                                <div>
                                    <div class="pull-left">
                                        <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="assets/img/invoice/squarespace2x.png" data-src="assets/img/invoice/squarespace.png" src="assets/img/invoice/squarespace2x.png">
                                        <address class="m-t-10">
                                            Fortuna Bangladesh
                                            <br>
                                            <br>
                                        </address>
                                    </div>
                                    <div class="pull-right sm-m-t-20">
                                        <h2 class="font-montserrat all-caps hint-text">Challan</h2>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <br>
                                <br>
                                <div class="container-sm-height">
                                    <div class="row-sm-height">
                                        <div class="col-md-9 col-sm-height sm-no-padding">
                                            <p class="small no-margin">Challan to</p>
                                            <h5 class="semi-bold m-t-0">Fortuna Bangladesh</h5>
                                            <address>
                                                <strong></strong>
                                                Kunia, Gazipur<br>
                                                Bangladesh
                                            </address>
                                        </div>
                                        <div class="col-md-3 col-sm-height col-bottom sm-no-padding sm-p-b-20">
                                            <br>
                                            <div>
                                                <div class="pull-left font-montserrat bold all-caps">Challan No :</div>
                                                <div class="pull-right">{{ $store_in_challan->challan_no }}</div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div>
                                                <div class="pull-left font-montserrat bold all-caps">Challan date :</div>
                                                <div class="pull-right">{{ $store_in_challan->request_date }}</div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table m-t-50">
                                        <thead>
                                        <tr>
                                            <th class="">Product Name</th>
                                            <th class="text-center">Product Combination</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-right">Price</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $total_quantity = 0;
                                            $total_prices = 0
                                        @endphp

                                        @foreach( $store_in_challan->challanInProducts as $challan_product )
                                        <tr>
                                            <td class="">
                                                <p class="text-black">{{ $challan_product->product->name }}</p>
                                                <p class="small hint-text">
                                                </p>
                                            </td>
                                            <td class="text-center">
                                                @foreach($challan_product->storeInCombinations as $product_combination)
                                                    @php
                                                        $count = count($product_combination->combination->combinationAttributeValues);
                                                        $i = 1;
                                                    @endphp
                                                    @foreach($product_combination->combination->combinationAttributeValues as $attributes)
                                                        {{ $attributes->attributeValue->attribute->name }}: {{ $attributes->attributeValue->value }}
                                                        @if($i < $count)
                                                            {{", "}}
                                                            @php($i++)
                                                        @endif
                                                    @endforeach
                                                    <br>
                                                @endforeach
                                            </td>
                                            <td class="text-center">
                                                @foreach($challan_product->storeInCombinations as $product_combination)
                                                    {{ $product_combination->quantity }}
                                                    @php($total_quantity = $total_quantity + $product_combination->quantity)
                                                    <br>
                                                @endforeach
                                            </td>
                                            <td class="text-right">
                                                @foreach($challan_product->product->prices as $product_prices)
                                                    {{ "$".$product_prices->price }}
                                                    @php($total_prices = $total_prices + $product_prices->price)
                                                @endforeach
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <div class="container-sm-height">
                                    <div class="row row-sm-height b-a b-grey">
                                        <div class="col-sm-2 col-sm-height col-middle p-l-25 sm-p-t-15 sm-p-l-15 clearfix sm-p-b-15">
                                            <h5 class="font-montserrat all-caps small no-margin hint-text bold">Total Quantity</h5>
                                            <h3 class="no-margin">{{ $total_quantity }}</h3>
                                        </div>
                                        <div class="col-sm-5 col-sm-height col-middle clearfix sm-p-b-15">
                                            <h5 class="font-montserrat all-caps small no-margin hint-text bold"></h5>
                                            <h3 class="no-margin"></h3>
                                        </div>
                                        <div class="col-sm-5 text-right bg-menu col-sm-height padding-15">
                                            <h5 class="font-montserrat all-caps small no-margin hint-text text-white bold">Total Price</h5>
                                            <h1 class="no-margin text-white">${{ $total_prices }}</h1>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div>
                                    <img src="{{ asset('images/logo.png') }}" alt="logo" data-src="{{ asset('images/logo.png') }}" data-src-retina="{{ asset('images/logo.png') }}" width="78" height="22">
                                    <span class="m-l-70 text-black sm-pull-right"></span>
                                    <span class="m-l-40 text-black sm-pull-right"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PANEL -->
                </div>
                <!-- END CONTAINER FLUID -->
            </div>
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTENT WRAPPER -->
    </div>
    </div>
@endsection
@section('page-script')
    <script src="{{mix('/theme/backend/assets/js/datatables.js')}}" type="text/javascript"></script>
@endsection

