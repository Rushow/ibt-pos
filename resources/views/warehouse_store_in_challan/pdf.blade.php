<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link href="{{ ltrim(mix('/theme/backend/assets/css/plugins.css'), '/') }}" type="text/css" rel="stylesheet"/>
    <style>
        body {
            color: #626262;
            font-family: "Segoe UI", Arial, sans-serif;
            font-size: 11px;
            font-weight: normal;
            letter-spacing: 0.01em;
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            -webkit-font-feature-settings: "kern" 1;
            -moz-font-feature-settings: "kern" 1;
        }
        .page-container {
            width: 100%;
            height: 100%;
        }
        .page-container .page-content-wrapper {
            min-height: 100%;
            position: relative;
        }
        .container-fluid {
            padding-left: 30px;
            padding-right: 30px;
        }
        .panel {
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 1px;
            -webkit-border-radius: 1px;
            -moz-border-radius: 1px;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease;
            position: relative;
        }
        .panel .panel-body {
            padding: 20px;
            height: 100%;
            width: 100%;
        }
        .padding-50 {
            padding: 50px !important;
        }
        .pull-left {
            float: left!important;
        }
        .pull-right {
            float: right!important;
        }
        table{
            background-color: #fff;
        }
    </style>
</head>
<body>

<div class="invoice padding-50 sm-padding-10">
    <div>
        <div class="col-md-9 col-sm-height sm-no-padding">
            <p class="small no-margin">Challan to</p>
            <h5 class="semi-bold m-t-0">Fortuna Bangladesh</h5>
            <address>
                Kunia, Gazipur<br>Bangladesh
            </address>
        </div>
        <div class="col-md-3 ">
            <div>
                <div class="pull-left bold">Challan No : {{ $store_in_challan->challan_no }}</div>
                <div class="pull-left bold">Challan date : {{ $store_in_challan->request_date }}</div>
            </div>
        </div>
    </div>
    <hr>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th class="text-left bold">Product Name</th>
                <th class="text-center bold">Product Combination</th>
                <th class="text-center bold">Quantity</th>
                <th class="text-right bold">Price</th>
            </tr>
            </thead>
            <tbody>
            @php
                $total_quantity = 0;
                $total_prices = 0
            @endphp

            @foreach( $store_in_challan->challanInProducts as $challan_product )
                <tr>
                    <td class="">
                        <p class="text-black">{{ $challan_product->product->name }}</p>
                        <p class="small hint-text">
                        </p>
                    </td>
                    <td class="text-center">
                        @foreach($challan_product->storeInCombinations as $product_combination)
                            @php
                                $count = count($product_combination->combination->combinationAttributeValues);
                                $i = 1;
                            @endphp
                            @foreach($product_combination->combination->combinationAttributeValues as $attributes)
                                {{ $attributes->attributeValue->attribute->name }}: {{ $attributes->attributeValue->value }}
                                @if($i < $count)
                                    {{", "}}
                                    @php($i++)
                                @endif
                            @endforeach
                            <br>
                        @endforeach
                    </td>
                    <td class="text-center">
                        @foreach($challan_product->storeInCombinations as $product_combination)
                            {{ $product_combination->quantity }}
                            @php($total_quantity = $total_quantity + $product_combination->quantity)
                            <br>
                        @endforeach
                    </td>
                    <td class="text-right">
                        @foreach($challan_product->product->prices as $product_prices)
                            {{ "$".$product_prices->price }}
                            @php($total_prices = $total_prices + $product_prices->price)
                        @endforeach
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <hr>
    <div class="text-right">
        <h5 class="font-montserrat bold">Total Quantity : {{ $total_quantity }}</h5>
        <h5 class="font-montserrat bold">Total Price : ${{ $total_prices }}</h5>
    </div>
    <hr>
    <div>
        <img src="{{ asset('images/logo.png') }}" alt="logo" data-src="{{ asset('images/logo.png') }}" data-src-retina="{{ asset('images/logo.png') }}" width="78" height="22">

    </div>
</div>

</body>
</html>