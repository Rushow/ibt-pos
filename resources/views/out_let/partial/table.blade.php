{{ Form::open(['url' => 'out_let/remove_outlets', 'method' => 'DELETE']) }}

    <button class="btn" onclick="return confirm('Are you sure you want to delete this item?');"><i class="pg-trash"></i></button>
    <table class="table table-hover" id="tableWithSearch">
        <thead>
            <tr>
                <th style="width:1%">
                    <div class="checkbox">
                        <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                        <label for="check_all"></label>
                    </div>
                </th>
                <th style="width:20%">Name</th>
                <th style="width:20%">Type</th>
                <th style="width:20%">Space</th>
                <th style="width:20%">Duration</th>
                <th style="width:20%">Expire Date</th>
                <th style="width:20%">Rent</th>
                <th style="width:20%">Trade License</th>
                <th style="width:20%">Address</th>
                <th style="width:20%">Phone</th>
                <th style="width:20%">Action</th>
            </tr>
        </thead>
        <tbody>

        @foreach( $outlets as $outlet )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input type="checkbox" name="outlet_ids[]" value={{$outlet->id}} data-id="checkbox" id="checkbox{{$outlet->id}}">
                    <label for="checkbox{{$outlet->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                <p>{{$outlet->name}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{ App\OutLetType::find($outlet->outlet_type_id)->name}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$outlet->space}} Sqft</p>
            </td>
            <td class="v-align-middle">
                <p>{{$outlet->duration}} Month(s)</p>
            </td>
            <td class="v-align-middle">
                <p>{{ date('F d, Y', strtotime($outlet->end_date)) }}
            </td>
            <td class="v-align-middle">
                <p>{{$outlet->monthly_rent}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$outlet->trade_license_no}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$outlet->address}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$outlet->phone}}</p>
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <button onclick="location.href='{{ route('out_let.edit', $outlet->id) }}'" type="button" class="btn btn-success"><i class="fa fa-pencil"></i></button>
                    {{ Form::open(['route' => ['out_let.destroy', $outlet->id], 'method' => 'post']) }}
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash-o"></i></button>
                        {{ method_field('DELETE') }}
                    {{ Form::hidden('outlet_ids[]', $outlet->id) }}
                    {{ Form::close() }}
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
</table>
{{ method_field('DELETE') }}
{{ Form::close() }}