{{ Form::open(['url' => 'attribute/remove_attributes', 'method' => 'DELETE']) }}
<button class="btn re-confirm"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Values</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $attributes as $attribute )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input class="attribute_ids" type="checkbox" name="attribute_ids[{{$attribute->id}}]" value={{$attribute->id}} data-id="checkbox" id="checkbox{{$attribute->id}}">
                    <label for="checkbox{{$attribute->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                <a href="{{url('attribute/'.$attribute->id.'/attribute_value')}}">{{ $attribute->name }} </a>
            </td>
            <td class="v-align-middle">
                {{ $attribute->attributeValues()->count() }}
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn btn-success" href="{{url('attribute/'.$attribute->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" param-first="{{$attribute->id}}" param-second="" type="button" class="btn btn-danger remove-item"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ Form::close() }}

{{ Form::open(['url' => '', 'method' => 'DELETE', 'class'=>'hide','id'=>'single-item-delete','action-url-first'=>url('attribute')]) }}

{{ Form::close() }}
