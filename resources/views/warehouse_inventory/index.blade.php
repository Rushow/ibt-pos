@extends('layouts.master')
@section('title', 'Warehouse Inventory')
@section('style')
    <link href="{{mix('/theme/backend/assets/css/datatables.css')}}" type="text/css" rel="stylesheet"/>
    <link href="{{url('/theme/backend/assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" type="text/css" rel="stylesheet"/>
@stop
@section('content')

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <!-- START WIDGET -->
            <div class="row-xs-height">
                <div class="col-xs-height">
                    <div class="p-l-20 p-r-20">
                        <div class="row">
                            <div class="col-lg-3 visible-xlg">
                                <div class="widget-14-chart-legend bg-transparent text-black no-padding pull-right"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-xs-height">
                <div class="col-xs-height relative bg-master-lightest">
                    <div class="widget-14-chart_y_axis"></div>
                    <div class="widget-14-chart rickshaw-chart top-left top-right bottom-left bottom-right"></div>
                </div>
            </div>
            <!-- END WIDGET -->
        </div>
        <div id="rootwizard" class="m-t-50">
            <!-- Success Messages -->
        @include('partial.success_message')
        <!-- END Success Messages -->
            <!-- Error Messages -->
        @include('partial.error_message')
        <!-- END Error Messages -->
            <!-- START CONTAINER FLUID -->
            <div class="container-fluid container-fixed-lg bg-white">
                <!-- START PANEL -->
                <div id="portlet-basic" class="panel panel-default" data-pages="portlet">
                    <div class="panel-heading ">
                        <div class="panel-title">Filter</div>
                        <div class="panel-controls">
                            <ul>
                                <li><a href="#" class="portlet-collapse" data-toggle="collapse"><i class="portlet-icon portlet-icon-collapse"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body" id="report-filter">
                        {!! Form::open(['url'=>'warehouse_inventory','role'=>'form','method'=>'get']) !!}
                        <div class="row clearfix">
                            <div class="col-md-4">
                                <div class="form-group form-group-default">
                                    {!! Form::label('product_name', 'Product') !!}
                                    {!! Form::text('product_name', '',['id'=>'product_name','class'=>'full-width  form-control','data-placeholder'=>'product name']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-group-default">
                                    {!! Form::label('to_date', 'To Date') !!}
                                    {!! Form::text('to_date', $to_date->format('Y-m-d'),['id'=>'to_date','class'=>'full-width  form-control datepicker','data-placeholder'=>'To Date','readonly'=>'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-group-default form-group-default-select2">
                                    {!! Form::label('warehouse_id', 'Warehouses') !!}
                                    {!! Form::select('warehouse_id', $warehouses, $warehouse_id ? $warehouse_id : null,['id'=>'warehouse_id','class'=>'full-width ','data-placeholder'=>'All Warehouses', 'placeholder' => 'All Warehouses','data-init-plugin'=>'select2']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">

                            <div class="col-md-12 col-md-offset-8">
                                <div class="col-md-4">
                                    <button type="submit"  type="button" class="btn btn-primary btn-cons full-width">Filter</button>
                                </div>
                                {{--<div class="col-md-4">--}}
                                    {{--<a target="_blank" href="{{url('warehouse_inventory_pdf?to_date='.$to_date->format('Y-m-d').'&warehouse_id='.$warehouse_id)}}" class="btn btn-primary btn-cons full-width">Download PDF</a>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<a target="_blank" href="{{url('warehouse_inventory_pdf?to_date='.$to_date->format('Y-m-d').'&warehouse_id='.$warehouse_id)}}" class="btn btn-primary btn-cons full-width">Download Excel</a>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="panel panel-transparent">
                    <div class="panel-heading">
                        <div class="panel-title panel-title-margin">Warehouse Inventory</div><br/>

                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <table class="table" id="tableWithSearch">
                            <thead>
                            <tr>
                                <th style="width:20%">Product Name</th>
                                <th style="width:20%">Reference Name</th>
                                <th style="width:20%">Product Combination</th>
                                <th style="width:20%">Quantity</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($inventory)
                            @foreach( $inventory as $product )

                                <tr>
                                    <td class="v-align-middle">
                                        {{ \App\ProductCombination::find($product->product_combination_id)->product->name }}
                                    </td>
                                    <td class="v-align-middle">
                                        {{ \App\ProductCombination::find($product->product_combination_id)->product->reference_code }}
                                    </td>
                                    <td class="v-align-middle">
                                        {{ \App\ProductCombination::find($product->product_combination_id)->name }}

                                    </td>
                                    <td class="v-align-middle">
                                        {{ $product->remaining }}
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END PANEL -->
            </div>
            <!-- END CONTAINER FLUID -->
        </div>
    </div>
@endsection

@section('page-script')

    <script src="{{mix('/theme/backend/assets/js/datatables.js')}}" type="text/javascript"></script>
    <script src="{{url('/theme/backend/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script>
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });

    </script>
@endsection