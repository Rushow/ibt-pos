<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link href="{{ ltrim(mix('/theme/backend/assets/css/plugins.css'), '/') }}" type="text/css" rel="stylesheet"/>
    <style>
        body {
            color: #626262;
            font-family: "Segoe UI", Arial, sans-serif;
            font-size: 11px;
            font-weight: normal;
            letter-spacing: 0.01em;
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            -webkit-font-feature-settings: "kern" 1;
            -moz-font-feature-settings: "kern" 1;
        }
        .page-container {
            width: 100%;
            height: 100%;
        }
        .page-container .page-content-wrapper {
            min-height: 100%;
            position: relative;
        }
        .container-fluid {
            padding-left: 30px;
            padding-right: 30px;
        }
        .panel {
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 1px;
            -webkit-border-radius: 1px;
            -moz-border-radius: 1px;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease;
            position: relative;
        }
        .panel .panel-body {
            padding: 20px;
            height: 100%;
            width: 100%;
        }
        .padding-50 {
            padding: 50px !important;
        }
        .pull-left {
            float: left!important;
        }
        .pull-right {
            float: right!important;
        }
        table{
            background-color: #fff;
        }
    </style>
</head>
<body>

    <div class="container-fluid container-fixed-lg">

        <div id="rootwizard" class="m-t-50">
            <div class="container-fluid container-fixed-lg bg-white">
                <div class="panel panel-transparent">
                    <div class="panel-heading">
                        <div class="panel-title panel-title-margin">Warehouse Inventory</div><br/>
                        <div class="pull-left">
                        </div>
                        <div class="pull-right">
                            {{--<div class="col-xs-12">--}}
                                {{--<input type="text" id="search-table" class="form-control pull-right" placeholder="Search">--}}
                            {{--</div>--}}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover" id="tableWithSearch">
                            <thead>
                            <tr>
                                <th class="v-align-middle" style="width:30%">Product Name</th>
                                <th class="v-align-middle" style="width:30%">Product Combination</th>
                                <th class="v-align-middle" style="width:30%">Quantity</th>
                            </tr>
                            </thead>
                            <hr>
                            <tbody>
                            @php
                                $total_quantity = 0;
                            @endphp
                            @foreach( $inventory as $product )
                                <tr>
                                    <td class="v-align-middle">
                                        {{ \App\Product::find($product->product_id)->name }}
                                    </td>
                                    <td class="v-align-middle">
                                        @foreach(\App\ProductCombinationAttribute::where('product_combination_id', '=', $product->product_combination_id)->get() as $attributes)
                                            {{ \App\AttributeValue::find($attributes->id)->value }}
                                        @endforeach
                                    </td>
                                    <td class="v-align-middle">
                                        {{ $product->quantity }}
                                        @php($total_quantity = $total_quantity + $product->quantity)
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="text-right">
                        <h5 class="font-montserrat bold">Total Quantity : {{ $total_quantity }}</h5>
                    </div>
                    <hr>
                    <div>
                        <img src="{{ asset('images/logo.png') }}" alt="logo" data-src="{{ asset('images/logo.png') }}" data-src-retina="{{ asset('images/logo.png') }}" width="78" height="22">

                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>