{{ Form::open(['route' => ['user_title.destroy', '1'], 'method' => 'post', 'id' => 'delete_items_form']) }}
<button id="delete_items" onclick="return confirm('Are you sure you want to delete this item?');" class="btn"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Title / Possition</th>
        <th style="width:20%">Last Updated</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>

    @foreach( $user_titles as $user_title )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input type="checkbox" name="user_title_ids[]" value={{$user_title->id}} data-id="checkbox" id="checkbox{{$user_title->id}}">
                    <label for="checkbox{{$user_title->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                <p>{{$user_title->title}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{ date('F d, Y H:i:s', strtotime($user_title->updated_at)) }}</p>
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <button onclick="location.href='{{ route('user_title.edit', $user_title->id) }}'" type="button" class="btn btn-success"><i class="fa fa-pencil"></i></button>
                    {{ Form::open(['route' => ['user_title.destroy', $user_title->id], 'method' => 'post']) }}
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash-o"></i></button>
                    {{ method_field('DELETE') }}
                        {{ Form::hidden('user_title_ids[]', $user_title->id) }}
                    {{ Form::close() }}
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ method_field('DELETE') }}
{{ Form::close() }}