{{ Form::open(['url' => 'role/remove_roles', 'method' => 'DELETE']) }}
<button class="btn re-confirm"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $roles as $role )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input @if( $role->slug == 'administrator' ) disabled @endif class="role_ids" type="checkbox" name="role_ids[{{$role->id}}]" value={{$role->id}} data-id="checkbox" id="checkbox{{$role->id}}">
                    <label for="checkbox{{$role->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                <a href="{{  url('role/'.$role->id.'/permission_role') }}">{{ $role->name }} </a>
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn btn-success" href="{{url('role/'.$role->id.'/edit')}}" @if( $role->slug == 'administrator' ) disabled @endif ><i class="fa fa-pencil"></i></a>
                    <button title="Delete" param-first="{{$role->id}}" param-second="" type="button" class="btn btn-danger remove-item" @if( $role->slug == 'administrator' || $role->users ) disabled @endif><i class="fa fa-trash-o"></i></button>
                </div>
                <div class="btn-actions set-permission">
                    <a title="Edit" class="btn btn-success" href="{{url('role/'.$role->id.'/permission_role')}}">Set Permission</a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ Form::close() }}

{{ Form::open(['url' => '', 'method' => 'DELETE', 'class'=>'hide','id'=>'single-item-delete','action-url-first'=>url('role')]) }}

{{ Form::close() }}
