<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Edit access permission</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required form-group-default-selectFx">
                            {{ Form::label('role', 'Role Name') }}
                            {{Form::select('role_id',$role_lists,'',['class' => 'cs-select cs-skin-slide cs-transparent form-control access-permission-select','data-init-plugin' => 'cs-select']) }}
                        </div>
                    </div>
                    <div class="form-group-attached">
                        <div class="form-group form-group-defaul" id="access_permission">
                            @foreach( $module_lists as $module )
                                {{ $module->name }}<br/>
                                @foreach( $module->permissions as $permissions )
                                    {{ $permissions->name }}<br/>
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group-attached pull-right padding-top">
                        <button class="btn btn-success btn-cons" type="submit">
                            <span>Save</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>