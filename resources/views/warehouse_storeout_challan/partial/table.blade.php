{{ Form::open(['url' => 'warehouse_store_out_challan', 'method' => 'DELETE']) }}
<button class="btn re-confirm"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Challan No</th>
        <th style="width:20%">From Warehouse</th>
        <th style="width:20%">Outlet</th>
        <th style="width:20%">Product</th>
        <th style="width:20%">Created By</th>
        <th style="width:20%">Request Date</th>
        <th style="width:20%">Status</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $store_out_challans as $store_out_challan )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input class="store_in_challan_ids" type="checkbox" name="store_in_challan_ids[{{$store_out_challan->id}}]" value={{$store_out_challan->id}} data-id="checkbox" id="checkbox{{$store_out_challan->id}}">
                    <label for="checkbox{{$store_out_challan->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                {{ $store_out_challan->challan_no }}
            </td>
            <td class="v-align-middle">
                {{ App\Warehouse::find($store_out_challan->warehouse_id)->name }}
            </td>
            <td class="v-align-middle">
                {{ App\OutLet::find($store_out_challan->challanoutProducts[0]->outlet_id)->name }}
            </td>
            <td class="v-align-middle">
                <ul>
                    @foreach( $store_out_challan->challanoutProducts as $challan_product )
                        <li>{{ App\Product::find($challan_product->product_id)->name }} </li>
                    @endforeach
                </ul>
            </td>
            <td class="v-align-middle">
                @php
                $user = App\User::with('userDetail')->where('id', $store_out_challan->user_id )->get();
                echo $user[0]->userDetail->first_name ." ". $user[0]->userDetail->last_name;
                @endphp
            </td>
            <td class="v-align-middle">
                {{ $store_out_challan->request_date }}
            </td>
            <td class="v-align-middle">
                {{ $store_out_challan->status }}
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    @if($store_out_challan->status !== 'draft')
                        <a title="final" class="btn btn-default" href="{{url('warehouse_store_out_challan/'.$store_out_challan->id.'/final')}}" disabled><i class="fa fa-check-square"></i></a>
                    @else
                        <a title="final" class="btn btn-default finalize" href="{{url('warehouse_store_out_challan/'.$store_out_challan->id.'/final')}}" id="{{ $store_out_challan->id }}"><i class="fa fa-square"></i></a>
                    @endif
                    <a title="view" class="btn btn-primary" href="{{url('warehouse_store_out_challan/'.$store_out_challan->id)}}"><i class="fa fa-eye"></i></a>
                    <a title="Edit" class="btn btn-success" href="{{url('warehouse_store_out_challan/'.$store_out_challan->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" param-first="{{$store_out_challan->id}}" param-second="" type="button" class="btn btn-danger remove-item"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ Form::close() }}

{{ Form::open(['url' => '', 'method' => 'DELETE', 'class'=>'hide','id'=>'single-item-delete','action-url-first'=>url('warehouse_store_out_challan')]) }}

{{ Form::close() }}
