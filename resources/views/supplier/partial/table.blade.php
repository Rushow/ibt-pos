{{ Form::open(['route' => ['supplier.destroy', '1'], 'method' => 'post', 'id' => 'outletDelete']) }}
<button class="btn" onclick="return confirm('Are you sure you want to delete this item?');"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Code</th>
        <th style="width:20%">Address</th>
        <th style="width:20%">City</th>
        <th style="width:20%">Zip</th>
        <th style="width:20%">Country</th>
        <th style="width:20%">Email</th>
        <th style="width:20%">Phone</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>

    @foreach( $suppliers as $supplier )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input type="checkbox" name="supplier_ids[]" value={{$supplier->id}} data-id="checkbox" id="checkbox{{$supplier->id}}">
                    <label for="checkbox{{$supplier->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                <p>{{$supplier->name}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$supplier->code}} Sqft</p>
            </td>
            <td class="v-align-middle">
                <p>{{$supplier->address}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$supplier->city}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$supplier->zip}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{ App\Country::find($supplier->country_id)->name}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$supplier->email}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$supplier->phone}}</p>
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <button onclick="location.href='{{ route('supplier.edit', $supplier->id) }}'" type="button" class="btn btn-success"><i class="fa fa-pencil"></i></button>
                    {{ Form::open(['route' => ['supplier.destroy', $supplier->id], 'method' => 'post']) }}
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash-o"></i></button>
                        {{ method_field('DELETE') }}
                        {{ Form::hidden('supplier_ids[]', $supplier->id) }}
                    {{ Form::close() }}
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ method_field('DELETE') }}
{{ Form::close() }}