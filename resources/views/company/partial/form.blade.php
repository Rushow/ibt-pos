<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Company Info</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('phone', 'Phone') }}
                            {{ Form::text('phone',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('email', 'Email') }}
                            {{ Form::EMAIL('email',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required form-group-default-selectFx">
                            {{ Form::label('businesscategory', 'Business Category') }}
                            {{Form::select('business_category_id',$business_categories,null,['class' => 'cs-select cs-skin-slide cs-transparent form-control','data-init-plugin' => 'cs-select']) }}
                        </div>
                    </div>
                    <br>
                </div>
            </div>
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('address', 'Address', ['class' => '']) }}
                            {{ Form::text('address',null, ['class' => 'form-control', 'placeholder' => 'Current address','required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('city', 'City', ['class' => '']) }}
                            {{ Form::text('city',null,  ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('zip', 'Zip/Postal Code', ['class' => '']) }}
                            {{ Form::text('zip',null, ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Receipt Info</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('receipt_address', 'Receipt address', ['class' => '']) }}
                            {{ Form::text('receipt_address', null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('receipt_text', 'Receipt text', ['class' => '']) }}
                            {{ Form::text('receipt_text', null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('signature_line_text', 'Signature line text', ['class' => '']) }}
                            {{ Form::text('signature_line_text', null, ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <button class="btn btn-success btn-cons pull-right" type="submit">
        <span>Save</span>
    </button>
</div>