<!DOCTYPE html>
<html>
<head>
    <title>Excel Export</title>
</head>
<body>
<table>
    <thead>
    <tr role="row">
        <th>Store</th>
        <th>Total</th>
        <th>Average</th>
        @foreach($totals as $total)
            <th>{{ Carbon\Carbon::parse($total->date)->formatLocalized('%d %b %Y') }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($outletSales as $outlet)
        <tr>
            <td>{{ $outlet->name }}</td>
            <td>{{ $outlet->total_sales }}</td>
            <td>{{ $outlet->average }}</td>
            @foreach($outlet->sales as $sale)
                <td>{{ $sale->total_received != null ? $sale->total_received : '0.00' }}</td>
            @endforeach
        </tr>
    @endforeach
    <tr>
        <th>Total:</th>
        <th>{{ $grandTotal }}</th>
        <th>{{ $grandAverage }}</th>
        @foreach($totals as $total)
            <th>{{ $total->total }}</th>
        @endforeach
    </tr>
    </tbody>
</table>
</body>
</html>