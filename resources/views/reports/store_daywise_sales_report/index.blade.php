@extends('layouts.master')
@section('head')
    <script src="{{ asset('js/jquery.min.js') }}"></script>
@endsection
@section('style')
    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <style>
        div.dt-buttons {
            margin-left: 20px;
        }
        .content {
            padding-top: 0px !important;
        }
        .m-t-50 {
            margin-top: 60px !important;
        }
        input{
            margin-bottom: 20px;
        }
        .submit {
            margin-top: 36px;
            border-right: 1px solid;
        }
        .table {
            overflow-x: scroll;
        }
        div.dt-buttons {
            margin-left: 0px !important;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div id="rootwizard" class="m-t-50">
                <div class="col-sm-12">
                    <h2>Store & Daywise Sales Report</h2>
                </div>

                <div class="col-sm-4">
                    <label for="start_date">From</label>
                    <input id="start_date" type="text" class="form-control start_date" value="{{ request('start_date', Carbon\Carbon::now()->subDays(7)->toDateString()) }}">
                </div>
                <div class="col-sm-4">
                    <label for="end_date">To</label>
                    <input id="end_date" type="text" class="form-control end_date" value="{{ request('end_date', Carbon\Carbon::now()->toDateString()) }}">
                </div>
                <div class="col-sm-4">
                    <button class="btn btn-primary btn-sm submit">Search</button>
                </div>
                <div class="col-sm-12">
                    <div id="dataTableBuilder_wrapper" class="dataTables_wrapper no-footer" style="overflow-x: scroll !important;">
                        <div class="dataTables_length" id="dataTableBuilder_length">
                            <div class="dt-buttons">
                                <a class="dt-button buttons-excel" tabindex="0" aria-controls="dataTableBuilder" href="{{ url('store_daywise_cumulative_sales_report') }}?start_date={{ request('start_date', Carbon\Carbon::now()->subDays(7)->toDateString()) }}&end_date={{ request('end_date', Carbon\Carbon::now()->toDateString()) }}&action=excel" target="_blank">
                                    <span>
                                        <i class="fa fa-file-excel-o"></i> Excel
                                    </span>
                                </a>
                            </div>
                            <table class="table table-responsive dataTable no-footer" id="dataTableBuilder" role="grid" style="width: 1640px;">
                                <thead>
                                <tr role="row">
                                    <th  rowspan="1" colspan="1">Store</th>
                                    <th  rowspan="1" colspan="1">Total</th>
                                    <th  rowspan="1" colspan="1">Average</th>
                                    @foreach($totals as $total)
                                        <th rowspan="1" colspan="1">{{ Carbon\Carbon::parse($total->date)->formatLocalized('%d %b %Y') }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($outletSales as $outlet)
                                <tr role="row" class="odd">
                                    <td>{{ $outlet->name }}</td>
                                    <td>{{ $outlet->total_sales }}</td>
                                    <td>{{ $outlet->average }}</td>
                                    @foreach($outlet->sales as $sale)
                                        <td>{{ $sale->total_received != null ? $sale->total_received : '0.00' }}</td>
                                    @endforeach
                                </tr>
                                @endforeach
                                <tr>
                                    <th>Total:</th>
                                    <th>{{ $grandTotal }}</th>
                                    <th>{{ $grandAverage }}</th>
                                    @foreach($totals as $total)
                                        <th>{{ $total->total }}</th>
                                    @endforeach
                                </tr>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script>
        $(function(){
            $('#start_date').datepicker({
                format: 'yyyy-mm-dd'
            });
            $('#end_date').datepicker({
                format: 'yyyy-mm-dd'
            });
        });

        $('.submit').on('click', function() {
            let startDate = $('#start_date').val();
            let endDate = $('#end_date').val();
            let url = window.location.pathname + '?start_date=' + startDate + '&end_date=' + endDate;
            window.location.replace(url);
        });
    </script>
@endsection