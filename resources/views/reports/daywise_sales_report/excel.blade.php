<!DOCTYPE html>
<html>
<head>
    <title>Excel Export</title>
</head>
<body>
<table>
    <thead>
        <tr role="row">
            <th>Date</th>
            <th>Product Price</th>
            <th>Vat</th>
            <th>Discount</th>
            <th>Card</th>
            <th>Cash</th>
            <th>Total Price</th>
        </tr>
    </thead>
    <tbody>
        @foreach($salesReport as $sr)
        <tr>
            <td>{{ $sr->date }}</td>
            <td>{{ $sr->product_price }}</td>
            <td>{{ $sr->vat }}</td>
            <td>{{ $sr->discount }}</td>
            <td>{{ $sr->card }}</td>
            <td>{{ $sr->cash }}</td>
            <td>{{ $sr->total_price }}</td>
        </tr>
        @endforeach
        @foreach($totals as $total)
        <tr>
            <th>Totals: </th>
            <th>{{ $total->product_price }}</th>
            <th>{{ $total->vat }}</th>
            <th>{{ $total->discount }}</th>
            <th>{{ $total->card }}</th>
            <th>{{ $total->cash }}</th>
            <th>{{ $total->total_price }}</th>
        </tr>
        @endforeach
    </tbody>
</table>
</body>
</html>