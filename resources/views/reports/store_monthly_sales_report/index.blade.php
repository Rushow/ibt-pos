@extends('layouts.master')
@section('head')
    <script src="{{ asset('js/jquery.min.js') }}"></script>
@endsection
@section('style')
    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <style>
        div.dt-buttons {
            margin-left: 20px;
        }
        .content {
            padding-top: 0px !important;
        }
        .m-t-50 {
            margin-top: 60px !important;
        }
        input{
            margin-bottom: 20px;
        }
        .submit {
            margin-top: 36px;
            border-right: 1px solid;
        }
        .table {
            overflow-x: scroll;
        }
        div.dt-buttons {
            margin-left: 0px !important;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div id="rootwizard" class="m-t-50">
                <div class="col-sm-12">
                    <h2>Store Monthly Sales Report</h2>
                </div>

                <div class="col-sm-4" style="margin-bottom: 10px">
                    <label for="start_date">Select Year</label>
                    <div class="input-group">
                        <select id="year" class="form-control">
                            @foreach($years as $year)
                                @if(!empty(request('year')))
                                    @if($year->year == request('year'))
                                        <option selected value="{{ $year->year }}">{{ $year->year }}</option>
                                    @else
                                        <option value="{{ $year->year }}">{{ $year->year }}</option>
                                    @endif
                                @else
                                    @if($year->year == \Carbon\Carbon::today()->format('Y'))
                                        <option selected value="{{ $year->year }}">{{ $year->year }}</option>
                                    @else
                                        <option value="{{ $year->year }}">{{ $year->year }}</option>
                                    @endif
                                @endif
                            @endforeach
                        </select>
                        <span class="input-group-addon submit btn-primary" id="basic-addon2" style="cursor: pointer">Submit</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div id="dataTableBuilder_wrapper" class="dataTables_wrapper no-footer" style="overflow-x: scroll !important;">
                        <div class="dataTables_length" id="dataTableBuilder_length">
                            <div class="dt-buttons">
                                <a class="dt-button buttons-excel" tabindex="0" aria-controls="dataTableBuilder" href="{{ url('store_monthly_sales_report') }}?year={{ request('year', Carbon\Carbon::today()->format('Y')) }}&action=excel" target="_blank">
                                    <span>
                                        <i class="fa fa-file-excel-o"></i> Excel
                                    </span>
                                </a>
                            </div>
                            <table class="table table-responsive dataTable no-footer" id="dataTableBuilder" role="grid" style="width: 1640px;">
                                <thead>
                                <tr role="row">
                                    <th  rowspan="1" colspan="1">Store</th>
                                    <th  rowspan="1" colspan="1">Total</th>
                                    <th  rowspan="1" colspan="1">Average</th>
                                    @foreach($totals as $total)
                                        <th rowspan="1" colspan="1">{{ Carbon\Carbon::parse($total->year.'-'.$total->month)->formatLocalized('%b %Y') }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($outletSales as $outlet)
                                    <tr role="row" class="odd">
                                        <td>{{ $outlet->name }}</td>
                                        <td>{{ $outlet->total_sales }}</td>
                                        <td>{{ $outlet->average }}</td>
                                        @foreach($outlet->sales as $sale)
                                            <td>{{ $sale->total_received != null ? $sale->total_received : '0.00' }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                <tr>
                                    <th>Total:</th>
                                    <th>{{ $grandTotal }}</th>
                                    <th>{{ $grandAverage }}</th>
                                    @foreach($totals as $total)
                                        <th>{{ $total->total }}</th>
                                    @endforeach
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('page-script')
            <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
            <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
            <script>
                $('.submit').on('click', function() {
                    let year = $('#year').val();
                    let url = window.location.pathname + '?year=' + year;
                    window.location.replace(url);
                });
            </script>
@endsection