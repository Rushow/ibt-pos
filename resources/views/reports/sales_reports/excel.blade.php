<!DOCTYPE html>
<html>
<head>
    <title>Excel Export</title>
</head>
<body>
<table>
    <thead>
    <tr role="row">
        <td></td>
        <th colspan="4">Product Lists</th>
    </tr>
    <tr>
        <th>Invoice No.</th>
        <th>Name</th>
        <th>Barcode</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Created At</th>
        <th>Total Without Vat</th>
        <th>Vat Amount</th>
        <th>Discount</th>
        <th>Card Received</th>
        <th>Cash Received</th>
        <th>Total Received</th>
    </tr>
    </thead>
    <tbody>
        @foreach($salesReport as $sr)
            <tr>
                <td>{{ $sr->invoice_no }}</td>
                <td>{{ $sr->name }}</td>
                <td>{{ $sr->barcode }}</td>
                <td>{{ $sr->quantity }}</td>
                <td>{{ $sr->price }}</td>
                <td>{{ \Carbon\Carbon::parse($sr->created_at)->formatLocalized('%d %b %Y') }}</td>
                <td>{{ $sr->total_without_vat }}</td>
                <td>{{ $sr->vat_amount }}</td>
                <td>{{ $sr->discount }}</td>
                <td>{{ $sr->card_received }}</td>
                <td>{{ $sr->cash_received }}</td>
                <td>{{ $sr->total_received }}</td>
            </tr>
        @endforeach
        @foreach($totals as $t)
            <tr>
                <th>Total: </th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <th>{{ $t->total }}</th>
            </tr>
        @endforeach
    </tbody>
</table>
</body>
</html>