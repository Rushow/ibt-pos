@extends('layouts.master')
@section('head')
    <script src="{{ asset('js/jquery.min.js') }}"></script>
@endsection
@section('style')
    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <style>
        div.dt-buttons {
            margin-left: 20px;
        }
        .content {
            padding-top: 0px !important;
        }
        .m-t-50 {
            margin-top: 60px !important;
        }
        input{
            margin-bottom: 20px;
        }
        .submit {
            margin-top: 36px;
            border-right: 1px solid;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div id="rootwizard" class="m-t-50">
                <div class="col-sm-12">
                    <h2>Sales Report</h2>
                </div>

                <div class="col-sm-4">
                    <label for="start_date">From</label>
                    <input id="start_date" type="text" class="form-control start_date" value="{{ request('start_date', Carbon\Carbon::now()->subDays(7)->toDateString()) }}">
                </div>
                <div class="col-sm-4">
                    <label for="end_date">To</label>
                    <input id="end_date" type="text" class="form-control end_date" value="{{ request('end_date', Carbon\Carbon::now()->toDateString()) }}">
                </div>
                <div class="col-sm-4">
                    <button class="btn btn-primary btn-sm submit">Search</button>
                </div>
                <div class="col-sm-12">
                    {!! $dataTable->table() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page-script')
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    {{--<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>--}}
    {{--<script src="/vendor/datatables/buttons.server-side.js"></script>--}}
    {{--<script src="{{mix('/theme/backend/assets/js/datatables.js')}}" type="text/javascript"></script>--}}
    {!! $dataTable->scripts() !!}
    <script>
        $(function() {
            var table = $('#dataTableBuilder').DataTable();
            $('#dataTableBuilder').on( 'draw.dt', function () {
                var pageInfo = table.page.info();
                if(pageInfo.page+1 == pageInfo.pages) {
                    showTotal();
                }
            } );

            $('#start_date').datepicker({
                format: 'yyyy-mm-dd'
            });
            $('#end_date').datepicker({
                format: 'yyyy-mm-dd'
            });

            function showTotal() {
                let startDate = $('#start_date').val();
                let endDate = $('#end_date').val();

                $.ajax({
                    url: '{{ url('get_sales_report_total') }}'+'?start_date='+startDate+'&end_date='+endDate,
                    method:'get',
                    success: function(data) {

                        $("#dataTableBuilder tbody").append('<tr><th>Total: </th><td></td><td></td><td></td><td></td><td></td><td></td>' +
                            '<td></td><td></td><td></td><td></td>' +
                            '<th>' + JSON.parse(data)[0].total + '</th></tr>');
                        if($('#product').length == 0) {
                            $('<tr id="product"><td style="border-bottom: none !important">&nbsp;</td><th style="border-bottom: 1px solid #000000 !important" cellpadding="1" colspan="4">Product Lists</th></tr>').insertBefore($('thead tr:first'));
                        }
                    }
                });
            }
        });

        $('.submit').on('click', function() {
            let startDate = $('#start_date').val();
            let endDate = $('#end_date').val();
            let url = window.location.pathname + '?start_date=' + startDate + '&end_date=' + endDate;
            window.location.replace(url);
        });

        jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
            return this.flatten().reduce( function ( a, b ) {
                if ( typeof a === 'string' ) {
                    a = a.replace(/[^\d.-]/g, '') * 1;
                }
                if ( typeof b === 'string' ) {
                    b = b.replace(/[^\d.-]/g, '') * 1;
                }

                return a + b;
            }, 0 );
        } );

        var e = '<div class="dt-buttons">' +
            '<a class="dt-button buttons-excel" target="_blank" href="{{ url('sales_report') }}?start_date={{ request('start_date', Carbon\Carbon::now()->subDays(7)->toDateString()) }}&end_date={{ request('end_date', Carbon\Carbon::now()->toDateString()) }}&action=excel">' +
            '<span>' +
            '<i class="fa fa-file-excel-o"></i> Excel' +
            '</span>' +
            '</a>';
        var lengthFilter = $('.dataTables_length');
        lengthFilter.after(e);

        $('#dataTableBuilder').on('preXhr.dt', function ( e, settings, data ) {
            data.start_date   = $('#start_date').val();
            data.end_date     = $('#end_date').val();
        });

    </script>
@endsection