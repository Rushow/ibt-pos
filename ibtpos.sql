-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 03, 2018 at 09:58 AM
-- Server version: 5.7.19
-- PHP Version: 7.0.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
SET FOREIGN_KEY_CHECKS = 0;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ibtpos`
--

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE IF NOT EXISTS `attributes` (
  `id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `company_id`, `name`, `type`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Size', 'drop-down-list', NULL, '2017-11-22 06:22:45', '2017-11-22 06:22:45'),
(2, 1, 'color', 'color-or-texture', NULL, '2017-11-22 06:22:55', '2017-11-22 06:22:55');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_values`
--

CREATE TABLE IF NOT EXISTS `attribute_values` (
  `id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  `value` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_values`
--

INSERT INTO `attribute_values` (`id`, `attribute_id`, `value`, `color`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, '36', '#000000', NULL, '2017-11-22 06:23:10', '2017-11-22 06:23:10'),
(2, 1, '39', '#000000', NULL, '2017-11-22 06:23:16', '2017-11-22 06:23:16'),
(3, 2, 'Pink', '#e81aa2', NULL, '2017-11-22 06:23:47', '2017-11-22 06:23:47'),
(4, 2, 'Red', '#de3737', NULL, '2017-11-22 06:23:57', '2017-11-22 06:23:57');

-- --------------------------------------------------------

--
-- Table structure for table `business_categories`
--

CREATE TABLE IF NOT EXISTS `business_categories` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business_categories`
--

INSERT INTO `business_categories` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Jewellers', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(2, 'Shop', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(3, 'Food', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(4, 'Restaurant', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(5, 'Leather Craft', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(6, 'Clothing', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_shop_default` int(11) NOT NULL DEFAULT '0',
  `level_depth` int(11) NOT NULL,
  `nleft` int(11) DEFAULT NULL,
  `nright` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL,
  `is_root_category` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `company_id`, `parent_id`, `name`, `is_shop_default`, `level_depth`, `nleft`, `nright`, `active`, `position`, `is_root_category`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'Home', 1, 0, 0, 0, 1, 1, 1, NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07');

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

CREATE TABLE IF NOT EXISTS `category_product` (
  `id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_product`
--

INSERT INTO `category_product` (`id`, `category_id`, `product_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL, NULL),
(2, 1, 2, NULL, NULL, NULL),
(3, 1, 3, NULL, NULL, NULL),
(4, 1, 4, NULL, NULL, NULL),
(6, 1, 6, NULL, NULL, NULL),
(7, 1, 7, NULL, NULL, NULL),
(8, 1, 8, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `time_zone_id` int(10) unsigned DEFAULT NULL,
  `business_category_id` int(10) unsigned NOT NULL,
  `currency_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt_address` text COLLATE utf8mb4_unicode_ci,
  `receipt_text` text COLLATE utf8mb4_unicode_ci,
  `signature_line_text` text COLLATE utf8mb4_unicode_ci,
  `receipt_logo` blob,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `country_id`, `time_zone_id`, `business_category_id`, `currency_id`, `name`, `phone`, `email`, `address`, `city`, `zip`, `receipt_address`, `receipt_text`, `signature_line_text`, `receipt_logo`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 23, 327, 1, 9, 'Infrablue Tech', '01729010683', 'sajib@bluebd.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `code`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Aruba', 'ABW', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(2, 'Afghanistan', 'AFG', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(3, 'Angola', 'AGO', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(4, 'Anguilla', 'AIA', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(5, 'Åland Islands', 'ALA', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(6, 'Albania', 'ALB', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(7, 'Andorra', 'AND', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(8, 'United Arab Emirates', 'ARE', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(9, 'Argentina', 'ARG', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(10, 'Armenia', 'ARM', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(11, 'American Samoa', 'ASM', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(12, 'Antarctica', 'ATA', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(13, 'French Southern Territories', 'ATF', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(14, 'Antigua and Barbuda', 'ATG', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(15, 'Australia', 'AUS', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(16, 'Austria', 'AUT', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(17, 'Azerbaijan', 'AZE', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(18, 'Burundi', 'BDI', NULL, '2017-11-22 06:18:05', '2017-11-22 06:18:05'),
(19, 'Belgium', 'BEL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(20, 'Benin', 'BEN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(21, 'Bonaire, Sint Eustatius and Saba', 'BES', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(22, 'Burkina Faso', 'BFA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(23, 'Bangladesh', 'BGD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(24, 'Bulgaria', 'BGR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(25, 'Bahrain', 'BHR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(26, 'Bahamas', 'BHS', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(27, 'Bosnia and Herzegovina', 'BIH', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(28, 'Saint Barthélemy', 'BLM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(29, 'Belarus', 'BLR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(30, 'Belize', 'BLZ', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(31, 'Bermuda', 'BMU', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(32, 'Bolivia, Plurinational State of', 'BOL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(33, 'Brazil', 'BRA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(34, 'Barbados', 'BRB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(35, 'Brunei Darussalam', 'BRN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(36, 'Bhutan', 'BTN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(37, 'Bouvet Island', 'BVT', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(38, 'Botswana', 'BWA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(39, 'Central African Republic', 'CAF', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(40, 'Canada', 'CAN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(41, 'Cocos (Keeling) Islands', 'CCK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(42, 'Switzerland', 'CHE', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(43, 'Chile', 'CHL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(44, 'China', 'CHN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(45, 'Côte d''Ivoire', 'CIV', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(46, 'Cameroon', 'CMR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(47, 'Congo, the Democratic Republic of the', 'COD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(48, 'Congo', 'COG', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(49, 'Cook Islands', 'COK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(50, 'Colombia', 'COL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(51, 'Comoros', 'COM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(52, 'Cape Verde', 'CPV', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(53, 'Costa Rica', 'CRI', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(54, 'Cuba', 'CUB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(55, 'Curaçao', 'CUW', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(56, 'Christmas Island', 'CXR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(57, 'Cayman Islands', 'CYM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(58, 'Cyprus', 'CYP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(59, 'Czech Republic', 'CZE', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(60, 'Germany', 'DEU', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(61, 'Djibouti', 'DJI', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(62, 'Dominica', 'DMA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(63, 'Denmark', 'DNK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(64, 'Dominican Republic', 'DOM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(65, 'Algeria', 'DZA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(66, 'Ecuador', 'ECU', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(67, 'Egypt', 'EGY', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(68, 'Eritrea', 'ERI', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(69, 'Western Sahara', 'ESH', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(70, 'Spain', 'ESP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(71, 'Estonia', 'EST', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(72, 'Ethiopia', 'ETH', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(73, 'Finland', 'FIN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(74, 'Fiji', 'FJI', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(75, 'Falkland Islands (Malvinas)', 'FLK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(76, 'France', 'FRA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(77, 'Faroe Islands', 'FRO', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(78, 'Micronesia, Federated States of', 'FSM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(79, 'Gabon', 'GAB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(80, 'United Kingdom', 'GBR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(81, 'Georgia', 'GEO', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(82, 'Guernsey', 'GGY', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(83, 'Ghana', 'GHA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(84, 'Gibraltar', 'GIB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(85, 'Guinea', 'GIN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(86, 'Guadeloupe', 'GLP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(87, 'Gambia', 'GMB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(88, 'Guinea-Bissau', 'GNB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(89, 'Equatorial Guinea', 'GNQ', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(90, 'Greece', 'GRC', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(91, 'Grenada', 'GRD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(92, 'Greenland', 'GRL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(93, 'Guatemala', 'GTM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(94, 'French Guiana', 'GUF', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(95, 'Guam', 'GUM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(96, 'Guyana', 'GUY', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(97, 'Hong Kong', 'HKG', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(98, 'Heard Island and McDonald Islands', 'HMD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(99, 'Honduras', 'HND', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(100, 'Croatia', 'HRV', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(101, 'Haiti', 'HTI', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(102, 'Hungary', 'HUN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(103, 'Indonesia', 'IDN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(104, 'Isle of Man', 'IMN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(105, 'India', 'IND', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(106, 'British Indian Ocean Territory', 'IOT', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(107, 'Ireland', 'IRL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(108, 'Iran, Islamic Republic of', 'IRN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(109, 'Iraq', 'IRQ', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(110, 'Iceland', 'ISL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(111, 'Israel', 'ISR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(112, 'Italy', 'ITA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(113, 'Jamaica', 'JAM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(114, 'Jersey', 'JEY', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(115, 'Jordan', 'JOR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(116, 'Japan', 'JPN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(117, 'Kazakhstan', 'KAZ', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(118, 'Kenya', 'KEN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(119, 'Kyrgyzstan', 'KGZ', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(120, 'Cambodia', 'KHM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(121, 'Kiribati', 'KIR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(122, 'Saint Kitts and Nevis', 'KNA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(123, 'Korea, Republic of', 'KOR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(124, 'Kuwait', 'KWT', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(125, 'Lao People''s Democratic Republic', 'LAO', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(126, 'Lebanon', 'LBN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(127, 'Liberia', 'LBR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(128, 'Libya', 'LBY', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(129, 'Saint Lucia', 'LCA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(130, 'Liechtenstein', 'LIE', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(131, 'Sri Lanka', 'LKA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(132, 'Lesotho', 'LSO', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(133, 'Lithuania', 'LTU', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(134, 'Luxembourg', 'LUX', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(135, 'Latvia', 'LVA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(136, 'Macao', 'MAC', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(137, 'Saint Martin (French part)', 'MAF', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(138, 'Morocco', 'MAR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(139, 'Monaco', 'MCO', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(140, 'Moldova, Republic of', 'MDA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(141, 'Madagascar', 'MDG', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(142, 'Maldives', 'MDV', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(143, 'Mexico', 'MEX', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(144, 'Marshall Islands', 'MHL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(145, 'Macedonia, the former Yugoslav Republic of', 'MKD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(146, 'Mali', 'MLI', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(147, 'Malta', 'MLT', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(148, 'Myanmar', 'MMR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(149, 'Montenegro', 'MNE', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(150, 'Mongolia', 'MNG', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(151, 'Northern Mariana Islands', 'MNP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(152, 'Mozambique', 'MOZ', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(153, 'Mauritania', 'MRT', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(154, 'Montserrat', 'MSR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(155, 'Martinique', 'MTQ', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(156, 'Mauritius', 'MUS', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(157, 'Malawi', 'MWI', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(158, 'Malaysia', 'MYS', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(159, 'Mayotte', 'MYT', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(160, 'Namibia', 'NAM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(161, 'New Caledonia', 'NCL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(162, 'Niger', 'NER', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(163, 'Norfolk Island', 'NFK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(164, 'Nigeria', 'NGA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(165, 'Nicaragua', 'NIC', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(166, 'Niue', 'NIU', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(167, 'Netherlands', 'NLD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(168, 'Norway', 'NOR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(169, 'Nepal', 'NPL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(170, 'Nauru', 'NRU', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(171, 'New Zealand', 'NZL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(172, 'Oman', 'OMN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(173, 'Pakistan', 'PAK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(174, 'Panama', 'PAN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(175, 'Pitcairn', 'PCN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(176, 'Peru', 'PER', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(177, 'Philippines', 'PHL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(178, 'Palau', 'PLW', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(179, 'Papua New Guinea', 'PNG', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(180, 'Poland', 'POL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(181, 'Puerto Rico', 'PRI', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(182, 'Korea, Democratic People''s Republic of', 'PRK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(183, 'Portugal', 'PRT', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(184, 'Paraguay', 'PRY', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(185, 'Palestinian Territory, Occupied', 'PSE', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(186, 'French Polynesia', 'PYF', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(187, 'Qatar', 'QAT', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(188, 'Réunion', 'REU', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(189, 'Romania', 'ROU', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(190, 'Russian Federation', 'RUS', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(191, 'Rwanda', 'RWA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(192, 'Saudi Arabia', 'SAU', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(193, 'Sudan', 'SDN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(194, 'Senegal', 'SEN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(195, 'Singapore', 'SGP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(196, 'South Georgia and the South Sandwich Islands', 'SGS', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(197, 'Saint Helena, Ascension and Tristan da Cunha', 'SHN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(198, 'Svalbard and Jan Mayen', 'SJM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(199, 'Solomon Islands', 'SLB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(200, 'Sierra Leone', 'SLE', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(201, 'El Salvador', 'SLV', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(202, 'San Marino', 'SMR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(203, 'Somalia', 'SOM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(204, 'Saint Pierre and Miquelon', 'SPM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(205, 'Serbia', 'SRB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(206, 'South Sudan', 'SSD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(207, 'Sao Tome and Principe', 'STP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(208, 'Suriname', 'SUR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(209, 'Slovakia', 'SVK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(210, 'Slovenia', 'SVN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(211, 'Sweden', 'SWE', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(212, 'Swaziland', 'SWZ', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(213, 'Sint Maarten (Dutch part)', 'SXM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(214, 'Seychelles', 'SYC', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(215, 'Syrian Arab Republic', 'SYR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(216, 'Turks and Caicos Islands', 'TCA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(217, 'Chad', 'TCD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(218, 'Togo', 'TGO', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(219, 'Thailand', 'THA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(220, 'Tajikistan', 'TJK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(221, 'Tokelau', 'TKL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(222, 'Turkmenistan', 'TKM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(223, 'Timor-Leste', 'TLS', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(224, 'Tonga', 'TON', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(225, 'Trinidad and Tobago', 'TTO', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(226, 'Tunisia', 'TUN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(227, 'Turkey', 'TUR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(228, 'Tuvalu', 'TUV', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(229, 'Taiwan, Province of China', 'TWN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(230, 'Tanzania, United Republic of', 'TZA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(231, 'Uganda', 'UGA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(232, 'Ukraine', 'UKR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(233, 'United States Minor Outlying Islands', 'UMI', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(234, 'Uruguay', 'URY', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(235, 'United States', 'USA', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(236, 'Uzbekistan', 'UZB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(237, 'Holy See (Vatican City State)', 'VAT', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(238, 'Saint Vincent and the Grenadines', 'VCT', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(239, 'Venezuela, Bolivarian Republic of', 'VEN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(240, 'Virgin Islands, British', 'VGB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(241, 'Virgin Islands, U.S.', 'VIR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(242, 'Viet Nam', 'VNM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(243, 'Vanuatu', 'VUT', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(244, 'Wallis and Futuna', 'WLF', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(245, 'Samoa', 'WSM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(246, 'Yemen', 'YEM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(247, 'South Africa', 'ZAF', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(248, 'Zambia', 'ZMB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(249, 'Zimbabwe', 'ZWE', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `code`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Albania Lek', 'ALL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(2, 'Afghanistan Afghani', 'AFN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(3, 'Argentina Peso', 'ARS', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(4, 'Aruba Guilder', 'AWG', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(5, 'Australia Dollar', 'AUD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(6, 'Azerbaijan New Manat', 'AZN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(7, 'Bahamas Dollar', 'BSD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(8, 'Barbados Dollar', 'BBD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(9, 'Bangladeshi taka', 'BDT', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(10, 'Belarus Ruble', 'BYR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(11, 'Belize Dollar', 'BZD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(12, 'Bermuda Dollar', 'BMD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(13, 'Bolivia Boliviano', 'BOB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(14, 'Bosnia and Herzegovina Convertible Marka', 'BAM', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(15, 'Botswana Pula', 'BWP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(16, 'Bulgaria Lev', 'BGN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(17, 'Brazil Real', 'BRL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(18, 'Brunei Darussalam Dollar', 'BND', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(19, 'Cambodia Riel', 'KHR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(20, 'Canada Dollar', 'CAD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(21, 'Cayman Islands Dollar', 'KYD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(22, 'Chile Peso', 'CLP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(23, 'China Yuan Renminbi', 'CNY', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(24, 'Colombia Peso', 'COP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(25, 'Costa Rica Colon', 'CRC', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(26, 'Croatia Kuna', 'HRK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(27, 'Cuba Peso', 'CUP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(28, 'Czech Republic Koruna', 'CZK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(29, 'Denmark Krone', 'DKK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(30, 'Dominican Republic Peso', 'DOP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(31, 'East Caribbean Dollar', 'XCD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(32, 'Egypt Pound', 'EGP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(33, 'El Salvador Colon', 'SVC', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(34, 'Estonia Kroon', 'EEK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(35, 'Euro Member Countries', 'EUR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(36, 'Falkland Islands (Malvinas) Pound', 'FKP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(37, 'Fiji Dollar', 'FJD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(38, 'Ghana Cedis', 'GHC', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(39, 'Gibraltar Pound', 'GIP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(40, 'Guatemala Quetzal', 'GTQ', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(41, 'Guernsey Pound', 'GGP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(42, 'Guyana Dollar', 'GYD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(43, 'Honduras Lempira', 'HNL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(44, 'Hong Kong Dollar', 'HKD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(45, 'Hungary Forint', 'HUF', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(46, 'Iceland Krona', 'ISK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(47, 'India Rupee', 'INR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(48, 'Indonesia Rupiah', 'IDR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(49, 'Iran Rial', 'IRR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(50, 'Isle of Man Pound', 'IMP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(51, 'Israel Shekel', 'ILS', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(52, 'Jamaica Dollar', 'JMD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(53, 'Japan Yen', 'JPY', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(54, 'Jersey Pound', 'JEP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(55, 'Kazakhstan Tenge', 'KZT', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(56, 'Korea (North) Won', 'KPW', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(57, 'Korea (South) Won', 'KRW', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(58, 'Kyrgyzstan Som', 'KGS', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(59, 'Laos Kip', 'LAK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(60, 'Latvia Lat', 'LVL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(61, 'Lebanon Pound', 'LBP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(62, 'Liberia Dollar', 'LRD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(63, 'Lithuania Litas', 'LTL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(64, 'Macedonia Denar', 'MKD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(65, 'Malaysia Ringgit', 'MYR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(66, 'Mauritius Rupee', 'MUR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(67, 'Mexico Peso', 'MXN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(68, 'Mongolia Tughrik', 'MNT', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(69, 'Mozambique Metical', 'MZN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(70, 'Namibia Dollar', 'NAD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(71, 'Nepal Rupee', 'NPR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(72, 'Netherlands Antilles Guilder', 'ANG', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(73, 'New Zealand Dollar', 'NZD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(74, 'Nicaragua Cordoba', 'NIO', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(75, 'Nigeria Naira', 'NGN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(76, 'Norway Krone', 'NOK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(77, 'Oman Rial', 'OMR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(78, 'Pakistan Rupee', 'PKR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(79, 'Panama Balboa', 'PAB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(80, 'Paraguay Guarani', 'PYG', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(81, 'Peru Nuevo Sol', 'PEN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(82, 'Philippines Peso', 'PHP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(83, 'Poland Zloty', 'PLN', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(84, 'Qatar Riyal', 'QAR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(85, 'Romania New Leu', 'RON', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(86, 'Russia Ruble', 'RUB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(87, 'Saint Helena Pound', 'SHP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(88, 'Saudi Arabia Riyal', 'SAR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(89, 'Serbia Dinar', 'RSD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(90, 'Seychelles Rupee', 'SCR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(91, 'Singapore Dollar', 'SGD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(92, 'Solomon Islands Dollar', 'SBD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(93, 'Somalia Shilling', 'SOS', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(94, 'South Africa Rand', 'ZAR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(95, 'Sri Lanka Rupee', 'LKR', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(96, 'Sweden Krona', 'SEK', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(97, 'Switzerland Franc', 'CHF', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(98, 'Suriname Dollar', 'SRD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(99, 'Syria Pound', 'SYP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(100, 'Taiwan New Dollar', 'TWD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(101, 'Thailand Baht', 'THB', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(102, 'Trinidad and Tobago Dollar', 'TTD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(103, 'Turkey Lira', 'TRY', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(104, 'Turkey Lira', 'TRL', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(105, 'Tuvalu Dollar', 'TVD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(106, 'Ukraine Hryvna', 'UAH', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(107, 'United Kingdom Pound', 'GBP', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(108, 'United States Dollar', 'USD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(109, 'Uruguay Peso', 'UYU', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(110, 'Uzbekistan Som', 'UZS', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(111, 'Venezuela Bolivar', 'VEF', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(112, 'Viet Nam Dong', 'VND', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(113, 'Yemen Rial', 'YER', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(114, 'Zimbabwe Dollar', 'ZWD', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) unsigned NOT NULL,
  `out_let_id` int(10) unsigned NOT NULL COMMENT 'Which outlet created this customer',
  `country_id` int(10) unsigned DEFAULT NULL,
  `customer_no` text COLLATE utf8mb4_unicode_ci COMMENT 'Customer_no =  Current Year+This user id',
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cell_phone` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` time DEFAULT NULL,
  `gender` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `address2` text COLLATE utf8mb4_unicode_ci,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `out_let_id`, `country_id`, `customer_no`, `first_name`, `last_name`, `cell_phone`, `work_phone`, `email`, `date_of_birth`, `gender`, `address`, `address2`, `city`, `zip`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, '', 'Sajib', 'Ahasan', '01534582730', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 1, NULL, NULL, 'Mr. Tea', 'Mr. Tea', '01673453623', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-03 10:32:22', '2018-01-03 10:32:22');

-- --------------------------------------------------------

--
-- Table structure for table `customer_order_details`
--

CREATE TABLE IF NOT EXISTS `customer_order_details` (
  `id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `order_by` int(10) unsigned NOT NULL COMMENT 'Sales Person',
  `serial_no` int(11) NOT NULL COMMENT 'Outlet wise serial no will be unique; each year it will start from 1',
  `order_date` date NOT NULL,
  `expected_delivery_date` date NOT NULL,
  `delivered_date` date NOT NULL,
  `unit_price` decimal(10,2) NOT NULL COMMENT 'Per Bhori',
  `weight_rotti` bigint(20) NOT NULL,
  `weight_milli` bigint(20) DEFAULT NULL,
  `material_price` decimal(8,2) NOT NULL,
  `making_charge` decimal(8,2) NOT NULL,
  `discount_coupon_id` int(11) DEFAULT NULL,
  `invoice_prefix_id` int(10) unsigned NOT NULL,
  `invoice_no` bigint(20) DEFAULT NULL COMMENT 'Sometimes Cash Memo No',
  `price_exclusive_vat` decimal(10,2) NOT NULL,
  `vat_amount` decimal(10,2) DEFAULT NULL,
  `price_inclusive_vat` decimal(10,2) NOT NULL,
  `order_discount_amount` decimal(10,2) DEFAULT NULL,
  `coupon_discount_amount` decimal(10,2) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `order_details` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Not-assigned',
  `is_sales_to_member` int(11) NOT NULL DEFAULT '0' COMMENT '1=Sales to member, 0=Sales to non-member',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_order_detail_paids`
--

CREATE TABLE IF NOT EXISTS `customer_order_detail_paids` (
  `id` int(10) unsigned NOT NULL,
  `customer_order_detail_id` int(10) unsigned NOT NULL,
  `paid_amount` decimal(10,2) NOT NULL,
  `paid_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dashboards`
--

CREATE TABLE IF NOT EXISTS `dashboards` (
  `id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
  `id` int(10) unsigned NOT NULL,
  `discount_name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `amount_type` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `discount_type_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `discount_name`, `discount`, `amount_type`, `start_date`, `end_date`, `discount_type_id`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'First Discount', 100, 3, '2017-11-01 00:00:00', '2017-11-30 00:00:00', 1, 0, '2017-11-26 05:36:47', '2017-12-03 02:52:32', NULL),
(2, 'LA Offer', 10, 4, '2017-11-01 00:00:00', '2017-11-30 00:00:00', 2, 1, '2017-11-26 06:01:25', '2017-12-03 03:54:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discount_amount_types`
--

CREATE TABLE IF NOT EXISTS `discount_amount_types` (
  `id` int(10) unsigned NOT NULL,
  `type_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discount_amount_types`
--

INSERT INTO `discount_amount_types` (`id`, `type_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'Amount ', '2017-11-26 05:33:56', '2017-11-26 05:33:56', NULL),
(4, 'Percentage', '2017-11-26 05:33:56', '2017-11-26 05:33:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discount_codes`
--

CREATE TABLE IF NOT EXISTS `discount_codes` (
  `id` int(10) unsigned NOT NULL,
  `discount_id` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount_code_type_id` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discount_codes`
--

INSERT INTO `discount_codes` (`id`, `discount_id`, `code`, `discount_code_type_id`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'XCFDFDG', 2, 1, '2017-11-26 06:01:42', '2017-11-29 02:55:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discount_code_types`
--

CREATE TABLE IF NOT EXISTS `discount_code_types` (
  `id` int(10) unsigned NOT NULL,
  `type_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discount_code_types`
--

INSERT INTO `discount_code_types` (`id`, `type_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'One-user-single-time ', '2017-11-26 05:35:06', '2017-11-26 05:35:06', NULL),
(2, 'Multiple-user-single-time', '2017-11-26 05:35:06', '2017-11-26 05:35:06', NULL),
(3, 'Multiple-user-multiple-times', '2017-11-26 05:35:06', '2017-11-26 05:35:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discount_coupons`
--

CREATE TABLE IF NOT EXISTS `discount_coupons` (
  `id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coupon_code` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount_amount` decimal(10,2) DEFAULT NULL,
  `discount_percent` decimal(10,2) DEFAULT NULL,
  `total_coupon` int(11) NOT NULL,
  `coupon_left` int(11) NOT NULL,
  `active_date` time NOT NULL,
  `expire_date` time NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `discount_coupon_outlets`
--

CREATE TABLE IF NOT EXISTS `discount_coupon_outlets` (
  `discount_coupon_id` int(10) unsigned NOT NULL,
  `out_let_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `discount_days`
--

CREATE TABLE IF NOT EXISTS `discount_days` (
  `id` int(10) unsigned NOT NULL,
  `day_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discount_days`
--

INSERT INTO `discount_days` (`id`, `day_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(0, 'Everyday', NULL, NULL, NULL),
(1, 'Sunday', NULL, NULL, NULL),
(2, 'Monday', NULL, NULL, NULL),
(3, 'Tuesday', NULL, NULL, NULL),
(4, 'Wednesday', NULL, NULL, NULL),
(5, 'Thursday', NULL, NULL, NULL),
(6, 'Friday', NULL, NULL, NULL),
(7, 'Saturday', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discount_types`
--

CREATE TABLE IF NOT EXISTS `discount_types` (
  `id` int(10) unsigned NOT NULL,
  `discount_type_name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `has_dependency` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discount_types`
--

INSERT INTO `discount_types` (`id`, `discount_type_name`, `has_dependency`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'General', 0, '2017-11-26 05:33:30', '2017-11-26 05:33:30', NULL),
(2, 'By Coupon', 0, '2017-11-26 05:33:30', '2017-11-26 05:33:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discount_type_configuration_categories`
--

CREATE TABLE IF NOT EXISTS `discount_type_configuration_categories` (
  `id` int(10) unsigned NOT NULL,
  `discount_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discount_type_configuration_categories`
--

INSERT INTO `discount_type_configuration_categories` (`id`, `discount_id`, `category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 0, '2017-11-26 05:36:47', '2017-11-29 02:56:16', '2017-11-29 02:56:16'),
(2, 2, 0, '2017-11-26 06:01:25', '2017-11-29 03:10:06', '2017-11-29 03:10:06'),
(3, 1, 1, '2017-11-29 02:56:16', '2017-11-29 02:57:57', '2017-11-29 02:57:57'),
(4, 1, 1, '2017-11-29 02:57:57', '2017-11-29 02:58:52', '2017-11-29 02:58:52'),
(5, 1, 0, '2017-11-29 02:58:52', '2017-11-29 02:59:08', '2017-11-29 02:59:08'),
(6, 1, 1, '2017-11-29 02:59:08', '2017-11-29 03:09:42', '2017-11-29 03:09:42'),
(7, 1, 0, '2017-11-29 03:09:42', '2017-12-03 01:58:21', '2017-12-03 01:58:21'),
(8, 2, 0, '2017-11-29 03:10:06', '2017-12-03 02:03:29', '2017-12-03 02:03:29'),
(9, 1, 0, '2017-12-03 01:58:21', '2017-12-03 02:00:02', '2017-12-03 02:00:02'),
(10, 1, 0, '2017-12-03 02:00:02', '2017-12-03 02:00:47', '2017-12-03 02:00:47'),
(11, 1, 0, '2017-12-03 02:00:47', '2017-12-03 02:01:18', '2017-12-03 02:01:18'),
(12, 1, 0, '2017-12-03 02:01:18', '2017-12-03 02:03:07', '2017-12-03 02:03:07'),
(13, 1, 0, '2017-12-03 02:03:07', '2017-12-03 02:52:32', '2017-12-03 02:52:32'),
(14, 2, 0, '2017-12-03 02:03:29', '2017-12-03 02:48:03', '2017-12-03 02:48:03'),
(15, 2, 0, '2017-12-03 02:48:03', '2017-12-03 02:55:45', '2017-12-03 02:55:45'),
(16, 1, 0, '2017-12-03 02:52:32', '2017-12-03 03:46:12', '2017-12-03 03:46:12'),
(17, 2, 0, '2017-12-03 02:55:45', '2017-12-03 02:57:28', '2017-12-03 02:57:28'),
(18, 2, 0, '2017-12-03 02:57:28', '2017-12-03 02:57:50', '2017-12-03 02:57:50'),
(19, 2, 0, '2017-12-03 02:57:50', '2017-12-03 03:00:06', '2017-12-03 03:00:06'),
(20, 2, 0, '2017-12-03 03:00:06', '2017-12-03 03:54:51', '2017-12-03 03:54:51'),
(21, 1, 1, '2017-12-03 03:46:12', '2017-12-03 03:48:07', '2017-12-03 03:48:07'),
(22, 1, 0, '2017-12-03 03:48:07', '2017-12-03 03:51:39', '2017-12-03 03:51:39'),
(23, 1, 0, '2017-12-03 03:51:39', '2017-12-03 03:51:39', NULL),
(24, 2, 0, '2017-12-03 03:54:51', '2017-12-03 03:54:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discount_type_configuration_codes`
--

CREATE TABLE IF NOT EXISTS `discount_type_configuration_codes` (
  `id` int(10) unsigned NOT NULL,
  `discount_id` int(11) NOT NULL,
  `discount_code_type_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `discount_type_configuration_days`
--

CREATE TABLE IF NOT EXISTS `discount_type_configuration_days` (
  `id` int(10) unsigned NOT NULL,
  `discount_id` int(11) NOT NULL,
  `discount_day_id` int(11) NOT NULL,
  `day_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discount_type_configuration_days`
--

INSERT INTO `discount_type_configuration_days` (`id`, `discount_id`, `discount_day_id`, `day_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 0, 'Everyday', '2017-12-03 02:03:07', '2017-12-03 02:52:32', '2017-12-03 02:52:32'),
(2, 2, 1, 'Sunday', '2017-12-03 02:03:30', '2017-12-03 02:48:03', '2017-12-03 02:48:03'),
(3, 2, 1, 'Sunday', '2017-12-03 02:48:03', '2017-12-03 03:00:06', '2017-12-03 03:00:06'),
(4, 2, 3, 'Tuesday', '2017-12-03 02:48:03', '2017-12-03 03:00:06', '2017-12-03 03:00:06'),
(5, 1, 2, 'Monday', '2017-12-03 02:52:32', '2017-12-03 03:46:12', '2017-12-03 03:46:12'),
(6, 1, 3, 'Tuesday', '2017-12-03 02:52:32', '2017-12-03 03:46:12', '2017-12-03 03:46:12'),
(7, 2, 0, 'Everyday', '2017-12-03 02:55:45', '2017-12-03 03:00:06', '2017-12-03 03:00:06'),
(8, 2, 0, 'Everyday', '2017-12-03 02:57:28', '2017-12-03 03:00:06', '2017-12-03 03:00:06'),
(9, 2, 0, 'Everyday', '2017-12-03 03:00:06', '2017-12-03 03:54:51', '2017-12-03 03:54:51'),
(10, 1, 2, 'Monday', '2017-12-03 03:46:12', '2017-12-03 03:48:07', '2017-12-03 03:48:07'),
(11, 1, 3, 'Tuesday', '2017-12-03 03:46:12', '2017-12-03 03:48:07', '2017-12-03 03:48:07'),
(12, 1, 2, 'Monday', '2017-12-03 03:48:07', '2017-12-03 03:51:39', '2017-12-03 03:51:39'),
(13, 1, 3, 'Tuesday', '2017-12-03 03:48:07', '2017-12-03 03:51:39', '2017-12-03 03:51:39'),
(14, 1, 2, 'Monday', '2017-12-03 03:51:39', '2017-12-03 03:51:39', NULL),
(15, 1, 3, 'Tuesday', '2017-12-03 03:51:39', '2017-12-03 03:51:39', NULL),
(16, 2, 0, 'Everyday', '2017-12-03 03:54:51', '2017-12-03 03:54:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discount_type_configuration_outlets`
--

CREATE TABLE IF NOT EXISTS `discount_type_configuration_outlets` (
  `id` int(10) unsigned NOT NULL,
  `discount_id` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discount_type_configuration_outlets`
--

INSERT INTO `discount_type_configuration_outlets` (`id`, `discount_id`, `outlet_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 0, '2017-11-26 05:36:47', '2017-11-29 02:56:16', '2017-11-29 02:56:16'),
(2, 2, 0, '2017-11-26 06:01:25', '2017-11-29 03:10:06', '2017-11-29 03:10:06'),
(3, 1, 1, '2017-11-29 02:56:16', '2017-11-29 02:57:57', '2017-11-29 02:57:57'),
(4, 1, 0, '2017-11-29 02:57:57', '2017-11-29 02:58:52', '2017-11-29 02:58:52'),
(5, 1, 0, '2017-11-29 02:58:52', '2017-11-29 02:59:08', '2017-11-29 02:59:08'),
(6, 1, 0, '2017-11-29 02:59:08', '2017-11-29 03:09:42', '2017-11-29 03:09:42'),
(7, 1, 0, '2017-11-29 03:09:42', '2017-12-03 01:58:21', '2017-12-03 01:58:21'),
(8, 2, 0, '2017-11-29 03:10:06', '2017-12-03 02:03:29', '2017-12-03 02:03:29'),
(9, 1, 0, '2017-12-03 01:58:21', '2017-12-03 02:00:02', '2017-12-03 02:00:02'),
(10, 1, 0, '2017-12-03 02:00:02', '2017-12-03 02:00:47', '2017-12-03 02:00:47'),
(11, 1, 0, '2017-12-03 02:00:47', '2017-12-03 02:01:18', '2017-12-03 02:01:18'),
(12, 1, 0, '2017-12-03 02:01:18', '2017-12-03 02:03:07', '2017-12-03 02:03:07'),
(13, 1, 0, '2017-12-03 02:03:07', '2017-12-03 02:52:32', '2017-12-03 02:52:32'),
(14, 2, 0, '2017-12-03 02:03:29', '2017-12-03 02:48:03', '2017-12-03 02:48:03'),
(15, 2, 0, '2017-12-03 02:48:03', '2017-12-03 02:55:45', '2017-12-03 02:55:45'),
(16, 1, 0, '2017-12-03 02:52:32', '2017-12-03 03:46:12', '2017-12-03 03:46:12'),
(17, 2, 0, '2017-12-03 02:55:45', '2017-12-03 02:57:28', '2017-12-03 02:57:28'),
(18, 2, 0, '2017-12-03 02:57:28', '2017-12-03 02:57:50', '2017-12-03 02:57:50'),
(19, 2, 0, '2017-12-03 02:57:50', '2017-12-03 03:00:06', '2017-12-03 03:00:06'),
(20, 2, 0, '2017-12-03 03:00:06', '2017-12-03 03:54:51', '2017-12-03 03:54:51'),
(21, 1, 0, '2017-12-03 03:46:12', '2017-12-03 03:48:07', '2017-12-03 03:48:07'),
(22, 1, 0, '2017-12-03 03:48:07', '2017-12-03 03:51:39', '2017-12-03 03:51:39'),
(23, 1, 1, '2017-12-03 03:48:07', '2017-12-03 03:51:39', '2017-12-03 03:51:39'),
(24, 1, 0, '2017-12-03 03:51:39', '2017-12-03 03:51:39', NULL),
(25, 1, 1, '2017-12-03 03:51:39', '2017-12-03 03:51:39', NULL),
(26, 2, 0, '2017-12-03 03:54:51', '2017-12-03 03:54:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discount_type_configuration_products`
--

CREATE TABLE IF NOT EXISTS `discount_type_configuration_products` (
  `id` int(10) unsigned NOT NULL,
  `discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(10) unsigned NOT NULL,
  `outlet_id` int(10) unsigned NOT NULL,
  `sales_by` int(10) unsigned NOT NULL COMMENT 'Sales Person',
  `customer_id` int(10) unsigned DEFAULT NULL,
  `invoice_no` bigint(20) NOT NULL,
  `product_price_exclusive_vat` decimal(10,2) NOT NULL,
  `vat_amount` decimal(10,2) DEFAULT NULL,
  `product_discount_amount` decimal(10,2) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `credit_card_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit_card_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_received` decimal(10,2) DEFAULT NULL,
  `cash_received` decimal(10,2) DEFAULT NULL,
  `total_received` decimal(10,2) NOT NULL,
  `sales_notes` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `outlet_id`, `sales_by`, `customer_id`, `invoice_no`, `product_price_exclusive_vat`, `vat_amount`, `product_discount_amount`, `total_amount`, `credit_card_type`, `credit_card_number`, `card_received`, `cash_received`, `total_received`, `sales_notes`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 1, 2, 1, 10001, '2300.00', '345.00', '100.00', '2545.00', NULL, NULL, NULL, '2545.00', '2545.00', 'First Sale', NULL, NULL, NULL),
(3, 1, 1, 2, 1010101, '3000.00', '300.00', '100.00', '3200.00', NULL, NULL, '0.00', '3200.00', '3200.00', NULL, NULL, '2018-01-03 10:38:55', '2018-01-03 10:38:55'),
(4, 1, 1, 2, 1010101, '3000.00', '300.00', '100.00', '3200.00', NULL, NULL, '0.00', '3200.00', '3200.00', NULL, NULL, '2018-01-03 10:39:28', '2018-01-03 10:39:28');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_prefixes`
--

CREATE TABLE IF NOT EXISTS `invoice_prefixes` (
  `id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_products`
--

CREATE TABLE IF NOT EXISTS `invoice_products` (
  `id` int(10) unsigned NOT NULL,
  `invoice_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `product_combination_id` int(10) unsigned NOT NULL,
  `quantity` tinyint(4) NOT NULL,
  `discount_rules_id` int(11) DEFAULT NULL,
  `discount_code_id` int(11) DEFAULT NULL COMMENT 'If discount type is coupon',
  `product_discounts_id` int(10) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_products`
--

INSERT INTO `invoice_products` (`id`, `invoice_id`, `product_id`, `product_combination_id`, `quantity`, `discount_rules_id`, `discount_code_id`, `product_discounts_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, 1, NULL, NULL, 1, NULL, NULL, NULL),
(2, 2, 2, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 4, 1, 2, 1, NULL, NULL, NULL, NULL, '2018-01-04 06:36:22', '2018-01-04 06:36:22'),
(4, 4, 1, 2, 1, 1, 1, 1, NULL, '2018-01-04 06:37:24', '2018-01-04 06:37:24'),
(5, 4, 2, 4, 1, 1, 1, 1, NULL, '2018-01-04 06:38:44', '2018-01-04 06:38:44'),
(6, 4, 2, 4, 1, NULL, NULL, NULL, NULL, '2018-01-04 10:56:16', '2018-01-04 10:56:16'),
(7, 4, 2, 4, 1, NULL, NULL, NULL, NULL, '2018-01-04 10:56:46', '2018-01-04 10:56:46'),
(8, 4, 2, 4, 1, NULL, NULL, NULL, NULL, '2018-01-04 10:57:02', '2018-01-04 10:57:02'),
(9, 4, 2, 4, 1, NULL, NULL, NULL, NULL, '2018-01-04 11:30:39', '2018-01-04 11:30:39'),
(10, 4, 2, 4, 1, NULL, NULL, NULL, NULL, '2018-01-04 11:31:12', '2018-01-04 11:31:12');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(73, '2014_10_12_000000_create_users_table', 1),
(74, '2014_10_12_100000_create_password_resets_table', 1),
(75, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(76, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(77, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(78, '2016_06_01_000004_create_oauth_clients_table', 1),
(79, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(80, '2017_02_27_075709_create_countries_table', 1),
(81, '2017_02_28_094402_create_business_categories_table', 1),
(82, '2017_02_28_095631_create_currencies_table', 1),
(83, '2017_02_28_115933_create_time_zones_table', 1),
(84, '2017_02_29_094402_create_companies_table', 1),
(85, '2017_03_01_152455_create_tax_categories_table', 1),
(86, '2017_03_01_175246_create_tax_category_values_table', 1),
(87, '2017_03_01_183637_create_payment_methods_table', 1),
(88, '2017_03_01_190940_create_outlet_types_table', 1),
(89, '2017_03_01_192312_create_out_lets_table', 1),
(90, '2017_03_12_094544_create_suppliers_table', 1),
(91, '2017_03_12_104115_create_products_table', 1),
(92, '2017_03_12_104552_create_categories_table', 1),
(93, '2017_03_12_114030_create_category_product_table', 1),
(94, '2017_03_15_132535_create_attributes_table', 1),
(95, '2017_03_15_133848_create_attribute_values_table', 1),
(96, '2017_03_18_091818_create_dashboards_table', 1),
(97, '2017_03_19_064200_create_user_titles_table', 1),
(98, '2017_03_20_203049_create_warehouses_table', 1),
(99, '2017_03_22_053610_create_schedule_types_table', 1),
(100, '2017_03_22_065017_create_outlet_schedules_table', 1),
(101, '2017_03_22_100333_create_product_discounts_table', 1),
(102, '2017_03_22_101757_create_product_discount_outlets_table', 1),
(103, '2017_03_22_112743_create_discount_coupons_table', 1),
(104, '2017_03_22_114115_create_discount_coupon_outlets_table', 1),
(105, '2017_03_25_060929_create_vats_table', 1),
(106, '2017_03_26_193909_create_product_prices_table', 1),
(107, '2017_03_27_181103_create_invoice_prefixes_table', 1),
(108, '2017_03_27_181110_create_customers_table', 1),
(109, '2017_03_28_085336_create_user_details_table', 1),
(110, '2017_03_29_092220_create_modules_table', 1),
(111, '2017_03_30_082207_create_roles_table', 1),
(112, '2017_04_02_070626_create_warehouse_store_out_types_table', 1),
(113, '2017_04_02_071305_create_warehouse_store_in_challans_table', 1),
(114, '2017_04_02_071325_create_warehouse_store_in_challan_products_table', 1),
(115, '2017_04_02_090435_create_warehouse_store_out_challans_table', 1),
(116, '2017_04_02_090445_create_warehouse_store_out_challan_products_table', 1),
(117, '2017_04_02_093910_create_outlet_challanins_table', 1),
(118, '2017_04_02_093915_create_outlet_products_table', 1),
(119, '2017_04_02_093917_create_sales_table', 1),
(120, '2017_04_02_103009_create_sale_products_table', 1),
(121, '2017_04_02_103019_create_customer_rewards_table', 1),
(122, '2017_05_02_070446_create_product_combinations_table', 1),
(123, '2017_05_02_070935_create_product_combination_attributes_table', 1),
(124, '2017_05_02_085657_create_statuses_table', 1),
(125, '2017_05_02_091735_create_warehouse_store_in_challan_product_combinations_table', 1),
(126, '2017_05_02_095400_create_product_images_table', 1),
(127, '2017_05_10_200616_create_temp_product_images_table', 1),
(128, '2017_06_13_082554_create_warehouse_store_out_challan_product_combinations_table', 1),
(129, '2017_07_02_112942_create_customer_order_details_table', 1),
(130, '2017_07_02_132805_create_customer_order_detail_paids_table', 1),
(131, '2017_08_14_113544_create_discount_table', 1),
(132, '2017_08_14_113557_create_discount_type_table', 1),
(133, '2017_08_14_113606_create_discount_code_table', 1),
(134, '2017_08_14_113639_create_discount_type_configuration_outlet_table', 1),
(135, '2017_08_14_113653_create_discount_type_configuration_category_table', 1),
(136, '2017_08_14_113702_create_discount_type_configuration_day_table', 1),
(137, '2017_08_17_091651_create_discount_amount_type_table', 1),
(138, '2017_08_23_065005_create_discount_code_types_table', 1),
(139, '2017_08_23_083525_create_discount_type_configuration_product_table', 1),
(140, '2017_08_23_091055_create_discount_type_configuration_code_table', 1),
(141, '2017_12_03_072935_create_discount_day_table', 2),
(145, '2017_12_11_074807_create_outlet_in_table', 3),
(146, '2017_12_11_074815_create_outlet_out_table', 3),
(147, '2017_12_11_074836_create_invoice_table', 4),
(148, '2017_12_11_082855_remove_unnecessary_table', 4),
(149, '2017_12_13_074312_create_invoice_product_list_table', 4),
(150, '2017_12_17_112303_create_warehouse_in_table', 5),
(151, '2017_12_17_112325_create_warehouse_out_table', 5),
(157, '2017_12_26_093117_create_task_type_table', 6),
(158, '2017_12_26_093420_create_tasks__table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `sequence`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Home', 1, NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(2, 'Register( Cash Drawer )', 2, NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(3, 'Customer', 3, NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(4, 'Sync', 4, NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(5, 'Company Settings', 5, NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(6, 'Users', 6, NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(7, 'Customers', 7, NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(8, 'Catalog', 8, NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(9, 'Inventory', 9, NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(10, 'Discount', 10, NULL, NULL, NULL),
(11, 'invoice', 11, NULL, NULL, NULL),
(12, 'Challan', 12, NULL, NULL, NULL),
(13, 'Sales Report', 13, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0063f931dce93dd39451bc3144d680ff94812f8c7d31a696ecbc39f7cfe069f99a914588b919da5a', 1, 1, NULL, '["*"]', 0, '2017-12-24 03:42:45', '2017-12-24 03:42:45', '2018-01-08 09:42:45'),
('1e8f7a2b3cc82d156849261e2dbe1776a03e08cd5e12a24a56f2162b4f48cf198b3c0b2d94611e5f', 1, 1, NULL, '[]', 0, '2018-01-04 07:53:16', '2018-01-04 07:53:16', '2018-01-19 13:53:16'),
('71812bd4b5bbe6d1bf3efce79d3117dfeccce50c3e120eab8ac0e2ddf947174a622e731f475c2d30', 1, 3, 'ibt-pos', '[]', 0, '2017-12-24 03:30:53', '2017-12-24 03:30:53', '2018-12-24 09:30:53'),
('aa3d2aaaff11e298b36dc846ea8e3d410be782cad0809207597118eee4eca449f53b50cbec4231c7', 2, 1, NULL, '[]', 0, '2018-01-04 11:05:43', '2018-01-04 11:05:43', '2018-01-19 17:05:43'),
('c8188564fa3c7ee293830a96f36ec19da02333e8f1cd1ee4d3d14bfd51ee0aedf0567953bd7fb9df', 1, 1, NULL, '["*"]', 0, '2017-12-26 02:16:37', '2017-12-26 02:16:37', '2018-01-10 08:16:37'),
('e587d464abe8018837befd73ec6ca83912fdab95879f62e6dea44f2e4c3133c79cdea493e32b78bd', 1, 1, NULL, '["*"]', 0, '2017-12-28 01:12:45', '2017-12-28 01:12:45', '2018-01-12 07:12:45');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'PasswordGrantMobile', 'qPEb8z0AwgAwVblcURjC30EUJn3AjxlmZ0ykWAEd', 'http://localhost', 0, 1, 0, '2017-12-21 03:45:04', '2017-12-21 03:45:04'),
(2, 1, 'sajib', 'XCMgsCTAuFG85zcuH5KscqUxCE8QSzuirdhI8s4S', 'http://localhost/auth/callback]', 0, 0, 0, '2017-12-21 04:34:40', '2017-12-21 04:34:40'),
(3, NULL, 'personalAccessClient', 'b0XmXNNxj83bzrcWMS4L072AV5L0m3aVeOB7z9PH', 'http://localhost', 1, 0, 0, '2017-12-21 04:48:34', '2017-12-21 04:48:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 3, '2017-12-21 04:48:34', '2017-12-21 04:48:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('02ea2fa146663dddd9bf694683a810faa8d5a65d01fbe1191a1447d3fa623254c99f38c0bd2eee43', '0063f931dce93dd39451bc3144d680ff94812f8c7d31a696ecbc39f7cfe069f99a914588b919da5a', 0, '2018-01-23 09:42:45'),
('453aa0910f517a8747837d940bc6646933123d5f69f401f09ea0dea61607c6d0871bbd6960096c50', 'e587d464abe8018837befd73ec6ca83912fdab95879f62e6dea44f2e4c3133c79cdea493e32b78bd', 0, '2018-01-27 07:12:45'),
('88915e8c3545643594a9fde9bf297a69ef253651a4bd6eb57d8924c367fde76e0f9cc72ef690a073', 'c8188564fa3c7ee293830a96f36ec19da02333e8f1cd1ee4d3d14bfd51ee0aedf0567953bd7fb9df', 0, '2018-01-25 08:16:37'),
('b2f0a9dcf357163637b51b4106eacef50c564a1e1ddfc72dfd6a754ce3880a3ed030dfead2837747', 'aa3d2aaaff11e298b36dc846ea8e3d410be782cad0809207597118eee4eca449f53b50cbec4231c7', 0, '2018-02-03 17:05:43'),
('bf3ac8c0cfabee17739e897666565464c403d6e4e28ff50fb0e7516c2bb728c66241961bd248082e', '1e8f7a2b3cc82d156849261e2dbe1776a03e08cd5e12a24a56f2162b4f48cf198b3c0b2d94611e5f', 0, '2018-02-03 13:53:16');

-- --------------------------------------------------------

--
-- Table structure for table `outlet_challanins`
--

CREATE TABLE IF NOT EXISTS `outlet_challanins` (
  `id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `outlet_ins`
--

CREATE TABLE IF NOT EXISTS `outlet_ins` (
  `id` int(10) unsigned NOT NULL,
  `outlet_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `product_combination_id` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `warehouse_store_out_challans_id` bigint(20) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `outlet_ins`
--

INSERT INTO `outlet_ins` (`id`, `outlet_id`, `product_id`, `product_combination_id`, `quantity`, `warehouse_store_out_challans_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, 1, 1, 1, 5, 1, '2017-12-19 10:35:47', '2017-12-19 10:35:47', '2017-12-19 10:35:47'),
(4, 1, 2, 3, 1, 1, '2017-12-19 10:35:47', '2017-12-19 10:35:47', '2017-12-19 10:35:47'),
(5, 1, 1, 1, 2, 4, NULL, '2018-01-04 10:49:41', '2018-01-04 10:49:41'),
(6, 1, 1, 2, 2, 4, NULL, '2018-01-04 10:49:41', '2018-01-04 10:49:41'),
(7, 1, 2, 3, 1, 4, NULL, '2018-01-04 10:49:41', '2018-01-04 10:49:41'),
(8, 1, 2, 4, 1, 4, NULL, '2018-01-04 10:49:41', '2018-01-04 10:49:41');

-- --------------------------------------------------------

--
-- Table structure for table `outlet_outs`
--

CREATE TABLE IF NOT EXISTS `outlet_outs` (
  `id` int(10) unsigned NOT NULL,
  `outlet_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `product_combination_id` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `outlet_outs`
--

INSERT INTO `outlet_outs` (`id`, `outlet_id`, `product_id`, `product_combination_id`, `quantity`, `invoice_id`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, '2017-12-19 10:37:05', NULL, '2017-12-19 10:37:05'),
(2, 1, 2, 3, 1, 1, '2017-12-19 10:37:05', NULL, '2017-12-19 10:37:05'),
(3, 1, 2, 2, 2, 4, '2018-01-04 11:31:12', NULL, '2018-01-04 11:31:12');

-- --------------------------------------------------------

--
-- Table structure for table `outlet_products`
--

CREATE TABLE IF NOT EXISTS `outlet_products` (
  `id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `outlet_schedules`
--

CREATE TABLE IF NOT EXISTS `outlet_schedules` (
  `id` int(10) unsigned NOT NULL,
  `schedule_type_id` int(10) unsigned NOT NULL,
  `out_let_id` int(10) unsigned NOT NULL,
  `day` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_hour` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `closing_hour` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `outlet_types`
--

CREATE TABLE IF NOT EXISTS `outlet_types` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `outlet_types`
--

INSERT INTO `outlet_types` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Own', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(2, 'Franchise', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(3, 'Pop-up Store', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(4, 'Dealer', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `out_lets`
--

CREATE TABLE IF NOT EXISTS `out_lets` (
  `id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL DEFAULT '23',
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `outlet_type_id` int(10) unsigned NOT NULL,
  `space` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `capacity` int(11) NOT NULL,
  `display_products` int(11) NOT NULL,
  `advance_amount` decimal(10,2) DEFAULT NULL,
  `monthly_rent` decimal(10,2) NOT NULL,
  `trade_license_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trade_license_photo` text COLLATE utf8mb4_unicode_ci,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsible_person` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `out_lets`
--

INSERT INTO `out_lets` (`id`, `country_id`, `company_id`, `name`, `outlet_type_id`, `space`, `duration`, `capacity`, `display_products`, `advance_amount`, `monthly_rent`, `trade_license_no`, `trade_license_photo`, `start_date`, `end_date`, `location`, `phone`, `address`, `city`, `zip`, `responsible_person`, `notes`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 23, 1, 'Uttara', 1, 1000, 12, 1000, 1000, '100000.00', '10000.00', 'ZXZXCXC', '', '2017-11-22', '2018-11-22', 'uttara', '01756382837', 'asdasd asdasd sda asd das', 'Dhaka', '1220', 'sddsa', 'qeqe', NULL, '2017-11-22 06:21:29', '2017-11-22 06:21:29');

-- --------------------------------------------------------

--
-- Table structure for table `out_let_user`
--

CREATE TABLE IF NOT EXISTS `out_let_user` (
  `user_id` int(10) unsigned NOT NULL,
  `out_let_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `out_let_user`
--

INSERT INTO `out_let_user` (`user_id`, `out_let_id`, `deleted_at`) VALUES
(2, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE IF NOT EXISTS `payment_methods` (
  `id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `module_id`, `name`, `slug`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Access Dashboard', 'access-dashboard', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(2, 1, 'Access My Profile', 'access-my-profile', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(3, 2, 'Access Register Page', 'access-register-page', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(4, 2, 'Access Discount at Register', 'access-discount-at-register', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(5, 2, 'Remove Tax in Register', 'remove-tax-in-register', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(6, 2, 'Allow Refund', 'allow-refund', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(7, 3, 'Create / View Customer', 'create-view-customer', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(8, 3, 'Edit / Update Customer', 'edit-update-customer', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(9, 3, 'Delete Customer', 'delete-customer', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(10, 3, 'Activate / Deactivate Customer', 'activate-deactivate-customer', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(11, 3, 'Create / View Customer Notes', 'create-view-customer-notes', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(12, 3, 'Edit / Update Customer Notes', 'edit-update-customer-notes', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(13, 3, 'Delete Customer Notes', 'delete-customer-notes', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(14, 3, 'Edit credit/balance/reward', 'edit-credit-balance-reward', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(15, 3, 'Access Purchase History', 'access-purchase-history', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(16, 4, 'Sync Challan', 'sync-challan', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(17, 4, 'Sync Sales Report', 'sync-sales-report', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(18, 4, 'Sync All( Challan and Sales Report )', 'sync-all', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(19, 5, 'Access Company info', 'access-company-info', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(20, 5, 'Access Global VAT Settings', 'access-global-vat-settings', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(21, 5, 'Access Global Payment Method Settings', 'access-global-payment-method-settings', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(22, 5, 'Create / View Outlets', 'create-view-outlets', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(23, 5, 'Edit / Update Outlets', 'edit-update-outlets', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(24, 5, 'Delete Outlets', 'delete-outlets', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(25, 5, 'Activate / Deactivate Outlets', 'activate-deactivate-outlets', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(26, 6, 'Access Position / Title', 'access-position-title', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(27, 6, 'Add / Edit Access Permission( Create Permission Name Also\n                            )', 'add-edit-access-permission', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(28, 6, 'Create / View Users', 'create-view-users', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(29, 6, 'Edit / Update User', 'edit-update-user', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(30, 6, 'Delete User', 'delete-user', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(31, 6, 'Activate / Deactivate User', 'activate-deactivate-user', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(32, 7, 'Create / View Customers', 'create-view-customers', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(33, 8, 'Access Category', 'access-category', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(34, 8, 'Create / View Products', 'create-view-products', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(35, 8, 'Edit / Update Products', 'edit-update-products', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(36, 8, 'Delete Products', 'delete-products', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(37, 8, 'Access Product Prices', 'access-product-prices', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(38, 8, 'Access Product Attributes', 'access-product-attributes', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(39, 8, 'Access Product Combination', 'access-product-combination', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(40, 9, 'Access Warehouse', 'access-warehouse', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(41, 9, 'Access Vendor / Supplier', 'access-vendor-supplier', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(42, 9, 'Access Warehouse Store-in', 'access-warehouse-store-in', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(43, 9, 'Access Warehouse Store-out', 'access-warehouse-store-out', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(44, 10, 'Access Discount', 'access-discount-module', NULL, NULL, NULL),
(45, 11, 'Access Invoice', 'access-invoice-module', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `role_id` int(10) unsigned NOT NULL,
  `permission_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`, `deleted_at`) VALUES
(1, 1, NULL),
(2, 1, NULL),
(1, 2, NULL),
(2, 2, NULL),
(1, 3, NULL),
(2, 3, NULL),
(1, 4, NULL),
(2, 4, NULL),
(1, 5, NULL),
(2, 5, NULL),
(1, 6, NULL),
(2, 6, NULL),
(1, 7, NULL),
(2, 7, NULL),
(1, 8, NULL),
(2, 8, NULL),
(1, 9, NULL),
(2, 9, NULL),
(1, 10, NULL),
(2, 10, NULL),
(1, 11, NULL),
(2, 11, NULL),
(1, 12, NULL),
(2, 12, NULL),
(1, 13, NULL),
(2, 13, NULL),
(1, 14, NULL),
(2, 14, NULL),
(1, 15, NULL),
(2, 15, NULL),
(1, 16, NULL),
(2, 16, NULL),
(1, 17, NULL),
(2, 17, NULL),
(1, 18, NULL),
(2, 18, NULL),
(1, 19, NULL),
(2, 19, NULL),
(1, 20, NULL),
(2, 20, NULL),
(1, 21, NULL),
(2, 21, NULL),
(1, 22, NULL),
(2, 22, NULL),
(1, 23, NULL),
(2, 23, NULL),
(1, 24, NULL),
(2, 24, NULL),
(1, 25, NULL),
(2, 25, NULL),
(1, 26, NULL),
(2, 26, NULL),
(1, 27, NULL),
(2, 27, NULL),
(1, 28, NULL),
(2, 28, NULL),
(1, 29, NULL),
(2, 29, NULL),
(1, 30, NULL),
(2, 30, NULL),
(1, 31, NULL),
(2, 31, NULL),
(1, 32, NULL),
(2, 32, NULL),
(1, 33, NULL),
(2, 33, NULL),
(1, 34, NULL),
(2, 34, NULL),
(1, 35, NULL),
(2, 35, NULL),
(1, 36, NULL),
(2, 36, NULL),
(1, 37, NULL),
(2, 37, NULL),
(1, 38, NULL),
(2, 38, NULL),
(1, 39, NULL),
(2, 39, NULL),
(1, 40, NULL),
(2, 40, NULL),
(1, 41, NULL),
(2, 41, NULL),
(1, 42, NULL),
(2, 42, NULL),
(1, 43, NULL),
(2, 43, NULL),
(1, 44, NULL),
(2, 44, NULL),
(1, 45, NULL),
(2, 45, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_code` text COLLATE utf8mb4_unicode_ci,
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '1= Active; 0=Inactive',
  `possition` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `company_id`, `name`, `item_code`, `reference_code`, `active`, `possition`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Ballerina', '000001', 'ZXCZXC', 1, NULL, NULL, '2017-11-22 06:21:53', '2017-11-22 06:21:53'),
(2, 1, 'New Ballerina', '000002', 'mnbvcv', 1, NULL, NULL, '2017-11-26 02:53:34', '2017-11-26 02:53:34'),
(3, 1, 'zzczxc', '000003', 'asdasdasd', 1, NULL, NULL, '2017-11-27 05:02:34', '2017-11-27 05:02:34'),
(4, 1, 'Tesy Cde', '000004', 'MZXNCZXC', 1, NULL, NULL, '2017-12-05 03:05:52', '2017-12-05 03:05:52'),
(6, 1, 'ALLA', '000006', 'ALLAALAA', 1, NULL, NULL, '2017-12-07 04:27:42', '2017-12-07 04:27:42'),
(7, 1, 'ddadasd', '000007', 'asdasdasd', 1, NULL, NULL, '2018-01-05 11:10:20', '2018-01-05 11:10:20'),
(8, 1, 'temporary', '000008', 'lksjlfkshfls', 1, NULL, NULL, '2018-02-03 02:33:58', '2018-02-03 02:33:58');

-- --------------------------------------------------------

--
-- Table structure for table `product_combinations`
--

CREATE TABLE IF NOT EXISTS `product_combinations` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `barcode` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_combinations`
--

INSERT INTO `product_combinations` (`id`, `product_id`, `barcode`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, '000001000001', 'Red-36', NULL, '2017-11-22 06:24:36', '2017-11-22 06:24:36'),
(2, 1, '000001000002', 'Pink-39', NULL, '2017-11-22 06:24:50', '2017-11-22 06:24:50'),
(3, 2, '000002000003', 'Pink 36', NULL, '2017-11-26 02:54:20', '2017-11-26 02:54:20'),
(4, 2, '000002000004', 'Red 39', NULL, '2017-11-26 02:54:45', '2017-11-26 02:54:45'),
(5, 4, '000004000005', 'Pink-39', NULL, '2017-12-05 03:34:34', '2017-12-05 03:34:34'),
(6, 6, '000006000006', 'pink-39', NULL, '2017-12-07 05:54:45', '2017-12-07 05:54:45');

-- --------------------------------------------------------

--
-- Table structure for table `product_combination_attributes`
--

CREATE TABLE IF NOT EXISTS `product_combination_attributes` (
  `id` int(10) unsigned NOT NULL,
  `attribute_value_id` int(10) unsigned NOT NULL,
  `product_combination_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_combination_attributes`
--

INSERT INTO `product_combination_attributes` (`id`, `attribute_value_id`, `product_combination_id`, `created_at`, `updated_at`) VALUES
(1, 4, 1, '2017-11-22 06:24:36', '2017-11-22 06:24:36'),
(2, 1, 1, '2017-11-22 06:24:36', '2017-11-22 06:24:36'),
(3, 3, 2, '2017-11-22 06:24:50', '2017-11-22 06:24:50'),
(4, 2, 2, '2017-11-22 06:24:50', '2017-11-22 06:24:50'),
(5, 3, 3, '2017-11-26 02:54:20', '2017-11-26 02:54:20'),
(6, 1, 3, '2017-11-26 02:54:20', '2017-11-26 02:54:20'),
(7, 4, 4, '2017-11-26 02:54:45', '2017-11-26 02:54:45'),
(8, 2, 4, '2017-11-26 02:54:45', '2017-11-26 02:54:45'),
(9, 3, 5, '2017-12-05 03:34:34', '2017-12-05 03:34:34'),
(10, 2, 5, '2017-12-05 03:34:35', '2017-12-05 03:34:35'),
(11, 3, 6, '2017-12-07 05:54:45', '2017-12-07 05:54:45');

-- --------------------------------------------------------

--
-- Table structure for table `product_discounts`
--

CREATE TABLE IF NOT EXISTS `product_discounts` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `discount_amount` decimal(10,2) DEFAULT NULL,
  `discount_type` tinyint(4) DEFAULT NULL,
  `active_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expire_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_discounts`
--

INSERT INTO `product_discounts` (`id`, `product_id`, `discount_amount`, `discount_type`, `active_date`, `expire_date`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 3, '200.00', 3, '2017-11-26 18:00:00', '2017-11-26 18:00:00', 1, NULL, '2017-11-27 06:43:29', '2017-11-27 06:43:29'),
(2, 3, '50.00', 3, '2017-10-31 18:00:00', '2017-12-28 18:00:00', 1, NULL, '2017-11-29 03:46:54', '2017-11-29 03:46:54'),
(3, 1, '100.00', 3, '2017-11-21 18:00:00', '2017-11-16 18:00:00', 1, NULL, '2017-11-29 05:50:50', '2017-11-29 05:50:50'),
(4, 8, '10.00', 3, '2018-02-05 18:00:00', '2018-02-27 18:00:00', 1, NULL, '2018-02-03 02:35:27', '2018-02-03 02:35:27');

-- --------------------------------------------------------

--
-- Table structure for table `product_discount_outlets`
--

CREATE TABLE IF NOT EXISTS `product_discount_outlets` (
  `product_discount_id` int(10) unsigned NOT NULL,
  `out_let_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_cover_image` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `name`, `is_cover_image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'product_2017-11-22 12:21:59.jpg', 0, NULL, '2017-11-22 06:21:59', '2017-11-22 06:21:59');

-- --------------------------------------------------------

--
-- Table structure for table `product_prices`
--

CREATE TABLE IF NOT EXISTS `product_prices` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `vat_id` int(10) unsigned NOT NULL,
  `price` decimal(10,2) NOT NULL COMMENT 'Selling Price',
  `cost` decimal(10,2) NOT NULL COMMENT 'Production Cost',
  `cost_price_date` date NOT NULL,
  `active` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Inactive' COMMENT '1= Active; 0=Inactive',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_prices`
--

INSERT INTO `product_prices` (`id`, `product_id`, `vat_id`, `price`, `cost`, `cost_price_date`, `active`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '1200.00', '700.00', '2017-11-22', '0', NULL, '2017-11-22 06:22:14', '2018-02-03 02:34:35'),
(2, 2, 1, '1200.00', '600.00', '2017-11-26', '0', NULL, '2017-11-26 02:53:52', '2018-02-03 02:34:35'),
(3, 3, 1, '980.00', '500.00', '2017-11-27', '0', NULL, '2017-11-27 06:25:54', '2018-02-03 02:34:35'),
(4, 4, 1, '399.00', '200.00', '2017-12-05', '0', NULL, '2017-12-05 03:34:04', '2018-02-03 02:34:35'),
(5, 6, 1, '1200.00', '900.00', '2017-12-07', '0', NULL, '2017-12-07 05:49:01', '2018-02-03 02:34:35'),
(6, 8, 1, '1200.00', '900.00', '2018-02-03', '1', NULL, '2018-02-03 02:34:35', '2018-02-03 02:34:35');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `company_id`, `name`, `slug`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Company Administrator( Read Only )', 'administrator', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(2, 1, 'normal', 'normal', NULL, '2018-02-03 02:52:36', '2018-02-03 02:52:36');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`, `deleted_at`) VALUES
(1, 1, NULL),
(2, 1, NULL),
(3, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `id` int(10) unsigned NOT NULL,
  `out_let_id` int(10) unsigned NOT NULL,
  `sales_by` int(10) unsigned NOT NULL COMMENT 'Sales Person',
  `customer_id` int(10) unsigned NOT NULL,
  `discount_coupon_id` int(11) DEFAULT NULL,
  `invoice_prefix_id` int(10) unsigned NOT NULL,
  `invoice_no` bigint(20) NOT NULL,
  `price_exclusive_vat` decimal(10,2) NOT NULL,
  `vat_amount` decimal(10,2) DEFAULT NULL,
  `product_discount_amount` decimal(10,2) DEFAULT NULL,
  `coupon_discount_amount` decimal(10,2) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `credit_card_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit_card_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_received` decimal(10,2) DEFAULT NULL,
  `cash_received` decimal(10,2) DEFAULT NULL,
  `total_received` decimal(10,2) DEFAULT NULL,
  `is_sales_to_member` int(11) NOT NULL DEFAULT '0' COMMENT '1=Sales to member, 0=Sales to non-member',
  `sales_notes` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sale_products`
--

CREATE TABLE IF NOT EXISTS `sale_products` (
  `id` int(10) unsigned NOT NULL,
  `outlet_product_id` int(10) unsigned NOT NULL,
  `sale_id` int(10) unsigned NOT NULL,
  `product_discount_id` int(10) unsigned NOT NULL,
  `product_price_id` int(10) unsigned NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `vat_percent` decimal(10,2) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schedule_types`
--

CREATE TABLE IF NOT EXISTS `schedule_types` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedule_types`
--

INSERT INTO `schedule_types` (`id`, `name`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Regular', 'Here you can describe the schedule purpose', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(2, 'Vacation', 'Here you can describe the schedule purpose', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE IF NOT EXISTS `statuses` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Draft', 'draft', '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(2, 'Pending', 'pending', '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(3, 'Approved', 'approved', '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(4, 'Active', 'active', '2017-11-22 06:18:06', '2017-11-22 06:18:06');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
  `id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` text COLLATE utf8mb4_unicode_ci,
  `city` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(10) unsigned NOT NULL DEFAULT '23',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `company_id`, `name`, `code`, `address`, `address2`, `city`, `zip`, `country_id`, `email`, `phone`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Supplier', 'asdas', 'asdasd dsdsada  sdasdasd', 'asdasd sdasdasd sdasd', 'asdas', '1216', 23, 'sup@sup.com', '01434572730', NULL, '2017-11-22 06:19:52', '2017-11-22 06:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `task_type_id` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `warehouse_store_out_challan_id` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `name`, `task_type_id`, `outlet_id`, `warehouse_store_out_challan_id`, `discount_id`, `product_id`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Challan', 1, 1, 4, NULL, NULL, 1, NULL, NULL, '2018-01-04 10:49:41');

-- --------------------------------------------------------

--
-- Table structure for table `task_types`
--

CREATE TABLE IF NOT EXISTS `task_types` (
  `id` int(10) unsigned NOT NULL,
  `task_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `task_types`
--

INSERT INTO `task_types` (`id`, `task_name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Challan', NULL, NULL, NULL),
(2, 'Discount Rules', NULL, NULL, NULL),
(3, 'Product Info,Price,Discount', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tax_categories`
--

CREATE TABLE IF NOT EXISTS `tax_categories` (
  `id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tax_category_values`
--

CREATE TABLE IF NOT EXISTS `tax_category_values` (
  `id` int(10) unsigned NOT NULL,
  `tax_category_id` int(10) unsigned NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `percent` decimal(10,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `time_zones`
--

CREATE TABLE IF NOT EXISTS `time_zones` (
  `id` int(10) unsigned NOT NULL,
  `location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gmt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=420 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `time_zones`
--

INSERT INTO `time_zones` (`id`, `location`, `gmt`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Pacific/Midway', '(UTC-11:00) Midway', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(2, 'Pacific/Niue', '(UTC-11:00) Niue', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(3, 'Pacific/Pago_Pago', '(UTC-11:00) Pago Pago', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(4, 'America/Adak', '(UTC-10:00) Adak', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(5, 'Pacific/Honolulu', '(UTC-10:00) Honolulu', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(6, 'Pacific/Johnston', '(UTC-10:00) Johnston', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(7, 'Pacific/Rarotonga', '(UTC-10:00) Rarotonga', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(8, 'Pacific/Tahiti', '(UTC-10:00) Tahiti', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(9, 'Pacific/Marquesas', '(UTC-09:30) Marquesas', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(10, 'America/Anchorage', '(UTC-09:00) Anchorage', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(11, 'Pacific/Gambier', '(UTC-09:00) Gambier', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(12, 'America/Juneau', '(UTC-09:00) Juneau', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(13, 'America/Nome', '(UTC-09:00) Nome', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(14, 'America/Sitka', '(UTC-09:00) Sitka', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(15, 'America/Yakutat', '(UTC-09:00) Yakutat', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(16, 'America/Dawson', '(UTC-08:00) Dawson', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(17, 'America/Los_Angeles', '(UTC-08:00) Los Angeles', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(18, 'America/Metlakatla', '(UTC-08:00) Metlakatla', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(19, 'Pacific/Pitcairn', '(UTC-08:00) Pitcairn', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(20, 'America/Santa_Isabel', '(UTC-08:00) Santa Isabel', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(21, 'America/Tijuana', '(UTC-08:00) Tijuana', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(22, 'America/Vancouver', '(UTC-08:00) Vancouver', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(23, 'America/Whitehorse', '(UTC-08:00) Whitehorse', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(24, 'America/Boise', '(UTC-07:00) Boise', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(25, 'America/Cambridge_Bay', '(UTC-07:00) Cambridge Bay', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(26, 'America/Chihuahua', '(UTC-07:00) Chihuahua', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(27, 'America/Creston', '(UTC-07:00) Creston', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(28, 'America/Dawson_Creek', '(UTC-07:00) Dawson Creek', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(29, 'America/Denver', '(UTC-07:00) Denver', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(30, 'America/Edmonton', '(UTC-07:00) Edmonton', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(31, 'America/Hermosillo', '(UTC-07:00) Hermosillo', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(32, 'America/Inuvik', '(UTC-07:00) Inuvik', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(33, 'America/Mazatlan', '(UTC-07:00) Mazatlan', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(34, 'America/Ojinaga', '(UTC-07:00) Ojinaga', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(35, 'America/Phoenix', '(UTC-07:00) Phoenix', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(36, 'America/Shiprock', '(UTC-07:00) Shiprock', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(37, 'America/Yellowknife', '(UTC-07:00) Yellowknife', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(38, 'America/Bahia_Banderas', '(UTC-06:00) Bahia Banderas', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(39, 'America/Belize', '(UTC-06:00) Belize', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(40, 'America/North_Dakota/Beulah', '(UTC-06:00) Beulah', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(41, 'America/Cancun', '(UTC-06:00) Cancun', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(42, 'America/North_Dakota/Center', '(UTC-06:00) Center', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(43, 'America/Chicago', '(UTC-06:00) Chicago', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(44, 'America/Costa_Rica', '(UTC-06:00) Costa Rica', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(45, 'Pacific/Easter', '(UTC-06:00) Easter', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(46, 'America/El_Salvador', '(UTC-06:00) El Salvador', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(47, 'Pacific/Galapagos', '(UTC-06:00) Galapagos', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(48, 'America/Guatemala', '(UTC-06:00) Guatemala', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(49, 'America/Indiana/Knox', '(UTC-06:00) Knox', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(50, 'America/Managua', '(UTC-06:00) Managua', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(51, 'America/Matamoros', '(UTC-06:00) Matamoros', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(52, 'America/Menominee', '(UTC-06:00) Menominee', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(53, 'America/Merida', '(UTC-06:00) Merida', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(54, 'America/Mexico_City', '(UTC-06:00) Mexico City', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(55, 'America/Monterrey', '(UTC-06:00) Monterrey', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(56, 'America/North_Dakota/New_Salem', '(UTC-06:00) New Salem', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(57, 'America/Rainy_River', '(UTC-06:00) Rainy River', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(58, 'America/Rankin_Inlet', '(UTC-06:00) Rankin Inlet', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(59, 'America/Regina', '(UTC-06:00) Regina', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(60, 'America/Resolute', '(UTC-06:00) Resolute', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(61, 'America/Swift_Current', '(UTC-06:00) Swift Current', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(62, 'America/Tegucigalpa', '(UTC-06:00) Tegucigalpa', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(63, 'America/Indiana/Tell_City', '(UTC-06:00) Tell City', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(64, 'America/Winnipeg', '(UTC-06:00) Winnipeg', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(65, 'America/Atikokan', '(UTC-05:00) Atikokan', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(66, 'America/Bogota', '(UTC-05:00) Bogota', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(67, 'America/Cayman', '(UTC-05:00) Cayman', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(68, 'America/Detroit', '(UTC-05:00) Detroit', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(69, 'America/Grand_Turk', '(UTC-05:00) Grand Turk', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(70, 'America/Guayaquil', '(UTC-05:00) Guayaquil', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(71, 'America/Havana', '(UTC-05:00) Havana', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(72, 'America/Indiana/Indianapolis', '(UTC-05:00) Indianapolis', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(73, 'America/Iqaluit', '(UTC-05:00) Iqaluit', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(74, 'America/Jamaica', '(UTC-05:00) Jamaica', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(75, 'America/Lima', '(UTC-05:00) Lima', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(76, 'America/Kentucky/Louisville', '(UTC-05:00) Louisville', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(77, 'America/Indiana/Marengo', '(UTC-05:00) Marengo', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(78, 'America/Kentucky/Monticello', '(UTC-05:00) Monticello', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(79, 'America/Montreal', '(UTC-05:00) Montreal', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(80, 'America/Nassau', '(UTC-05:00) Nassau', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(81, 'America/New_York', '(UTC-05:00) New York', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(82, 'America/Nipigon', '(UTC-05:00) Nipigon', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(83, 'America/Panama', '(UTC-05:00) Panama', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(84, 'America/Pangnirtung', '(UTC-05:00) Pangnirtung', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(85, 'America/Indiana/Petersburg', '(UTC-05:00) Petersburg', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(86, 'America/Port-au-Prince', '(UTC-05:00) Port-au-Prince', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(87, 'America/Thunder_Bay', '(UTC-05:00) Thunder Bay', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(88, 'America/Toronto', '(UTC-05:00) Toronto', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(89, 'America/Indiana/Vevay', '(UTC-05:00) Vevay', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(90, 'America/Indiana/Vincennes', '(UTC-05:00) Vincennes', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(91, 'America/Indiana/Winamac', '(UTC-05:00) Winamac', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(92, 'America/Caracas', '(UTC-04:30) Caracas', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(93, 'America/Anguilla', '(UTC-04:00) Anguilla', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(94, 'America/Antigua', '(UTC-04:00) Antigua', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(95, 'America/Aruba', '(UTC-04:00) Aruba', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(96, 'America/Asuncion', '(UTC-04:00) Asuncion', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(97, 'America/Barbados', '(UTC-04:00) Barbados', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(98, 'Atlantic/Bermuda', '(UTC-04:00) Bermuda', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(99, 'America/Blanc-Sablon', '(UTC-04:00) Blanc-Sablon', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(100, 'America/Boa_Vista', '(UTC-04:00) Boa Vista', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(101, 'America/Campo_Grande', '(UTC-04:00) Campo Grande', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(102, 'America/Cuiaba', '(UTC-04:00) Cuiaba', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(103, 'America/Curacao', '(UTC-04:00) Curacao', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(104, 'America/Dominica', '(UTC-04:00) Dominica', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(105, 'America/Eirunepe', '(UTC-04:00) Eirunepe', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(106, 'America/Glace_Bay', '(UTC-04:00) Glace Bay', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(107, 'America/Goose_Bay', '(UTC-04:00) Goose Bay', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(108, 'America/Grenada', '(UTC-04:00) Grenada', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(109, 'America/Guadeloupe', '(UTC-04:00) Guadeloupe', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(110, 'America/Guyana', '(UTC-04:00) Guyana', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(111, 'America/Halifax', '(UTC-04:00) Halifax', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(112, 'America/Kralendijk', '(UTC-04:00) Kralendijk', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(113, 'America/La_Paz', '(UTC-04:00) La Paz', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(114, 'America/Lower_Princes', '(UTC-04:00) Lower Princes', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(115, 'America/Manaus', '(UTC-04:00) Manaus', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(116, 'America/Marigot', '(UTC-04:00) Marigot', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(117, 'America/Martinique', '(UTC-04:00) Martinique', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(118, 'America/Moncton', '(UTC-04:00) Moncton', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(119, 'America/Montserrat', '(UTC-04:00) Montserrat', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(120, 'Antarctica/Palmer', '(UTC-04:00) Palmer', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(121, 'America/Port_of_Spain', '(UTC-04:00) Port of Spain', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(122, 'America/Porto_Velho', '(UTC-04:00) Porto Velho', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(123, 'America/Puerto_Rico', '(UTC-04:00) Puerto Rico', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(124, 'America/Rio_Branco', '(UTC-04:00) Rio Branco', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(125, 'America/Santiago', '(UTC-04:00) Santiago', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(126, 'America/Santo_Domingo', '(UTC-04:00) Santo Domingo', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(127, 'America/St_Barthelemy', '(UTC-04:00) St. Barthelemy', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(128, 'America/St_Kitts', '(UTC-04:00) St. Kitts', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(129, 'America/St_Lucia', '(UTC-04:00) St. Lucia', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(130, 'America/St_Thomas', '(UTC-04:00) St. Thomas', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(131, 'America/St_Vincent', '(UTC-04:00) St. Vincent', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(132, 'America/Thule', '(UTC-04:00) Thule', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(133, 'America/Tortola', '(UTC-04:00) Tortola', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(134, 'America/St_Johns', '(UTC-03:30) St. Johns', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(135, 'America/Araguaina', '(UTC-03:00) Araguaina', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(136, 'America/Bahia', '(UTC-03:00) Bahia', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(137, 'America/Belem', '(UTC-03:00) Belem', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(138, 'America/Argentina/Buenos_Aires', '(UTC-03:00) Buenos Aires', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(139, 'America/Argentina/Catamarca', '(UTC-03:00) Catamarca', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(140, 'America/Cayenne', '(UTC-03:00) Cayenne', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(141, 'America/Argentina/Cordoba', '(UTC-03:00) Cordoba', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(142, 'America/Fortaleza', '(UTC-03:00) Fortaleza', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(143, 'America/Godthab', '(UTC-03:00) Godthab', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(144, 'America/Argentina/Jujuy', '(UTC-03:00) Jujuy', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(145, 'America/Argentina/La_Rioja', '(UTC-03:00) La Rioja', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(146, 'America/Maceio', '(UTC-03:00) Maceio', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(147, 'America/Argentina/Mendoza', '(UTC-03:00) Mendoza', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(148, 'America/Miquelon', '(UTC-03:00) Miquelon', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(149, 'America/Montevideo', '(UTC-03:00) Montevideo', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(150, 'America/Paramaribo', '(UTC-03:00) Paramaribo', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(151, 'America/Recife', '(UTC-03:00) Recife', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(152, 'America/Argentina/Rio_Gallegos', '(UTC-03:00) Rio Gallegos', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(153, 'Antarctica/Rothera', '(UTC-03:00) Rothera', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(154, 'America/Argentina/Salta', '(UTC-03:00) Salta', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(155, 'America/Argentina/San_Juan', '(UTC-03:00) San Juan', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(156, 'America/Argentina/San_Luis', '(UTC-03:00) San Luis', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(157, 'America/Santarem', '(UTC-03:00) Santarem', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(158, 'America/Sao_Paulo', '(UTC-03:00) Sao Paulo', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(159, 'Atlantic/Stanley', '(UTC-03:00) Stanley', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(160, 'America/Argentina/Tucuman', '(UTC-03:00) Tucuman', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(161, 'America/Argentina/Ushuaia', '(UTC-03:00) Ushuaia', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(162, 'America/Noronha', '(UTC-02:00) Noronha', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(163, 'Atlantic/South_Georgia', '(UTC-02:00) South Georgia', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(164, 'Atlantic/Azores', '(UTC-01:00) Azores', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(165, 'Atlantic/Cape_Verde', '(UTC-01:00) Cape Verde', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(166, 'America/Scoresbysund', '(UTC-01:00) Scoresbysund', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(167, 'Africa/Abidjan', '(UTC+00:00) Abidjan', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(168, 'Africa/Accra', '(UTC+00:00) Accra', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(169, 'Africa/Bamako', '(UTC+00:00) Bamako', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(170, 'Africa/Banjul', '(UTC+00:00) Banjul', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(171, 'Africa/Bissau', '(UTC+00:00) Bissau', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(172, 'Atlantic/Canary', '(UTC+00:00) Canary', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(173, 'Africa/Casablanca', '(UTC+00:00) Casablanca', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(174, 'Africa/Conakry', '(UTC+00:00) Conakry', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(175, 'Africa/Dakar', '(UTC+00:00) Dakar', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(176, 'America/Danmarkshavn', '(UTC+00:00) Danmarkshavn', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(177, 'Europe/Dublin', '(UTC+00:00) Dublin', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(178, 'Africa/El_Aaiun', '(UTC+00:00) El Aaiun', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(179, 'Atlantic/Faroe', '(UTC+00:00) Faroe', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(180, 'Africa/Freetown', '(UTC+00:00) Freetown', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(181, 'Europe/Guernsey', '(UTC+00:00) Guernsey', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(182, 'Europe/Isle_of_Man', '(UTC+00:00) Isle of Man', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(183, 'Europe/Jersey', '(UTC+00:00) Jersey', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(184, 'Europe/Lisbon', '(UTC+00:00) Lisbon', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(185, 'Africa/Lome', '(UTC+00:00) Lome', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(186, 'Europe/London', '(UTC+00:00) London', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(187, 'Atlantic/Madeira', '(UTC+00:00) Madeira', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(188, 'Africa/Monrovia', '(UTC+00:00) Monrovia', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(189, 'Africa/Nouakchott', '(UTC+00:00) Nouakchott', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(190, 'Africa/Ouagadougou', '(UTC+00:00) Ouagadougou', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(191, 'Atlantic/Reykjavik', '(UTC+00:00) Reykjavik', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(192, 'Africa/Sao_Tome', '(UTC+00:00) Sao Tome', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(193, 'Atlantic/St_Helena', '(UTC+00:00) St. Helena', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(194, 'UTC', '(UTC+00:00) UTC', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(195, 'Africa/Algiers', '(UTC+01:00) Algiers', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(196, 'Europe/Amsterdam', '(UTC+01:00) Amsterdam', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(197, 'Europe/Andorra', '(UTC+01:00) Andorra', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(198, 'Africa/Bangui', '(UTC+01:00) Bangui', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(199, 'Europe/Belgrade', '(UTC+01:00) Belgrade', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(200, 'Europe/Berlin', '(UTC+01:00) Berlin', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(201, 'Europe/Bratislava', '(UTC+01:00) Bratislava', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(202, 'Africa/Brazzaville', '(UTC+01:00) Brazzaville', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(203, 'Europe/Brussels', '(UTC+01:00) Brussels', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(204, 'Europe/Budapest', '(UTC+01:00) Budapest', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(205, 'Europe/Busingen', '(UTC+01:00) Busingen', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(206, 'Africa/Ceuta', '(UTC+01:00) Ceuta', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(207, 'Europe/Copenhagen', '(UTC+01:00) Copenhagen', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(208, 'Africa/Douala', '(UTC+01:00) Douala', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(209, 'Europe/Gibraltar', '(UTC+01:00) Gibraltar', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(210, 'Africa/Kinshasa', '(UTC+01:00) Kinshasa', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(211, 'Africa/Lagos', '(UTC+01:00) Lagos', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(212, 'Africa/Libreville', '(UTC+01:00) Libreville', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(213, 'Europe/Ljubljana', '(UTC+01:00) Ljubljana', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(214, 'Arctic/Longyearbyen', '(UTC+01:00) Longyearbyen', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(215, 'Africa/Luanda', '(UTC+01:00) Luanda', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(216, 'Europe/Luxembourg', '(UTC+01:00) Luxembourg', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(217, 'Europe/Madrid', '(UTC+01:00) Madrid', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(218, 'Africa/Malabo', '(UTC+01:00) Malabo', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(219, 'Europe/Malta', '(UTC+01:00) Malta', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(220, 'Europe/Monaco', '(UTC+01:00) Monaco', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(221, 'Africa/Ndjamena', '(UTC+01:00) Ndjamena', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(222, 'Africa/Niamey', '(UTC+01:00) Niamey', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(223, 'Europe/Oslo', '(UTC+01:00) Oslo', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(224, 'Europe/Paris', '(UTC+01:00) Paris', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(225, 'Europe/Podgorica', '(UTC+01:00) Podgorica', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(226, 'Africa/Porto-Novo', '(UTC+01:00) Porto-Novo', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(227, 'Europe/Prague', '(UTC+01:00) Prague', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(228, 'Europe/Rome', '(UTC+01:00) Rome', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(229, 'Europe/San_Marino', '(UTC+01:00) San Marino', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(230, 'Europe/Sarajevo', '(UTC+01:00) Sarajevo', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(231, 'Europe/Skopje', '(UTC+01:00) Skopje', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(232, 'Europe/Stockholm', '(UTC+01:00) Stockholm', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(233, 'Europe/Tirane', '(UTC+01:00) Tirane', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(234, 'Africa/Tripoli', '(UTC+01:00) Tripoli', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(235, 'Africa/Tunis', '(UTC+01:00) Tunis', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(236, 'Europe/Vaduz', '(UTC+01:00) Vaduz', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(237, 'Europe/Vatican', '(UTC+01:00) Vatican', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(238, 'Europe/Vienna', '(UTC+01:00) Vienna', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(239, 'Europe/Warsaw', '(UTC+01:00) Warsaw', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(240, 'Africa/Windhoek', '(UTC+01:00) Windhoek', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(241, 'Europe/Zagreb', '(UTC+01:00) Zagreb', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(242, 'Europe/Zurich', '(UTC+01:00) Zurich', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(243, 'Europe/Athens', '(UTC+02:00) Athens', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(244, 'Asia/Beirut', '(UTC+02:00) Beirut', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(245, 'Africa/Blantyre', '(UTC+02:00) Blantyre', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(246, 'Europe/Bucharest', '(UTC+02:00) Bucharest', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(247, 'Africa/Bujumbura', '(UTC+02:00) Bujumbura', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(248, 'Africa/Cairo', '(UTC+02:00) Cairo', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(249, 'Europe/Chisinau', '(UTC+02:00) Chisinau', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(250, 'Asia/Damascus', '(UTC+02:00) Damascus', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(251, 'Africa/Gaborone', '(UTC+02:00) Gaborone', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(252, 'Asia/Gaza', '(UTC+02:00) Gaza', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(253, 'Africa/Harare', '(UTC+02:00) Harare', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(254, 'Asia/Hebron', '(UTC+02:00) Hebron', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(255, 'Europe/Helsinki', '(UTC+02:00) Helsinki', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(256, 'Europe/Istanbul', '(UTC+02:00) Istanbul', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(257, 'Asia/Jerusalem', '(UTC+02:00) Jerusalem', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(258, 'Africa/Johannesburg', '(UTC+02:00) Johannesburg', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(259, 'Europe/Kiev', '(UTC+02:00) Kiev', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(260, 'Africa/Kigali', '(UTC+02:00) Kigali', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(261, 'Africa/Lubumbashi', '(UTC+02:00) Lubumbashi', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(262, 'Africa/Lusaka', '(UTC+02:00) Lusaka', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(263, 'Africa/Maputo', '(UTC+02:00) Maputo', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(264, 'Europe/Mariehamn', '(UTC+02:00) Mariehamn', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(265, 'Africa/Maseru', '(UTC+02:00) Maseru', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(266, 'Africa/Mbabane', '(UTC+02:00) Mbabane', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(267, 'Asia/Nicosia', '(UTC+02:00) Nicosia', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(268, 'Europe/Riga', '(UTC+02:00) Riga', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(269, 'Europe/Simferopol', '(UTC+02:00) Simferopol', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(270, 'Europe/Sofia', '(UTC+02:00) Sofia', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(271, 'Europe/Tallinn', '(UTC+02:00) Tallinn', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(272, 'Europe/Uzhgorod', '(UTC+02:00) Uzhgorod', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(273, 'Europe/Vilnius', '(UTC+02:00) Vilnius', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(274, 'Europe/Zaporozhye', '(UTC+02:00) Zaporozhye', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(275, 'Africa/Addis_Ababa', '(UTC+03:00) Addis Ababa', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(276, 'Asia/Aden', '(UTC+03:00) Aden', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(277, 'Asia/Amman', '(UTC+03:00) Amman', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(278, 'Indian/Antananarivo', '(UTC+03:00) Antananarivo', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(279, 'Africa/Asmara', '(UTC+03:00) Asmara', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(280, 'Asia/Baghdad', '(UTC+03:00) Baghdad', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(281, 'Asia/Bahrain', '(UTC+03:00) Bahrain', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(282, 'Indian/Comoro', '(UTC+03:00) Comoro', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(283, 'Africa/Dar_es_Salaam', '(UTC+03:00) Dar es Salaam', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(284, 'Africa/Djibouti', '(UTC+03:00) Djibouti', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(285, 'Africa/Juba', '(UTC+03:00) Juba', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(286, 'Europe/Kaliningrad', '(UTC+03:00) Kaliningrad', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(287, 'Africa/Kampala', '(UTC+03:00) Kampala', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(288, 'Africa/Khartoum', '(UTC+03:00) Khartoum', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(289, 'Asia/Kuwait', '(UTC+03:00) Kuwait', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(290, 'Indian/Mayotte', '(UTC+03:00) Mayotte', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(291, 'Europe/Minsk', '(UTC+03:00) Minsk', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(292, 'Africa/Mogadishu', '(UTC+03:00) Mogadishu', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(293, 'Africa/Nairobi', '(UTC+03:00) Nairobi', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(294, 'Asia/Qatar', '(UTC+03:00) Qatar', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(295, 'Asia/Riyadh', '(UTC+03:00) Riyadh', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(296, 'Antarctica/Syowa', '(UTC+03:00) Syowa', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(297, 'Asia/Tehran', '(UTC+03:30) Tehran', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(298, 'Asia/Baku', '(UTC+04:00) Baku', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(299, 'Asia/Dubai', '(UTC+04:00) Dubai', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(300, 'Indian/Mahe', '(UTC+04:00) Mahe', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(301, 'Indian/Mauritius', '(UTC+04:00) Mauritius', NULL, '2017-11-22 06:18:06', '2017-11-22 06:18:06'),
(302, 'Europe/Moscow', '(UTC+04:00) Moscow', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(303, 'Asia/Muscat', '(UTC+04:00) Muscat', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(304, 'Indian/Reunion', '(UTC+04:00) Reunion', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(305, 'Europe/Samara', '(UTC+04:00) Samara', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(306, 'Asia/Tbilisi', '(UTC+04:00) Tbilisi', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(307, 'Europe/Volgograd', '(UTC+04:00) Volgograd', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(308, 'Asia/Yerevan', '(UTC+04:00) Yerevan', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(309, 'Asia/Kabul', '(UTC+04:30) Kabul', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(310, 'Asia/Aqtau', '(UTC+05:00) Aqtau', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(311, 'Asia/Aqtobe', '(UTC+05:00) Aqtobe', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(312, 'Asia/Ashgabat', '(UTC+05:00) Ashgabat', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(313, 'Asia/Dushanbe', '(UTC+05:00) Dushanbe', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(314, 'Asia/Karachi', '(UTC+05:00) Karachi', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(315, 'Indian/Kerguelen', '(UTC+05:00) Kerguelen', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(316, 'Indian/Maldives', '(UTC+05:00) Maldives', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(317, 'Antarctica/Mawson', '(UTC+05:00) Mawson', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(318, 'Asia/Oral', '(UTC+05:00) Oral', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(319, 'Asia/Samarkand', '(UTC+05:00) Samarkand', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(320, 'Asia/Tashkent', '(UTC+05:00) Tashkent', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(321, 'Asia/Colombo', '(UTC+05:30) Colombo', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(322, 'Asia/Kolkata', '(UTC+05:30) Kolkata', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(323, 'Asia/Kathmandu', '(UTC+05:45) Kathmandu', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(324, 'Asia/Almaty', '(UTC+06:00) Almaty', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(325, 'Asia/Bishkek', '(UTC+06:00) Bishkek', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(326, 'Indian/Chagos', '(UTC+06:00) Chagos', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(327, 'Asia/Dhaka', '(UTC+06:00) Dhaka', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(328, 'Asia/Qyzylorda', '(UTC+06:00) Qyzylorda', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(329, 'Asia/Thimphu', '(UTC+06:00) Thimphu', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(330, 'Antarctica/Vostok', '(UTC+06:00) Vostok', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(331, 'Asia/Yekaterinburg', '(UTC+06:00) Yekaterinburg', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(332, 'Indian/Cocos', '(UTC+06:30) Cocos', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(333, 'Asia/Rangoon', '(UTC+06:30) Rangoon', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(334, 'Asia/Bangkok', '(UTC+07:00) Bangkok', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(335, 'Indian/Christmas', '(UTC+07:00) Christmas', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(336, 'Antarctica/Davis', '(UTC+07:00) Davis', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(337, 'Asia/Ho_Chi_Minh', '(UTC+07:00) Ho Chi Minh', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(338, 'Asia/Hovd', '(UTC+07:00) Hovd', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(339, 'Asia/Jakarta', '(UTC+07:00) Jakarta', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(340, 'Asia/Novokuznetsk', '(UTC+07:00) Novokuznetsk', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(341, 'Asia/Novosibirsk', '(UTC+07:00) Novosibirsk', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(342, 'Asia/Omsk', '(UTC+07:00) Omsk', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(343, 'Asia/Phnom_Penh', '(UTC+07:00) Phnom Penh', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(344, 'Asia/Pontianak', '(UTC+07:00) Pontianak', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(345, 'Asia/Vientiane', '(UTC+07:00) Vientiane', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(346, 'Asia/Brunei', '(UTC+08:00) Brunei', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(347, 'Antarctica/Casey', '(UTC+08:00) Casey', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(348, 'Asia/Choibalsan', '(UTC+08:00) Choibalsan', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(349, 'Asia/Chongqing', '(UTC+08:00) Chongqing', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(350, 'Asia/Harbin', '(UTC+08:00) Harbin', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(351, 'Asia/Hong_Kong', '(UTC+08:00) Hong Kong', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(352, 'Asia/Kashgar', '(UTC+08:00) Kashgar', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(353, 'Asia/Krasnoyarsk', '(UTC+08:00) Krasnoyarsk', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(354, 'Asia/Kuala_Lumpur', '(UTC+08:00) Kuala Lumpur', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(355, 'Asia/Kuching', '(UTC+08:00) Kuching', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(356, 'Asia/Macau', '(UTC+08:00) Macau', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(357, 'Asia/Makassar', '(UTC+08:00) Makassar', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(358, 'Asia/Manila', '(UTC+08:00) Manila', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(359, 'Australia/Perth', '(UTC+08:00) Perth', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(360, 'Asia/Shanghai', '(UTC+08:00) Shanghai', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(361, 'Asia/Singapore', '(UTC+08:00) Singapore', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(362, 'Asia/Taipei', '(UTC+08:00) Taipei', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(363, 'Asia/Ulaanbaatar', '(UTC+08:00) Ulaanbaatar', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(364, 'Asia/Urumqi', '(UTC+08:00) Urumqi', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(365, 'Australia/Eucla', '(UTC+08:45) Eucla', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(366, 'Asia/Dili', '(UTC+09:00) Dili', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(367, 'Asia/Irkutsk', '(UTC+09:00) Irkutsk', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(368, 'Asia/Jayapura', '(UTC+09:00) Jayapura', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(369, 'Pacific/Palau', '(UTC+09:00) Palau', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(370, 'Asia/Pyongyang', '(UTC+09:00) Pyongyang', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(371, 'Asia/Seoul', '(UTC+09:00) Seoul', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(372, 'Asia/Tokyo', '(UTC+09:00) Tokyo', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(373, 'Australia/Adelaide', '(UTC+09:30) Adelaide', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(374, 'Australia/Broken_Hill', '(UTC+09:30) Broken Hill', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(375, 'Australia/Darwin', '(UTC+09:30) Darwin', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(376, 'Australia/Brisbane', '(UTC+10:00) Brisbane', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(377, 'Pacific/Chuuk', '(UTC+10:00) Chuuk', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(378, 'Australia/Currie', '(UTC+10:00) Currie', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(379, 'Antarctica/DumontDUrville', '(UTC+10:00) DumontDUrville', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(380, 'Pacific/Guam', '(UTC+10:00) Guam', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(381, 'Australia/Hobart', '(UTC+10:00) Hobart', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(382, 'Asia/Khandyga', '(UTC+10:00) Khandyga', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(383, 'Australia/Lindeman', '(UTC+10:00) Lindeman', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(384, 'Australia/Melbourne', '(UTC+10:00) Melbourne', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(385, 'Pacific/Port_Moresby', '(UTC+10:00) Port Moresby', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(386, 'Pacific/Saipan', '(UTC+10:00) Saipan', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(387, 'Australia/Sydney', '(UTC+10:00) Sydney', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(388, 'Asia/Yakutsk', '(UTC+10:00) Yakutsk', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(389, 'Australia/Lord_Howe', '(UTC+10:30) Lord Howe', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(390, 'Pacific/Efate', '(UTC+11:00) Efate', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(391, 'Pacific/Guadalcanal', '(UTC+11:00) Guadalcanal', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(392, 'Pacific/Kosrae', '(UTC+11:00) Kosrae', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(393, 'Antarctica/Macquarie', '(UTC+11:00) Macquarie', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(394, 'Pacific/Noumea', '(UTC+11:00) Noumea', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(395, 'Pacific/Pohnpei', '(UTC+11:00) Pohnpei', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(396, 'Asia/Sakhalin', '(UTC+11:00) Sakhalin', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(397, 'Asia/Ust-Nera', '(UTC+11:00) Ust-Nera', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(398, 'Asia/Vladivostok', '(UTC+11:00) Vladivostok', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(399, 'Pacific/Norfolk', '(UTC+11:30) Norfolk', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(400, 'Asia/Anadyr', '(UTC+12:00) Anadyr', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(401, 'Pacific/Auckland', '(UTC+12:00) Auckland', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(402, 'Pacific/Fiji', '(UTC+12:00) Fiji', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(403, 'Pacific/Funafuti', '(UTC+12:00) Funafuti', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(404, 'Asia/Kamchatka', '(UTC+12:00) Kamchatka', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(405, 'Pacific/Kwajalein', '(UTC+12:00) Kwajalein', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(406, 'Asia/Magadan', '(UTC+12:00) Magadan', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(407, 'Pacific/Majuro', '(UTC+12:00) Majuro', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(408, 'Antarctica/McMurdo', '(UTC+12:00) McMurdo', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(409, 'Pacific/Nauru', '(UTC+12:00) Nauru', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(410, 'Antarctica/South_Pole', '(UTC+12:00) South Pole', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(411, 'Pacific/Tarawa', '(UTC+12:00) Tarawa', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(412, 'Pacific/Wake', '(UTC+12:00) Wake', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(413, 'Pacific/Wallis', '(UTC+12:00) Wallis', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(414, 'Pacific/Chatham', '(UTC+12:45) Chatham', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(415, 'Pacific/Apia', '(UTC+13:00) Apia', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(416, 'Pacific/Enderbury', '(UTC+13:00) Enderbury', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(417, 'Pacific/Fakaofo', '(UTC+13:00) Fakaofo', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(418, 'Pacific/Tongatapu', '(UTC+13:00) Tongatapu', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(419, 'Pacific/Kiritimati', '(UTC+14:00) Kiritimati', NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_pos_admin` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `company_id`, `name`, `email`, `password`, `is_pos_admin`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'sajib@bluebd.com', '$2y$10$wtUlc2Rx5j3z8/e5moNOVu/gDjNxCtTvXchUD3d4fY9KF3E9PSlAC', 1, NULL, '2M0079GzAOylRCpJqB6PyN2oPQM0YwiHLB6k2mSWRyBKMFGLoKgBGBd7GMGs', '2017-11-22 06:18:07', '2017-11-22 06:18:07'),
(2, 1, 'salesman', 'salesman@fortunabd.com', '$2y$10$wtUlc2Rx5j3z8/e5moNOVu/gDjNxCtTvXchUD3d4fY9KF3E9PSlAC', 0, NULL, NULL, '2017-12-13 03:42:56', '2017-12-13 03:42:56'),
(3, 1, 'something', 'ah@ah.com', '$2y$10$pMgYFS9qvFMH1IsBp.gbKekVa0cbUQjWi0fJ8RzLCJ3qz1mMUloue', 1, NULL, NULL, '2018-02-03 02:51:48', '2018-02-03 02:51:48');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE IF NOT EXISTS `user_details` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL DEFAULT '23',
  `user_title_id` int(10) unsigned DEFAULT NULL,
  `photo` blob,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `user_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cell_phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `address2` text COLLATE utf8mb4_unicode_ci,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `country_id`, `user_title_id`, `photo`, `first_name`, `last_name`, `date_of_birth`, `user_code`, `cell_phone`, `work_phone`, `address`, `address2`, `city`, `zip`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 23, NULL, NULL, 'Sajib', 'Ahasan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-22 06:18:07', '2017-11-22 06:18:07');

-- --------------------------------------------------------

--
-- Table structure for table `user_titles`
--

CREATE TABLE IF NOT EXISTS `user_titles` (
  `id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vats`
--

CREATE TABLE IF NOT EXISTS `vats` (
  `id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `percent` decimal(10,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vats`
--

INSERT INTO `vats` (`id`, `company_id`, `name`, `percent`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Vat', '15.00', NULL, '2017-11-22 06:20:07', '2017-11-22 06:20:07');

-- --------------------------------------------------------

--
-- Table structure for table `warehouses`
--

CREATE TABLE IF NOT EXISTS `warehouses` (
  `id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warehouses`
--

INSERT INTO `warehouses` (`id`, `company_id`, `name`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Uttara Warehouse', NULL, NULL, '2017-11-22 06:18:45', '2017-11-22 06:18:45'),
(2, 1, 'Gazipur', 'Gazipur Warehouse', NULL, '2017-11-29 04:36:30', '2017-11-29 04:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_store_in`
--

CREATE TABLE IF NOT EXISTS `warehouse_store_in` (
  `id` int(10) unsigned NOT NULL,
  `warehouse_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `product_combination_id` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `warehouse_store_in_challans_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_store_in_challans`
--

CREATE TABLE IF NOT EXISTS `warehouse_store_in_challans` (
  `id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `warehouse_id` int(10) unsigned NOT NULL,
  `challan_no` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_date` date NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warehouse_store_in_challans`
--

INSERT INTO `warehouse_store_in_challans` (`id`, `company_id`, `warehouse_id`, `challan_no`, `status`, `request_date`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '20171', 'final', '2017-11-30', 1, '2017-11-22 06:25:38', '2017-12-17 02:22:46', NULL),
(2, 1, 1, '20172', 'final', '2017-11-21', 1, '2017-11-26 03:08:13', '2017-12-14 02:23:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_store_in_challan_products`
--

CREATE TABLE IF NOT EXISTS `warehouse_store_in_challan_products` (
  `id` int(10) unsigned NOT NULL,
  `warehouse_store_in_challan_id` int(10) unsigned NOT NULL,
  `supplier_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warehouse_store_in_challan_products`
--

INSERT INTO `warehouse_store_in_challan_products` (`id`, `warehouse_store_in_challan_id`, `supplier_id`, `product_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, '2017-11-22 06:25:38', '2017-11-22 06:25:38'),
(2, 2, 1, 2, NULL, '2017-11-26 03:08:13', '2017-11-26 03:08:13'),
(3, 2, 1, 1, NULL, '2017-11-26 04:48:18', '2017-11-26 04:48:18');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_store_in_challan_product_combinations`
--

CREATE TABLE IF NOT EXISTS `warehouse_store_in_challan_product_combinations` (
  `id` int(10) unsigned NOT NULL,
  `warehouse_store_in_challan_product_id` int(10) unsigned NOT NULL,
  `product_combination_id` int(10) unsigned NOT NULL,
  `quantity` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warehouse_store_in_challan_product_combinations`
--

INSERT INTO `warehouse_store_in_challan_product_combinations` (`id`, `warehouse_store_in_challan_product_id`, `product_combination_id`, `quantity`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, NULL, '2017-11-22 06:25:38', '2017-11-22 06:25:38'),
(2, 1, 2, 3, NULL, '2017-11-22 06:25:38', '2017-11-26 04:35:15'),
(3, 2, 3, 2, NULL, '2017-11-26 03:08:13', '2017-11-26 03:08:13'),
(4, 2, 4, 2, NULL, '2017-11-26 03:08:13', '2017-11-26 03:08:13'),
(5, 3, 1, 1, NULL, '2017-11-26 04:48:18', '2017-11-26 04:48:18'),
(6, 3, 2, 1, NULL, '2017-11-26 04:48:18', '2017-11-26 04:48:18');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_store_out`
--

CREATE TABLE IF NOT EXISTS `warehouse_store_out` (
  `id` int(10) unsigned NOT NULL,
  `outlet_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `product_combination_id` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `warehouse_store_out_challans_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_store_out_challans`
--

CREATE TABLE IF NOT EXISTS `warehouse_store_out_challans` (
  `id` int(10) unsigned NOT NULL,
  `warehouse_id` int(10) unsigned NOT NULL,
  `outlet_id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `warehouse_store_out_type_id` int(10) unsigned NOT NULL,
  `challan_no` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `request_date` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warehouse_store_out_challans`
--

INSERT INTO `warehouse_store_out_challans` (`id`, `warehouse_id`, `outlet_id`, `company_id`, `warehouse_store_out_type_id`, `challan_no`, `status`, `user_id`, `request_date`, `deleted_at`, `created_at`, `updated_at`) VALUES
(4, 1, 1, 1, 1, '20174', 'final', 1, '2017-12-01', NULL, '2017-11-22 06:28:15', '2017-12-14 02:18:41'),
(5, 1, 1, 1, 1, '20175', 'final', 1, '2017-12-30', NULL, '2017-11-26 03:30:32', '2017-12-19 04:13:24');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_store_out_challan_products`
--

CREATE TABLE IF NOT EXISTS `warehouse_store_out_challan_products` (
  `id` int(10) unsigned NOT NULL,
  `warehouse_store_out_challan_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `outlet_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warehouse_store_out_challan_products`
--

INSERT INTO `warehouse_store_out_challan_products` (`id`, `warehouse_store_out_challan_id`, `product_id`, `outlet_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 1, NULL, '2017-11-22 06:28:15', '2017-11-22 06:28:15'),
(2, 4, 2, 1, NULL, '2017-11-26 02:55:36', '2017-11-26 02:55:36'),
(3, 5, 1, 1, NULL, '2017-11-26 03:30:32', '2017-11-26 03:30:32'),
(4, 5, 2, 1, NULL, '2017-11-26 03:30:32', '2017-11-26 03:30:32');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_store_out_challan_product_combinations`
--

CREATE TABLE IF NOT EXISTS `warehouse_store_out_challan_product_combinations` (
  `id` int(10) unsigned NOT NULL,
  `warehouse_store_out_challan_product_id` int(10) unsigned NOT NULL,
  `product_combination_id` int(10) unsigned NOT NULL,
  `outlet_id` int(10) unsigned NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warehouse_store_out_challan_product_combinations`
--

INSERT INTO `warehouse_store_out_challan_product_combinations` (`id`, `warehouse_store_out_challan_product_id`, `product_combination_id`, `outlet_id`, `quantity`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 2, '2017-11-22 06:28:15', '2017-11-26 02:07:56', NULL),
(2, 1, 2, 1, 2, '2017-11-22 06:28:15', '2017-11-26 02:07:56', NULL),
(3, 2, 3, 1, 1, '2017-11-26 03:20:48', '2017-11-26 03:20:48', NULL),
(4, 2, 4, 1, 1, '2017-11-26 03:20:48', '2017-11-26 03:20:48', NULL),
(5, 3, 1, 1, 1, '2017-11-26 03:30:32', '2017-11-26 03:30:32', NULL),
(6, 3, 2, 1, 1, '2017-11-26 03:30:32', '2017-11-26 03:30:32', NULL),
(7, 4, 3, 1, 1, '2017-11-26 03:30:32', '2017-11-26 03:30:32', NULL),
(8, 4, 4, 1, 1, '2017-11-26 03:30:32', '2017-11-26 03:30:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_store_out_types`
--

CREATE TABLE IF NOT EXISTS `warehouse_store_out_types` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warehouse_store_out_types`
--

INSERT INTO `warehouse_store_out_types` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'outy', NULL, NULL, NULL),
(2, 'puty', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attributes_company_id_foreign` (`company_id`);

--
-- Indexes for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attribute_values_attribute_id_foreign` (`attribute_id`);

--
-- Indexes for table `business_categories`
--
ALTER TABLE `business_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_company_id_foreign` (`company_id`);

--
-- Indexes for table `category_product`
--
ALTER TABLE `category_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_product_category_id_foreign` (`category_id`),
  ADD KEY `category_product_product_id_foreign` (`product_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `companies_country_id_foreign` (`country_id`),
  ADD KEY `companies_time_zone_id_foreign` (`time_zone_id`),
  ADD KEY `companies_business_category_id_foreign` (`business_category_id`),
  ADD KEY `companies_currency_id_foreign` (`currency_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customers_out_let_id_foreign` (`out_let_id`),
  ADD KEY `customers_country_id_foreign` (`country_id`);

--
-- Indexes for table `customer_order_details`
--
ALTER TABLE `customer_order_details`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customer_order_details_serial_no_unique` (`serial_no`),
  ADD KEY `customer_order_details_order_by_foreign` (`order_by`),
  ADD KEY `customer_order_details_customer_id_foreign` (`customer_id`),
  ADD KEY `customer_order_details_invoice_prefix_id_foreign` (`invoice_prefix_id`);

--
-- Indexes for table `customer_order_detail_paids`
--
ALTER TABLE `customer_order_detail_paids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dashboards`
--
ALTER TABLE `dashboards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount_amount_types`
--
ALTER TABLE `discount_amount_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount_codes`
--
ALTER TABLE `discount_codes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `discount_codes_code_unique` (`code`);

--
-- Indexes for table `discount_code_types`
--
ALTER TABLE `discount_code_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount_coupons`
--
ALTER TABLE `discount_coupons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discount_coupons_company_id_foreign` (`company_id`);

--
-- Indexes for table `discount_coupon_outlets`
--
ALTER TABLE `discount_coupon_outlets`
  ADD PRIMARY KEY (`discount_coupon_id`,`out_let_id`),
  ADD KEY `discount_coupon_outlets_out_let_id_foreign` (`out_let_id`);

--
-- Indexes for table `discount_days`
--
ALTER TABLE `discount_days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount_types`
--
ALTER TABLE `discount_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount_type_configuration_categories`
--
ALTER TABLE `discount_type_configuration_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount_type_configuration_codes`
--
ALTER TABLE `discount_type_configuration_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount_type_configuration_days`
--
ALTER TABLE `discount_type_configuration_days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount_type_configuration_outlets`
--
ALTER TABLE `discount_type_configuration_outlets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount_type_configuration_products`
--
ALTER TABLE `discount_type_configuration_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_out_let_id_foreign` (`outlet_id`),
  ADD KEY `invoices_sales_by_foreign` (`sales_by`),
  ADD KEY `invoices_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `invoice_prefixes`
--
ALTER TABLE `invoice_prefixes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_prefixes_company_id_foreign` (`company_id`);

--
-- Indexes for table `invoice_products`
--
ALTER TABLE `invoice_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_products_invoice_id_foreign` (`invoice_id`),
  ADD KEY `invoice_products_product_id_foreign` (`product_id`),
  ADD KEY `invoice_products_product_combination_id_foreign` (`product_combination_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `outlet_challanins`
--
ALTER TABLE `outlet_challanins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outlet_ins`
--
ALTER TABLE `outlet_ins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outlet_outs`
--
ALTER TABLE `outlet_outs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outlet_products`
--
ALTER TABLE `outlet_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outlet_schedules`
--
ALTER TABLE `outlet_schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `outlet_schedules_schedule_type_id_foreign` (`schedule_type_id`),
  ADD KEY `outlet_schedules_out_let_id_foreign` (`out_let_id`);

--
-- Indexes for table `outlet_types`
--
ALTER TABLE `outlet_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `out_lets`
--
ALTER TABLE `out_lets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `out_lets_country_id_foreign` (`country_id`),
  ADD KEY `out_lets_company_id_foreign` (`company_id`),
  ADD KEY `out_lets_outlet_type_id_foreign` (`outlet_type_id`);

--
-- Indexes for table `out_let_user`
--
ALTER TABLE `out_let_user`
  ADD PRIMARY KEY (`user_id`,`out_let_id`),
  ADD KEY `out_let_user_out_let_id_foreign` (`out_let_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_methods_company_id_foreign` (`company_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_module_id_foreign` (`module_id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_item_code_unique` (`item_code`),
  ADD KEY `products_company_id_foreign` (`company_id`);

--
-- Indexes for table `product_combinations`
--
ALTER TABLE `product_combinations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_combinations_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_combination_attributes`
--
ALTER TABLE `product_combination_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_combination_attributes_attribute_value_id_foreign` (`attribute_value_id`),
  ADD KEY `product_combination_attributes_product_combination_id_foreign` (`product_combination_id`);

--
-- Indexes for table `product_discounts`
--
ALTER TABLE `product_discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_discounts_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_discount_outlets`
--
ALTER TABLE `product_discount_outlets`
  ADD PRIMARY KEY (`product_discount_id`,`out_let_id`),
  ADD KEY `product_discount_outlets_out_let_id_foreign` (`out_let_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_prices`
--
ALTER TABLE `product_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_prices_product_id_foreign` (`product_id`),
  ADD KEY `product_prices_vat_id_foreign` (`vat_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_company_id_foreign` (`company_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sales_out_let_id_foreign` (`out_let_id`),
  ADD KEY `sales_sales_by_foreign` (`sales_by`),
  ADD KEY `sales_customer_id_foreign` (`customer_id`),
  ADD KEY `sales_invoice_prefix_id_foreign` (`invoice_prefix_id`);

--
-- Indexes for table `sale_products`
--
ALTER TABLE `sale_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_products_outlet_product_id_foreign` (`outlet_product_id`),
  ADD KEY `sale_products_sale_id_foreign` (`sale_id`),
  ADD KEY `sale_products_product_discount_id_foreign` (`product_discount_id`),
  ADD KEY `sale_products_product_price_id_foreign` (`product_price_id`);

--
-- Indexes for table `schedule_types`
--
ALTER TABLE `schedule_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `suppliers_email_unique` (`email`),
  ADD UNIQUE KEY `suppliers_phone_unique` (`phone`),
  ADD KEY `suppliers_country_id_foreign` (`country_id`),
  ADD KEY `suppliers_company_id_foreign` (`company_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task_types`
--
ALTER TABLE `task_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_categories`
--
ALTER TABLE `tax_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tax_categories_company_id_foreign` (`company_id`);

--
-- Indexes for table `tax_category_values`
--
ALTER TABLE `tax_category_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tax_category_values_tax_category_id_foreign` (`tax_category_id`);

--
-- Indexes for table `time_zones`
--
ALTER TABLE `time_zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_details_user_id_foreign` (`user_id`),
  ADD KEY `user_details_country_id_foreign` (`country_id`),
  ADD KEY `user_details_user_title_id_foreign` (`user_title_id`);

--
-- Indexes for table `user_titles`
--
ALTER TABLE `user_titles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_titles_company_id_foreign` (`company_id`);

--
-- Indexes for table `vats`
--
ALTER TABLE `vats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vats_company_id_foreign` (`company_id`);

--
-- Indexes for table `warehouses`
--
ALTER TABLE `warehouses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `warehouses_company_id_foreign` (`company_id`);

--
-- Indexes for table `warehouse_store_in`
--
ALTER TABLE `warehouse_store_in`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse_store_in_challans`
--
ALTER TABLE `warehouse_store_in_challans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `warehouse_store_in_challans_challan_no_unique` (`challan_no`),
  ADD KEY `warehouse_store_in_challans_company_id_foreign` (`company_id`),
  ADD KEY `warehouse_store_in_challans_warehouse_id_foreign` (`warehouse_id`),
  ADD KEY `warehouse_store_in_challans_created_by_foreign` (`created_by`);

--
-- Indexes for table `warehouse_store_in_challan_products`
--
ALTER TABLE `warehouse_store_in_challan_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_warehouse_store_in_challan_id` (`warehouse_store_in_challan_id`),
  ADD KEY `warehouse_store_in_challan_products_product_id_foreign` (`product_id`),
  ADD KEY `warehouse_store_in_challan_products_supplier_id_foreign` (`supplier_id`);

--
-- Indexes for table `warehouse_store_in_challan_product_combinations`
--
ALTER TABLE `warehouse_store_in_challan_product_combinations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_combination_id` (`product_combination_id`);

--
-- Indexes for table `warehouse_store_out`
--
ALTER TABLE `warehouse_store_out`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse_store_out_challans`
--
ALTER TABLE `warehouse_store_out_challans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `warehouse_store_out_challans_challan_no_unique` (`challan_no`),
  ADD KEY `warehouse_store_out_challans_warehouse_id_foreign` (`warehouse_id`),
  ADD KEY `warehouse_store_out_challans_company_id_foreign` (`company_id`),
  ADD KEY `warehouse_store_out_challans_outlet_id_foreign` (`outlet_id`),
  ADD KEY `warehouse_store_out_challans_warehouse_store_out_type_id_foreign` (`warehouse_store_out_type_id`),
  ADD KEY `warehouse_store_out_challans_user_id_foreign` (`user_id`);

--
-- Indexes for table `warehouse_store_out_challan_products`
--
ALTER TABLE `warehouse_store_out_challan_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `warehouse_store_out_challan_products_outlet_id_foreign` (`outlet_id`),
  ADD KEY `warehouse_store_out_challan_products_product_id_foreign` (`product_id`);

--
-- Indexes for table `warehouse_store_out_challan_product_combinations`
--
ALTER TABLE `warehouse_store_out_challan_product_combinations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_warehouse_store_out_challan_product_id` (`warehouse_store_out_challan_product_id`),
  ADD KEY `fk_store_out_product_combination_id` (`product_combination_id`);

--
-- Indexes for table `warehouse_store_out_types`
--
ALTER TABLE `warehouse_store_out_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `attribute_values`
--
ALTER TABLE `attribute_values`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `business_categories`
--
ALTER TABLE `business_categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `category_product`
--
ALTER TABLE `category_product`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=250;
--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customer_order_details`
--
ALTER TABLE `customer_order_details`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_order_detail_paids`
--
ALTER TABLE `customer_order_detail_paids`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dashboards`
--
ALTER TABLE `dashboards`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `discount_amount_types`
--
ALTER TABLE `discount_amount_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `discount_codes`
--
ALTER TABLE `discount_codes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `discount_code_types`
--
ALTER TABLE `discount_code_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `discount_coupons`
--
ALTER TABLE `discount_coupons`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `discount_days`
--
ALTER TABLE `discount_days`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `discount_types`
--
ALTER TABLE `discount_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `discount_type_configuration_categories`
--
ALTER TABLE `discount_type_configuration_categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `discount_type_configuration_codes`
--
ALTER TABLE `discount_type_configuration_codes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `discount_type_configuration_days`
--
ALTER TABLE `discount_type_configuration_days`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `discount_type_configuration_outlets`
--
ALTER TABLE `discount_type_configuration_outlets`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `discount_type_configuration_products`
--
ALTER TABLE `discount_type_configuration_products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `invoice_prefixes`
--
ALTER TABLE `invoice_prefixes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice_products`
--
ALTER TABLE `invoice_products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=159;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `outlet_challanins`
--
ALTER TABLE `outlet_challanins`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `outlet_ins`
--
ALTER TABLE `outlet_ins`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `outlet_outs`
--
ALTER TABLE `outlet_outs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `outlet_products`
--
ALTER TABLE `outlet_products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `outlet_schedules`
--
ALTER TABLE `outlet_schedules`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `outlet_types`
--
ALTER TABLE `outlet_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `out_lets`
--
ALTER TABLE `out_lets`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `product_combinations`
--
ALTER TABLE `product_combinations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `product_combination_attributes`
--
ALTER TABLE `product_combination_attributes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `product_discounts`
--
ALTER TABLE `product_discounts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `product_prices`
--
ALTER TABLE `product_prices`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sale_products`
--
ALTER TABLE `sale_products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `schedule_types`
--
ALTER TABLE `schedule_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `task_types`
--
ALTER TABLE `task_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tax_categories`
--
ALTER TABLE `tax_categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tax_category_values`
--
ALTER TABLE `tax_category_values`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `time_zones`
--
ALTER TABLE `time_zones`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=420;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_titles`
--
ALTER TABLE `user_titles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vats`
--
ALTER TABLE `vats`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `warehouses`
--
ALTER TABLE `warehouses`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `warehouse_store_in`
--
ALTER TABLE `warehouse_store_in`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `warehouse_store_in_challans`
--
ALTER TABLE `warehouse_store_in_challans`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `warehouse_store_in_challan_products`
--
ALTER TABLE `warehouse_store_in_challan_products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `warehouse_store_in_challan_product_combinations`
--
ALTER TABLE `warehouse_store_in_challan_product_combinations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `warehouse_store_out`
--
ALTER TABLE `warehouse_store_out`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `warehouse_store_out_challans`
--
ALTER TABLE `warehouse_store_out_challans`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `warehouse_store_out_challan_products`
--
ALTER TABLE `warehouse_store_out_challan_products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `warehouse_store_out_challan_product_combinations`
--
ALTER TABLE `warehouse_store_out_challan_product_combinations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `warehouse_store_out_types`
--
ALTER TABLE `warehouse_store_out_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `attributes`
--
ALTER TABLE `attributes`
  ADD CONSTRAINT `attributes_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD CONSTRAINT `attribute_values_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_product`
--
ALTER TABLE `category_product`
  ADD CONSTRAINT `category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `companies_business_category_id_foreign` FOREIGN KEY (`business_category_id`) REFERENCES `business_categories` (`id`),
  ADD CONSTRAINT `companies_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `companies_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`),
  ADD CONSTRAINT `companies_time_zone_id_foreign` FOREIGN KEY (`time_zone_id`) REFERENCES `time_zones` (`id`);

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `customers_out_let_id_foreign` FOREIGN KEY (`out_let_id`) REFERENCES `out_lets` (`id`);

--
-- Constraints for table `customer_order_details`
--
ALTER TABLE `customer_order_details`
  ADD CONSTRAINT `customer_order_details_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `customer_order_details_invoice_prefix_id_foreign` FOREIGN KEY (`invoice_prefix_id`) REFERENCES `invoice_prefixes` (`id`),
  ADD CONSTRAINT `customer_order_details_order_by_foreign` FOREIGN KEY (`order_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `discount_coupons`
--
ALTER TABLE `discount_coupons`
  ADD CONSTRAINT `discount_coupons_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `discount_coupon_outlets`
--
ALTER TABLE `discount_coupon_outlets`
  ADD CONSTRAINT `discount_coupon_outlets_discount_coupon_id_foreign` FOREIGN KEY (`discount_coupon_id`) REFERENCES `discount_coupons` (`id`),
  ADD CONSTRAINT `discount_coupon_outlets_out_let_id_foreign` FOREIGN KEY (`out_let_id`) REFERENCES `out_lets` (`id`);

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `invoices_out_let_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `out_lets` (`id`),
  ADD CONSTRAINT `invoices_sales_by_foreign` FOREIGN KEY (`sales_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `invoice_prefixes`
--
ALTER TABLE `invoice_prefixes`
  ADD CONSTRAINT `invoice_prefixes_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `invoice_products`
--
ALTER TABLE `invoice_products`
  ADD CONSTRAINT `invoice_products_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  ADD CONSTRAINT `invoice_products_product_combination_id_foreign` FOREIGN KEY (`product_combination_id`) REFERENCES `product_combinations` (`id`),
  ADD CONSTRAINT `invoice_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `outlet_schedules`
--
ALTER TABLE `outlet_schedules`
  ADD CONSTRAINT `outlet_schedules_out_let_id_foreign` FOREIGN KEY (`out_let_id`) REFERENCES `out_lets` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `outlet_schedules_schedule_type_id_foreign` FOREIGN KEY (`schedule_type_id`) REFERENCES `schedule_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `out_lets`
--
ALTER TABLE `out_lets`
  ADD CONSTRAINT `out_lets_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `out_lets_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `out_lets_outlet_type_id_foreign` FOREIGN KEY (`outlet_type_id`) REFERENCES `outlet_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `out_let_user`
--
ALTER TABLE `out_let_user`
  ADD CONSTRAINT `out_let_user_out_let_id_foreign` FOREIGN KEY (`out_let_id`) REFERENCES `out_lets` (`id`),
  ADD CONSTRAINT `out_let_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD CONSTRAINT `payment_methods_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_combinations`
--
ALTER TABLE `product_combinations`
  ADD CONSTRAINT `product_combinations_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_combination_attributes`
--
ALTER TABLE `product_combination_attributes`
  ADD CONSTRAINT `product_combination_attributes_attribute_value_id_foreign` FOREIGN KEY (`attribute_value_id`) REFERENCES `attribute_values` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_combination_attributes_product_combination_id_foreign` FOREIGN KEY (`product_combination_id`) REFERENCES `product_combinations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_discounts`
--
ALTER TABLE `product_discounts`
  ADD CONSTRAINT `product_discounts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_discount_outlets`
--
ALTER TABLE `product_discount_outlets`
  ADD CONSTRAINT `product_discount_outlets_out_let_id_foreign` FOREIGN KEY (`out_let_id`) REFERENCES `out_lets` (`id`),
  ADD CONSTRAINT `product_discount_outlets_product_discount_id_foreign` FOREIGN KEY (`product_discount_id`) REFERENCES `product_discounts` (`id`);

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_prices`
--
ALTER TABLE `product_prices`
  ADD CONSTRAINT `product_prices_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `product_prices_vat_id_foreign` FOREIGN KEY (`vat_id`) REFERENCES `vats` (`id`);

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `sales_invoice_prefix_id_foreign` FOREIGN KEY (`invoice_prefix_id`) REFERENCES `invoice_prefixes` (`id`),
  ADD CONSTRAINT `sales_out_let_id_foreign` FOREIGN KEY (`out_let_id`) REFERENCES `out_lets` (`id`),
  ADD CONSTRAINT `sales_sales_by_foreign` FOREIGN KEY (`sales_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `sale_products`
--
ALTER TABLE `sale_products`
  ADD CONSTRAINT `sale_products_outlet_product_id_foreign` FOREIGN KEY (`outlet_product_id`) REFERENCES `outlet_products` (`id`),
  ADD CONSTRAINT `sale_products_product_discount_id_foreign` FOREIGN KEY (`product_discount_id`) REFERENCES `product_discounts` (`id`),
  ADD CONSTRAINT `sale_products_product_price_id_foreign` FOREIGN KEY (`product_price_id`) REFERENCES `product_prices` (`id`),
  ADD CONSTRAINT `sale_products_sale_id_foreign` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`id`);

--
-- Constraints for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD CONSTRAINT `suppliers_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `suppliers_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`);

--
-- Constraints for table `tax_categories`
--
ALTER TABLE `tax_categories`
  ADD CONSTRAINT `tax_categories_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tax_category_values`
--
ALTER TABLE `tax_category_values`
  ADD CONSTRAINT `tax_category_values_tax_category_id_foreign` FOREIGN KEY (`tax_category_id`) REFERENCES `tax_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `user_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_details_user_title_id_foreign` FOREIGN KEY (`user_title_id`) REFERENCES `user_titles` (`id`);

--
-- Constraints for table `user_titles`
--
ALTER TABLE `user_titles`
  ADD CONSTRAINT `user_titles_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vats`
--
ALTER TABLE `vats`
  ADD CONSTRAINT `vats_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `warehouses`
--
ALTER TABLE `warehouses`
  ADD CONSTRAINT `warehouses_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `warehouse_store_in_challans`
--
ALTER TABLE `warehouse_store_in_challans`
  ADD CONSTRAINT `warehouse_store_in_challans_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `warehouse_store_in_challans_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `warehouse_store_in_challans_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `warehouse_store_in_challan_products`
--
ALTER TABLE `warehouse_store_in_challan_products`
  ADD CONSTRAINT `fk_warehouse_store_in_challan_id` FOREIGN KEY (`warehouse_store_in_challan_id`) REFERENCES `warehouse_store_in_challans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `warehouse_store_in_challan_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `warehouse_store_in_challan_products_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `warehouse_store_in_challan_product_combinations`
--
ALTER TABLE `warehouse_store_in_challan_product_combinations`
  ADD CONSTRAINT `fk_product_combination_id` FOREIGN KEY (`product_combination_id`) REFERENCES `product_combinations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `warehouse_store_out_challans`
--
ALTER TABLE `warehouse_store_out_challans`
  ADD CONSTRAINT `warehouse_store_out_challans_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `warehouse_store_out_challans_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `out_lets` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `warehouse_store_out_challans_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `warehouse_store_out_challans_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `warehouse_store_out_challans_warehouse_store_out_type_id_foreign` FOREIGN KEY (`warehouse_store_out_type_id`) REFERENCES `warehouse_store_out_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `warehouse_store_out_challan_products`
--
ALTER TABLE `warehouse_store_out_challan_products`
  ADD CONSTRAINT `warehouse_store_out_challan_products_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `out_lets` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `warehouse_store_out_challan_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `warehouse_store_out_challan_product_combinations`
--
ALTER TABLE `warehouse_store_out_challan_product_combinations`
  ADD CONSTRAINT `fk_store_out_product_combination_id` FOREIGN KEY (`product_combination_id`) REFERENCES `product_combination_attributes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_warehouse_store_out_challan_product_id` FOREIGN KEY (`warehouse_store_out_challan_product_id`) REFERENCES `warehouse_store_out_challan_products` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

SET FOREIGN_KEY_CHECKS = 1;
