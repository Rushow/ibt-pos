-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 05, 2018 at 04:11 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ibtpos`
--

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `sequence`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Home', 1, NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(2, 'Register( Cash Drawer )', 2, NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(3, 'Customer', 3, NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(4, 'Sync', 4, NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(5, 'Company Settings', 5, NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(6, 'Users', 6, NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(7, 'Customers', 7, NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(8, 'Catalog', 8, NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(9, 'Inventory', 9, NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(10, 'Discount', 10, NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(11, 'Invoice', 11, NULL, NULL, NULL),
(12, 'Sales Report', 12, NULL, NULL, NULL),
(13, 'Task', 13, NULL, NULL, NULL),
(14, 'API', 14, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
