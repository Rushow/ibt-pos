-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 05, 2018 at 04:09 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ibtpos`
--

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `module_id`, `name`, `slug`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Access Dashboard', 'access-dashboard', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(2, 1, 'Access My Profile', 'access-my-profile', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(3, 2, 'Access Register Page', 'access-register-page', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(4, 2, 'Access Discount at Register', 'access-discount-at-register', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(5, 2, 'Remove Tax in Register', 'remove-tax-in-register', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(6, 2, 'Allow Refund', 'allow-refund', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(7, 3, 'Create / View Customer', 'create-view-customer', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(8, 3, 'Edit / Update Customer', 'edit-update-customer', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(9, 3, 'Delete Customer', 'delete-customer', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(10, 3, 'Activate / Deactivate Customer', 'activate-deactivate-customer', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(11, 3, 'Create / View Customer Notes', 'create-view-customer-notes', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(12, 3, 'Edit / Update Customer Notes', 'edit-update-customer-notes', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(13, 3, 'Delete Customer Notes', 'delete-customer-notes', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(14, 3, 'Edit credit/balance/reward', 'edit-credit-balance-reward', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(15, 3, 'Access Purchase History', 'access-purchase-history', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(16, 4, 'Sync Challan', 'sync-challan', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(17, 4, 'Sync Sales Report', 'sync-sales-report', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(18, 4, 'Sync All( Challan and Sales Report )', 'sync-all', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(19, 5, 'Access Company info', 'access-company-info', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(20, 5, 'Access Global VAT Settings', 'access-global-vat-settings', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(21, 5, 'Access Global Payment Method Settings', 'access-global-payment-method-settings', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(22, 5, 'Create / View Outlets', 'create-view-outlets', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(23, 5, 'Edit / Update Outlets', 'edit-update-outlets', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(24, 5, 'Delete Outlets', 'delete-outlets', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(25, 5, 'Activate / Deactivate Outlets', 'activate-deactivate-outlets', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(26, 6, 'Access Position / Title', 'access-position-title', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(27, 6, 'Add / Edit Access Permission( Create Permission Name Also\n                            )', 'add-edit-access-permission', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(28, 6, 'Create / View Users', 'create-view-users', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(29, 6, 'Edit / Update User', 'edit-update-user', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(30, 6, 'Delete User', 'delete-user', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(31, 6, 'Activate / Deactivate User', 'activate-deactivate-user', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(32, 7, 'Create / View Customers', 'create-view-customers', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(33, 8, 'Access Category', 'access-category', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(34, 8, 'Create / View Products', 'create-view-products', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(35, 8, 'Edit / Update Products', 'edit-update-products', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(36, 8, 'Delete Products', 'delete-products', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(37, 8, 'Access Product Prices', 'access-product-prices', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(38, 8, 'Access Product Attributes', 'access-product-attributes', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(39, 8, 'Access Product Combination', 'access-product-combination', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(40, 9, 'Access Warehouse', 'access-warehouse', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(41, 9, 'Access Vendor / Supplier', 'access-vendor-supplier', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(42, 9, 'Access Warehouse Store-in', 'access-warehouse-store-in', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(43, 9, 'Access Warehouse Store-out', 'access-warehouse-store-out', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(44, 10, 'Access Discount', 'access-discount-moudule', NULL, '2018-01-05 15:19:23', '2018-01-05 15:19:23'),
(45, 11, 'Access Invoice', 'access-invoice-module', NULL, NULL, NULL),
(46, 12, 'Access Sales Report', 'access-sales-report-module', NULL, NULL, NULL),
(47, 13, 'Access Task', 'access-task-module', NULL, NULL, NULL),
(48, 14, 'Product API', 'access-product-api', NULL, NULL, NULL),
(49, 14, 'Challan API', 'access-challan-api', NULL, NULL, NULL),
(50, 14, 'Coupon API', 'access-validatecoupon-api', NULL, NULL, NULL),
(51, 14, 'Discount Rules API', 'access-discount-rules-api', NULL, NULL, NULL),
(52, 14, 'Invoice API', 'access-invoice-api', NULL, NULL, NULL),
(53, 14, 'Task API', 'access-task-api', NULL, NULL, NULL),
(54, 14, 'Sync Task', 'sync-task-api', NULL, NULL, NULL),
(55, 14, 'Inventory API', 'access-inventory-api', NULL, NULL, NULL),
(56, 14, 'Sales Report API', 'access-sales-report-api', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_module_id_foreign` (`module_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
