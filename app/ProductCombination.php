<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCombination extends Model
{
    use SoftDeletes;
    protected $table = 'product_combinations';
    protected $fillable = ['product_id', 'name', 'barcode'];
    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->belongsTo('App\Product')->select('id', 'name', 'item_code', 'reference_code');
    }

    public function combinationAttributeValues()
    {
        return $this->hasMany('App\ProductCombinationAttribute');
    }
}