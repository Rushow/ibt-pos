<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountCoupon extends Model
{
    use SoftDeletes;
    protected $table = 'discount_coupons';
    protected $fillable = ['company_id', 'coupon_code', 'discount_amount', 'discount_percent', 'total_coupon', 'coupon_left', 'active_date', 'expire_date'];
    protected $dates = ['deleted_at'];

}
