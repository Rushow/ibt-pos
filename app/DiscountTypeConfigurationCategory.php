<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountTypeConfigurationCategory extends Model
{
    use SoftDeletes;
    protected $table = 'discount_type_configuration_categories';
    protected $fillable = ['discount_id', 'category_id'];
    protected $dates = ['deleted_at'];
}
