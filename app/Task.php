<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    public function TaskType()
    {

        return $this->belongsTo("App\TaskType");

    }
}
