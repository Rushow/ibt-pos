<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceProduct extends Model
{
    use SoftDeletes;
    protected $fillable = ['invoice_id', 'product_id', 'product_combination_id', 'quantity', 'product_discounts_id', 'discount_rules_id', 'discount_code_id'];
    protected $dates = ['deleted_at'];

    public function Product()
    {

        return $this->belongsTo('App\Product', 'product_id');
    }

    public function ProductCombination()
    {
        return $this->belongsTo('App\ProductCombination', 'product_combination_id');
    }

    public function Discount()
    {
        return $this->belongsTo('App\Discount', 'discount_rules_id');
    }

    public function ProductDiscount()
    {

        return $this->belongsTo('App\ProductDiscount', 'product_discount_id');
    }
}
