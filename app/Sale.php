<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    use SoftDeletes;
    protected $table = 'sales';
    protected $dates = ['deleted_at'];

    protected $fillable = ['company_id', 'outlet_id', 'user_id', 'customer_id', 'discount_coupon_id',
        'invoice_prefix_id', 'invoice_no', 'price_exclusive_vat', 'vat_amount', 'product_discount_amount',
        'coupon_discount_amount', 'total_amount', 'credit_card_type', 'credit_card_number', 'card_received',
        'cash_received', 'total_received', 'is_sales_to_member', 'sales_notes'];
}