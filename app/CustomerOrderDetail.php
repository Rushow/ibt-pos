<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerOrderDetail extends Model
{
    use SoftDeletes;
    protected $table = 'customer_order_details';
    protected $fillable = ['outlet_id', 'order_by', 'customer_id', 'serial_no', 'order_date', 'expected_delivery_date',
        'delivered_date', 'unit_price', 'weight_rotti', 'weight_milli', 'material_price', 'making_charge', 'discount_coupon_id',
        'invoice_prefix_id', 'invoice_no', 'price_exclusive_vat', 'vat_amount', 'price_inclusive_vat', 'order_discount_amount', 'coupon_discount_amount',
        'total_amount', 'order_details', 'status', 'is_sales_to_member'];
    protected $dates = ['deleted_at'];
}