<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutletIn extends Model
{
    protected $table = 'outlet_ins';

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function combination()
    {
        return $this->belongsTo('App\ProductCombination');
    }

    public function warehouseChallan()
    {
        return $this->belongsTo('App\WarehouseStoreOutChallan');
    }
}
