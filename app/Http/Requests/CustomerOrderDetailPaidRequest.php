<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerOrderDetailPaidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'paid_date' => 'required',
                    'paid_amount' => 'required|regex:/\d+\.?\d*/',
                ];
                break;

            case 'PUT':
                return [
                    'paid_date' => 'required',
                    'paid_amount' => 'required|regex:/\d+\.?\d*/',
                ];
                break;
        }
    }
}
