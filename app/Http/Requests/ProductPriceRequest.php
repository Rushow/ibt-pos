<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductPriceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'vat_id' => 'required',
                    'price' => 'required|regex:/\d+\.?\d*/',
                    'cost' => 'required|regex:/\d+\.?\d*/',
                    'cost_price_date' => 'required',
                ];
                break;

            case 'PUT':
                return [
                    'vat_id' => 'required',
                    'price' => 'required|regex:/\d+\.?\d*/',
                    'cost' => 'required|regex:/\d+\.?\d*/',
                    'cost_price_date' => 'required',
                ];
                break;
        }
    }
}
