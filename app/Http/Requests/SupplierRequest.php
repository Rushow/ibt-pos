<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'name' => 'required|min:3|unique:suppliers,name',
                    'code' => 'required|min:3|unique:suppliers,code',
                    'address' => 'required|min:15|max:200',
                    'city' => 'required|min:3|max:50',
                    'zip' => 'required|regex:/^[0-9]{4}$/',
                    'phone' => 'required|regex:/(01)[0-9]{9}$/|unique:suppliers,phone',
                    'email' => 'required|email|unique:suppliers,email',
                ];
                break;

            case 'PUT':
                return [
                    'name' => 'required|min:3|max:200|unique:suppliers,name,'.$this->route('supplier'),
                    'code' => 'required|min:3|unique:suppliers,code,'.$this->route('supplier'),
                    'address' => 'required|min:15|max:200',
                    'city' => 'required|min:3|max:50',
                    'zip' => 'required|regex:/^[0-9]{4}$/',
                    'phone' => 'required|regex:/(01)[0-9]{9}$/|unique:suppliers,phone,'.$this->route('supplier'),
                    'email' => 'required|email|unique:suppliers,email,'.$this->route('supplier'),
                ];
                break;
        }
    }
}
