<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OutLetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules( )
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'name' => 'required|min:3|max:200|unique:out_lets,name',
                    'outlet_type_id' => 'required|exists:outlet_types,id',
                    'space' => 'required|integer',
                    'duration' => 'required|integer',
                    'capacity' => 'required|integer',
                    'display_products' => 'required|integer',
                    'advance_amount' => 'required|regex:/\d+\.?\d*/',
                    'monthly_rent' => 'required|regex:/\d+\.?\d*/',
                    'trade_license_no' => 'required|min:5|max:100',
                    'start_date' => 'required|date_format:Y-m-d',
                    'location' => 'required',
                    'phone' => 'required|regex:/(01)[0-9]{9}$/',
                    'address' => 'required|min:15|max:200',
                    'city' => 'required|min:3|max:50',
                    'zip' => 'required|regex:/^[0-9]{4}$/',
                    'responsible_person' => 'required|min:5|max:200',
                    'trade_license_photo' => 'mimes:jpeg,jpg,png,pdf|max:10000'
                ];
                break;

            case 'PUT':
                return [
                    'name' => 'required|min:3|max:200|unique:out_lets,name,'.$this->route('out_let'),
                    'outlet_type_id' => 'required|exists:outlet_types,id',
                    'space' => 'required|integer',
                    'duration' => 'required|integer',
                    'capacity' => 'required|integer',
                    'display_products' => 'required|integer',
                    'advance_amount' => 'required|regex:/\d+\.?\d*/',
                    'monthly_rent' => 'required|regex:/\d+\.?\d*/',
                    'trade_license_no' => 'required|min:5|max:100',
                    'start_date' => 'required|date_format:Y-m-d',
                    'location' => 'required',
                    'phone' => 'required|regex:/(01)[0-9]{9}$/',
                    'address' => 'required|min:15|max:200',
                    'city' => 'required|min:3|max:50',
                    'zip' => 'required|regex:/^[0-9]{4}$/',
                    'responsible_person' => 'required|min:5|max:200',
                    'trade_license_photo' => 'mimes:jpeg,jpg,png,pdf|max:10000'
                ];
                break;
        }
    }
}
