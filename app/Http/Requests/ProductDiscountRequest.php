<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductDiscountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    
                    'discount_amount' => 'required',
                    'discount_type' => 'required',
                    'active_date' => 'required',
                    'expire_date' => 'required',
                    'status' => 'required',
                ];
                break;

            case 'PUT':
                return [

                    'discount_amount' => 'required',
                    'discount_type' => 'required',
                    'active_date' => 'required',
                    'expire_date' => 'required',
                    'status' => 'required',
                ];
                break;
        }
    }
}
