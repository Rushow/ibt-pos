<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WarehouseStoreoutChallanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'warehouse_id' => 'required',
                    'user_id' => 'required',
                    'outlet_id' => 'required',
                    'warehouse_store_out_type_id' => 'required',
                    'challan_no' => 'required',
                ];
                break;

            case 'PUT':
                return [
                    'warehouse_id' => 'required',
                    'user_id' => 'required',
                    'outlet_id' => 'required',
                    'warehouse_store_out_type_id' => 'required',
                    'challan_no' => 'required',
                ];
                break;
        }
    }
}
