<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'name' => 'required|min:3|unique:products,name',
                    //'item_code' => 'required|regex:/^[0-9]{6}$/|min:6|max:6|unique:products,item_code',
                ];
                break;

            case 'PUT':
                return [
                    'name' => 'required|min:3|unique:products,name,'.$this->route('product')->id,
                    //'item_code' => 'required|regex:/^[0-9]{6}$/|min:6|max:6|unique:products,item_code,'.$this->route('product')->id,
                ];
                break;
        }
    }
}
