<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'first_name' => 'required|min:3|max:20',
                    'last_name' => 'required|min:3|max:20',
                    'date_of_birth' => 'required',
                    'cell_phone' => 'required|regex:/(01)[0-9]{9}$/',
                    'work_phone' => 'regex:/(01)[0-9]{9}$/',
                    'address' => 'required|min:15|max:200',
                    'city' => 'required|min:3|max:50',
                    'zip' => 'required|regex:/^[0-9]{4}$/',
                ];
                break;

            case 'PUT':
                return [
                    'first_name' => 'required|min:3|max:20',
                    'last_name' => 'required|min:3|max:20',
                    'date_of_birth' => 'required',
                    'cell_phone' => 'required|regex:/(01)[0-9]{9}$/',
                    'address' => 'required|min:15|max:200',
                    'city' => 'required|min:3|max:50',
                    'zip' => 'required|regex:/^[0-9]{4}$/',
                ];
                break;
        }
    }
}
