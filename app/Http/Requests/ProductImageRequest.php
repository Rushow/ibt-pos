<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'name' => 'mimes:jpeg,jpg,png|max:10000',
                ];
                break;

            case 'PUT':
                return [
                    'name' => 'mimes:jpeg,jpg,png|max:10000',
                ];
                break;
        }
    }
}
