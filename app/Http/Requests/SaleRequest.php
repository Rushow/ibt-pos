<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DailySaleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'invoice_prefix_id' => 'required',
                    'invoice_no' => 'required',
                    'total_received' => 'required',
                ];
                break;

            case 'PUT':
                return [
                    'invoice_prefix_id' => 'required',
                    'invoice_no' => 'required',
                    'total_received' => 'required',
                ];
                break;
        }
    }
}