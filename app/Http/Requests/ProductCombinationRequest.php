<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductCombinationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'name' => 'required',
                    'product_id' => 'required',
                    'barcode' => 'required',
                ];
                break;

            case 'PUT':
                return [
                    'name' => 'required',
                    'product_id' => 'required',
                ];
                break;
        }
    }
}
