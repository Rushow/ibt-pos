<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerOrderDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'serial_no' => 'required',
                    'order_date' => 'required',
                    'expected_delivery_date' => 'required',
                    'delivered_date' => 'required',
                    'unit_price' => 'required|regex:/\d+\.?\d*/',
                    'weight_rotti' => 'required|integer',
                    'weight_milli' => 'integer',
                    'material_price' => 'required|regex:/\d+\.?\d*/',
                    'making_charge' => 'required|regex:/\d+\.?\d*/',
                    'price_exclusive_vat' => 'required|regex:/\d+\.?\d*/',
                    'vat_amount' => 'required|regex:/\d+\.?\d*/',
                    'price_inclusive_vat' => 'required|regex:/\d+\.?\d*/',
                    'order_discount_amount' => 'regex:/\d+\.?\d*/',
                    'coupon_discount_amount' => 'regex:/\d+\.?\d*/',
                    'total_amount' => 'required|regex:/\d+\.?\d*/',
                    'order_details' => 'required|min:10',
                ];
                break;

            case 'PUT':
                return [
                    'serial_no' => 'required',
                    'order_date' => 'required',
                    'expected_delivery_date' => 'required',
                    'delivered_date' => 'required',
                    'unit_price' => 'required|regex:/\d+\.?\d*/',
                    'weight_rotti' => 'required|integer',
                    'weight_milli' => 'integer',
                    'material_price' => 'required|regex:/\d+\.?\d*/',
                    'making_charge' => 'required|regex:/\d+\.?\d*/',
                    'price_exclusive_vat' => 'required|regex:/\d+\.?\d*/',
                    'vat_amount' => 'required|regex:/\d+\.?\d*/',
                    'price_inclusive_vat' => 'required|regex:/\d+\.?\d*/',
                    'order_discount_amount' => 'regex:/\d+\.?\d*/',
                    'coupon_discount_amount' => 'regex:/\d+\.?\d*/',
                    'total_amount' => 'required|regex:/\d+\.?\d*/',
                    'order_details' => 'required|min:10',
                ];
                break;
        }
    }
}
