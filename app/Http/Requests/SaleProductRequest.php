<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaleProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'outlet_product_id' => 'required',
                    'quantity' => 'required',
                    'unit_price' => 'required',
                ];
                break;

            case 'PUT':
                return [
                    'outlet_product_id' => 'required',
                    'quantity' => 'required',
                    'unit_price' => 'required',
                ];
                break;
        }
    }
}
