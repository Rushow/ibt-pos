<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRewardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'reward_amount' => 'required',
                    'total_reward_amount' => 'required',
                    'expire_date' => 'required',
                ];
                break;

            case 'PUT':
                return [
                    'reward_amount' => 'required',
                    'total_reward_amount' => 'required',
                    'expire_date' => 'required',
                ];
                break;
        }
    }
}
