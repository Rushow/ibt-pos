<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WarehouseStoreInChallanProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'warehouse_store_in_challan_id' => 'required',
                    'product_id' => 'required',
                    'product_attribute_combination_id' => 'required',
                    'quantity' => 'required',
                ];
                break;

            case 'PUT':
                return [
                    'warehouse_store_in_challan_id' => 'required',
                    'product_id' => 'required',
                    'product_attribute_combination_id' => 'required',
                    'quantity' => 'required',
                ];
                break;
        }
    }
}
