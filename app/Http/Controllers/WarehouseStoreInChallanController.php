<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\WarehouseStoreInChallanRequest;
use App\Warehouse;
use App\Supplier;
use App\Category;
use App\Product;
use App\WarehouseStoreInChallan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Elibyy\TCPDF\Facades\TCPDF;

class WarehouseStoreInChallanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->can('access-warehouse-store-in')) {
            abort(401);
        }
        $store_in_challans = WarehouseStoreInChallan::with('challanInProducts')->where('company_id', Auth::user()->company_id)->orderBy('id', 'DESC')->get();
//        dd( $store_in_challans );
//        echo '<pre>';
//        print_r( $store_in_challans );
//        die;

        return view('warehouse_store_in_challan.index', compact('store_in_challans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->can('access-warehouse-store-in')) {
            abort(401);
        }
        return view('warehouse_store_in_challan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(WarehouseStoreInChallanRequest $request)
    {
        if (!Auth::user()->can('access-warehouse-store-in')) {
            abort(401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(WarehouseStoreInChallan $store_in_challan)
    {
        if (!Auth::user()->can('access-warehouse-store-in')) {
            abort(401);
        }
        return view('warehouse_store_in_challan.show', compact('store_in_challan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(WarehouseStoreInChallan $store_in_challan)
    {
        if (!Auth::user()->can('access-warehouse-store-in')) {
            abort(401);
        }
        return view('warehouse_store_in_challan.edit', compact('store_in_challan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AttributeRequest|Request $request
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(AttributeRequest $request, Attribute $attribute)
    {
        if (!Auth::user()->can('access-warehouse-store-in')) {
            abort(401);
        }
        $attribute->update($request->all());
        flash('Attribute updated successfully', 'success');
        return redirect('attribute');
    }

    public function makefinal($id)
    {

        //while making final insert into new table warehouse_in

        if (!Auth::user()->can('access-warehouse-store-in')) {
            abort(401);
        }
        WarehouseStoreInChallan::find($id)->update(array("status" => "final"));
        return redirect("warehouse_store_in_challan");


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attribute $attribute)
    {
        if (!Auth::user()->can('access-warehouse-store-in')) {
            abort(401);
        }
        if (!empty($attribute)) {
            Attribute::destroy($attribute->id);
            flash('Attribute deleted successfully', 'success');
        }
        return redirect('attribute');
    }

    /**
     * Remove specified resources from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function removeAttributes(Request $request)
    {
        if (!Auth::user()->can('access-warehouse-store-in')) {
            abort(401);
        }
        if (!empty($request->attribute_ids)) {
            Attribute::destroy($request->attribute_ids);
            flash('Attribute deleted successfully', 'success');
        }
        return redirect('attribute');
    }


    public function warehouse_challan_in_pdf($id)
    {
        $store_in_challan = WarehouseStoreInChallan::find($id);
        $pdf = new TCPDF();
        $pdf::SetTitle('Challan ' . $store_in_challan->challan_no);
        $pdf::AddPage();
        $pdf::writeHTML(view('warehouse_store_in_challan.pdf', compact('store_in_challan'))->render());
        $pdf::Output('Challan ' . $store_in_challan->challan_no . '.pdf');
    }
}
