<?php

namespace App\Http\Controllers;

use App\Calendar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\DataTables\SalesDatatable;
use App\DataTables\DaywiseSalesDatatable;
use App\Invoice;
use App\OutLet;
use DB;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller
{
    public function salesReport(SalesDatatable $dataTable, Request $request)
    {
        if (!Auth::user()->can('access-sales-report')) {
            abort(401);
        }

        if(!empty($request->get('action'))) {
            $totals = $this->calculateSalesReportTotal($request);
            $salesReport = $this->salesReportQuery($request);

            Excel::create('sales_report',
                function($excel) use($totals, $salesReport){
                    $excel->sheet('New Sheet', function($sheet) use($totals, $salesReport){
                        $sheet->loadView('reports.sales_reports.excel', array('salesReport' => $salesReport, 'totals' => $totals));
                    });
                })->download('xls');
        }
        return $dataTable->render('reports.sales_reports.index');
    }

    private function salesReportQuery($request)
    {
        $query = Invoice::select(DB::raw('GROUP_CONCAT(`i_p`.`name`) AS name'), DB::raw('GROUP_CONCAT(`i_p`.`quantity`) AS quantity'),
            'invoices.id', 'invoices.invoice_no','i_p.invoice_id', 'invoices.created_at', 'invoices.product_price_exclusive_vat as total_without_vat',
            'invoices.card_received', 'invoices.cash_received', 'invoices.total_received', 'invoices.vat_amount', 'invoices.product_discount_amount as discount',
            DB::raw('GROUP_CONCAT(`product_combinations`.`barcode`) AS barcode'), DB::raw('GROUP_CONCAT(`i_p`.`price`) AS price'))
            ->join(DB::raw('(SELECT `invoice_products`.`invoice_id`, `invoice_products`.`product_id`, `products`.`name`, `invoice_products`.`quantity`, 
                            `product_prices`.`price` FROM `invoice_products` JOIN `products` ON `invoice_products`.`product_id` = `products`.`id` 
                            JOIN `product_prices` ON `products`.`id` = `product_prices`.`product_id`) AS i_p'), 'invoices.id', '=', 'i_p.invoice_id')
            ->join('product_combinations', 'product_combinations.product_id', '=', 'i_p.product_id');

        if(!empty($request->get('start_date'))) {
            $query->where('invoices.created_at', '>=', $request->get('start_date'));
        } else {
            $query->where('invoices.created_at', '>=', Carbon::today()->subDays(7)->toDateString());
        }

        if(!empty($request->get('end_date'))) {
            $query->where('invoices.created_at', '<=', $request->get('end_date'));
        } else {
            $query->where('invoices.created_at', '<=', Carbon::today()->toDateString());
        }

        return $query->groupBy('i_p.invoice_id')->get();
    }

    public function daywiseSalesReport(DaywiseSalesDatatable $dataTable, Request $request) {
        if(!Auth::user()->can('access-sales-report')) {
            abort(401);
        }

        if(!empty($request->get('action'))) {
            $totals = $this->calculateDaywiseSalesReportTotal($request);
            $salesReport = $this->daywiseSalesReportQuery($request);

            Excel::create('daywise_sales_report',
                function($excel) use($totals, $salesReport){
                    $excel->sheet('New Sheet', function($sheet) use($totals, $salesReport){
                        $sheet->loadView('reports.daywise_sales_report.excel', array('salesReport' => $salesReport, 'totals' => $totals));
                    });
                })->download('xls');
        }

        return $dataTable->with([
            'start_date' => $request->get('start_date'),
            'end_date' => $request->get('end_date'),
        ])->render('reports.daywise_sales_report.index');
    }
    
    private function daywiseSalesReportQuery($request) {
        $startDate = !empty($request->get('start_date')) ? $request->get('start_date') : Carbon::today()->subDays(7)->toDateString();
        $endDate = !empty($request->get('end_date')) ? $request->get('end_date') : Carbon::today()->toDateString();

        $query = Invoice::select(DB::raw('IFNULL(SUM(`invoices`.`product_price_exclusive_vat`), 0) AS product_price'),
            DB::raw('IFNULL(SUM(`invoices`.`vat_amount`), 0) AS vat'),
            DB::raw('IFNULL(SUM(`invoices`.`product_discount_amount`), 0) AS discount'),
            DB::raw('IFNULL(SUM(`invoices`.`card_received`), 0) AS card'),
            DB::raw('IFNULL(SUM(`invoices`.`cash_received`), 0) AS cash'),
            DB::raw('IFNULL(SUM(`invoices`.`total_received`), 0) AS total_price'),
            DB::raw('`calendar`.`datefield` AS date'))
            ->join('calendar', DB::raw('DATE(`invoices`.`created_at`)'), '=', 'calendar.datefield', 'right');

        return $query->whereBetween('calendar.datefield', [$startDate, $endDate])
            ->groupBy('calendar.datefield')->get();
    }

    public function storeDaywiseCumulativeSalesReport(Request $request) {
        $outlets = Outlet::select('id', 'name')->get();
        $action = $request->get('action');
        $outletSales = $this->getDaywiseSalesForOutlets($outlets, $request);
        $totals = $this->getTotalsForDaywiseStoreSales($request);

        $grandTotal = 0;
        $grandAverage = 0;

        foreach ($totals as $t) {
            $grandTotal = $grandTotal + (float) $t->total;
            $grandAverage = round($grandTotal / count($totals), 2);
        }

        if(!empty($action)) {
            Excel::create('daywise_store_sales_report',
                function($excel) use($outletSales, $totals, $grandTotal, $grandAverage){
                $excel->sheet('New Sheet', function($sheet) use($outletSales, $totals, $grandTotal, $grandAverage){
                    $sheet->loadView('reports.store_daywise_sales_report.excel', array('outletSales' => $outletSales, 'totals' => $totals, 'grandTotal' => $grandTotal, 'grandAverage' => $grandAverage));
                });
            })->download('xls');
        }
        
        return view('reports.store_daywise_sales_report.index', compact('outletSales', 'totals', 'grandTotal', 'grandAverage'));
    }

    private function getTotalsForDaywiseStoreSales($request) {
        $startDate = !empty($request->get('start_date')) ? $request->get('start_date') : Carbon::today()->subDays(7)->toDateString();
        $endDate = !empty($request->get('end_date')) ? $request->get('end_date') : Carbon::today()->toDateString();

        return Invoice::select(DB::raw('IFNULL(SUM(`invoices`.`total_received`), 0) as total'), 'calendar.datefield as date')
            ->join('calendar', DB::raw('DATE(`invoices`.`created_at`)'), '=', 'calendar.datefield', 'right')
            ->whereBetween('calendar.datefield', [$startDate, $endDate])
            ->groupBy('calendar.datefield')
            ->get();
    }

    private function getDaywiseSalesForOutlets($outlets, $request) {
        $outletDaywiseSales= $outlets;

        foreach ($outletDaywiseSales as $outlet) {
            $outlet->sales = $this->getIndividualDaywiseStoreSales($outlet->id, $request);
            $totalSaleStore = 0;
            foreach ($outlet->sales as $sale) {
                if($outlet->id == $sale->outlet_id) {
                    $totalSaleStore = $totalSaleStore + (float) $sale->total_received;
                }
            }
            $outlet->total_sales = $totalSaleStore;
            if(count($outlet->sales) > 0) {
                $outlet->average = round($totalSaleStore / count($outlet->sales), 2);
            } else {
                $outlet->average = 0.00;
            }
        }

        return $outletDaywiseSales;
    }

    private function getIndividualDaywiseStoreSales($id, $request) {
        $startDate = !empty($request->get('start_date')) ? $request->get('start_date') : Carbon::today()->subDays(7)->toDateString();
        $endDate = !empty($request->get('end_date')) ? $request->get('end_date') : Carbon::today()->toDateString();

        return DB::select("select `calendar`.`datefield`, `processed_invoices`.`outlet_id`, `processed_invoices`.`total_received`, `processed_invoices`.`average` from calendar 
            left join ((select IFNULL(SUM(`invoices`.`total_received`), 0) as total_received, CAST(IFNULL(AVG(`invoices`.`total_received`), 0) AS DECIMAL(10, 2)) AS average,
               DATE(`invoices`.`created_at`) AS date, IFNULL(`invoices`.`outlet_id`, NULL) AS outlet_id from `invoices`
              where `invoices`.`created_at` between '".$startDate."' and '".Carbon::parse($endDate)->addDay()->toDateString()."'
                    and `invoices`.`outlet_id` = '".$id."' and `invoices`.`deleted_at` is null group by DATE(`invoices`.`created_at`), `invoices`.`outlet_id`) AS processed_invoices) 
                    on `calendar`.`datefield` = `processed_invoices`.`date`
              where `calendar`.`datefield` >= '".$startDate."' and `calendar`.`datefield` <= '".$endDate."' ORDER BY `calendar`.`datefield` ASC");
    }

    public function storeMonthlySalesReport(Request $request) {
        $outlets = Outlet::select('id', 'name')->get();
        $action = $request->get('action');
        $outletSales = $this->getMonthlySalesForOutlets($outlets, $request);
        $totals = $this->getTotalsForMonthlyStoreSales($request);
        $years = Calendar::select(DB::raw('YEAR(`datefield`) as year'))
                        ->groupBy(DB::raw('YEAR(`datefield`)'))
                        ->get();

        $grandTotal = 0;
        $grandAverage = 0;

        foreach ($totals as $t) {
            $grandTotal = $grandTotal + (float) $t->total;
            $grandAverage = round($grandTotal / count($totals), 2);
        }

        if(!empty($action)) {
            Excel::create('monthly_store_sales_report',
                function ($excel) use ($outletSales, $totals, $grandTotal, $grandAverage) {
                    $excel->sheet('New Sheet', function ($sheet) use ($outletSales, $totals, $grandTotal, $grandAverage) {
                        $sheet->loadView('reports.store_monthly_sales_report.excel', array('outletSales' => $outletSales, 'totals' => $totals, 'grandTotal' => $grandTotal, 'grandAverage' => $grandAverage));
                    });
                })->download('xls');
        }

        return view('reports.store_monthly_sales_report.index', compact('outletSales', 'totals', 'grandTotal', 'grandAverage', 'years'));
    }

    private function getMonthlySalesForOutlets($outlets, $request) {
        $outletDaywiseSales= $outlets;

        foreach ($outletDaywiseSales as $outlet) {
            $outlet->sales = $this->getIndividualMonthlyStoreSales($outlet->id, $request);
            $totalSaleStore = 0;
            foreach ($outlet->sales as $sale) {
                if($outlet->id == $sale->outlet_id) {
                    $totalSaleStore = $totalSaleStore + (float) $sale->total_received;
                }
            }
            $outlet->total_sales = $totalSaleStore;
            if(count($outlet->sales) > 0) {
                $outlet->average = round($totalSaleStore / count($outlet->sales), 2);
            } else {
                $outlet->average = 0.00;
            }
        }

        return $outletDaywiseSales;
    }

    private function getIndividualMonthlyStoreSales($id, $request) {
        $startDate = !empty($request->get('year')) ? Carbon::parse($request->get('year').'-01-01')->startOfYear()->toDateString() : Carbon::today()->startOfYear()->toDateString();
        $endDate = !empty($request->get('year')) ? Carbon::parse($request->get('year').'-01-01')->endOfYear()->toDateString() : Carbon::today()->endOfYear()->toDateString();

        return DB::select("select YEAR(`calendar`.`datefield`) AS year, MONTH(`calendar`.`datefield`) as month, `processed_invoices`.`outlet_id`, `processed_invoices`.`total_received`, `processed_invoices`.`average` from calendar
              left join((select IFNULL(SUM(`invoices`.`total_received`), 0) as total_received, CAST(IFNULL(AVG(`invoices`.`total_received`), 0) AS DECIMAL(10, 2)) AS average,
              MONTH(`invoices`.`created_at`) AS date, IFNULL(`invoices`.`outlet_id`, NULL) AS outlet_id from `invoices`
              where `invoices`.`created_at` between '".$startDate."' and '".Carbon::parse($endDate)->addDay()->toDateString()."'
                    and `invoices`.`outlet_id` = '".$id."' and `invoices`.`deleted_at` is null group by YEAR(`invoices`.`created_at`), MONTH(`invoices`.`created_at`), `invoices`.`outlet_id`) 
                    AS processed_invoices) on MONTH(`calendar`.`datefield`) = `processed_invoices`.`date`
              where `calendar`.`datefield` between '".$startDate."' and '".$endDate."' GROUP BY YEAR(`calendar`.`datefield`), MONTH(`calendar`.`datefield`), `processed_invoices`.`outlet_id`,
            `processed_invoices`.`total_received`,`processed_invoices`.`average` ORDER BY YEAR(`calendar`.`datefield`), MONTH(`calendar`.`datefield`) ASC");
    }

    private function getTotalsForMonthlyStoreSales($request) {
        $startDate = !empty($request->get('year')) ? Carbon::parse($request->get('year').'-01-01')->startOfYear()->toDateString() : Carbon::today()->startOfYear()->toDateString();
        $endDate = !empty($request->get('year')) ? Carbon::parse($request->get('year').'-01-01')->endOfYear()->toDateString() : Carbon::today()->endOfYear()->toDateString();

        return Invoice::select(DB::raw('IFNULL(SUM(`invoices`.`total_received`), 0) as total'), DB::raw('YEAR(`calendar`.`datefield`) as year'),
            DB::raw('MONTH(`calendar`.`datefield`) as month'))
            ->join('calendar', DB::raw('DATE(`invoices`.`created_at`)'), '=', 'calendar.datefield', 'right')
            ->whereBetween('calendar.datefield', [$startDate, $endDate])
            ->groupBy(DB::raw('YEAR(`calendar`.`datefield`)'), DB::raw('MONTH(`calendar`.`datefield`)'))
            ->get();
    }

    public function getSalesReportTotal(Request $request) {
        return json_encode($this->calculateSalesReportTotal($request));
    }

    private function calculateSalesReportTotal($request) {
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        return Invoice::select(DB::raw('SUM(`total_received`) AS total'))
            ->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->get();
    }

    public function getDaywiseSalesReportTotal(Request $request) {
        return json_encode($this->calculateDaywiseSalesReportTotal($request));
    }

    private function calculateDaywiseSalesReportTotal($request) {
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        return Invoice::select(DB::raw('IFNULL(SUM(`invoices`.`product_price_exclusive_vat`), 0) AS product_price'),
            DB::raw('IFNULL(SUM(`invoices`.`vat_amount`), 0) AS vat'),
            DB::raw('IFNULL(SUM(`invoices`.`product_discount_amount`), 0) AS discount'),
            DB::raw('IFNULL(SUM(`invoices`.`card_received`), 0) AS card'),
            DB::raw('IFNULL(SUM(`invoices`.`cash_received`), 0) AS cash'),
            DB::raw('IFNULL(SUM(`invoices`.`total_received`), 0) AS total_price'))
            ->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->get();
    }
}
