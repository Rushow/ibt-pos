<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Company;
use App\Warehouse;
use App\WarehouseStoreInChallan;
use App\WarehouseStoreOutChallan;
use Illuminate\Http\Request;
use App\Http\Requests\WarehouseRequest;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Elibyy\TCPDF\Facades\TCPDF;

class WarehouseInventoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->can('access-warehouse')) {
            abort(401);
        }

        if (request('to_date')) {
            $to_date = Carbon::parse(request('to_date'));
        } else {
            $to_date = Carbon::today();
        }

        $warehouse_id = 0;
        if ($warehouse_id) {
            $warehouses = Warehouse::where('id', $warehouse_id)->orderBy('name')->pluck('name', 'id');
        } else {
            $warehouses = Warehouse::where('company_id', Auth::user()->company_id)->orderBy('name')->pluck('name', 'id');
        }

//        $query ="SELECT warehouse_store_in_challan_product_combinations.product_combination_id,
//                        warehouse_store_in_challan_product_combinations.winQuantity- warehouse_store_out_challan_product_combinations.woutQuantity
//                         as remaining FROM ( SELECT product_combination_id, SUM(quantity) as winQuantity FROM warehouse_store_in_challan_product_combinations
//                          Group BY product_combination_id) as warehouse_store_in_challan_product_combinations
//                          LEFT OUTER JOIN ( SELECT product_combination_id, SUM(quantity) as woutQuantity
//                          FROM warehouse_store_out_challan_product_combinations
//                          Group BY product_combination_id) as warehouse_store_out_challan_product_combinations
//                          ON warehouse_store_in_challan_product_combinations.product_combination_id=warehouse_store_out_challan_product_combinations.product_combination_id";

        $query = 'SELECT oi.product_combination_id,oi.quantity as q1,oo.quantity as q2, (oi.quantity - IFNULL( oo.quantity, 0 ))remaining
            FROM (
                SELECT wicpc.product_combination_id, IFNULL( sum( wicpc.quantity ) , 0 ) quantity
                FROM warehouse_store_in_challan_product_combinations wicpc, warehouse_store_in_challan_products wicp, warehouse_store_in_challans wic
                WHERE wic.id = wicp.warehouse_store_in_challan_id
                AND wicp.id = wicpc.warehouse_store_in_challan_product_id
                AND wic.status ="final" 
                AND wic.request_date <= "' . $to_date . '"
                GROUP BY wicpc.product_combination_id
            )oi
            LEFT JOIN (
                SELECT  wocpc.product_combination_id, IFNULL( sum( wocpc.quantity ) , 0 ) quantity
                FROM warehouse_store_out_challan_product_combinations wocpc, warehouse_store_out_challan_products wocp, warehouse_store_out_challans woc
                WHERE woc.id = wocp.warehouse_store_out_challan_id
                AND wocp.id = wocpc.warehouse_store_out_challan_product_id
                AND woc.status ="final" 
                AND woc.request_date <=  "' . $to_date . '"
                GROUP BY wocpc.product_combination_id
            )oo ON oi.product_combination_id = oo.product_combination_id
            GROUP BY product_combination_id';

        $inventory = DB::select($query);
        return view('warehouse_inventory.index', compact('to_date', 'warehouse_id', 'warehouses', 'inventory'));

    }


    /*
        public function warehouse_inventory_pdf(){
            $warehouse_id = 0;
            if(request('warehouse_id')){
                $warehouse_id = Carbon::parse(request('warehouse_id'));
            }

            if(request('to_date')){
                $to_date = Carbon::parse(request('to_date'));
            }else{
                $to_date = new Carbon();
            }

            if($warehouse_id){
                $warehouses = Warehouse::where('id',$warehouse_id)->orderBy('name')->get();
            }else{
                $warehouses = Warehouse::orderBy('name')->get();
            }

            $inventory = DB::select('SELECT oi.*, p.*,oi.quantity,oo.quantity, (oi.quantity - IFNULL( oo.quantity, 0 ))qty
                FROM (
                    SELECT wicp.product_id, wicpc.product_combination_id, IFNULL( sum( wicpc.quantity ) , 0 ) quantity
                    FROM warehouse_store_in_challan_product_combinations wicpc, warehouse_store_in_challan_products wicp, warehouse_store_in_challans wic
                    WHERE wic.id = wicp.warehouse_store_in_challan_id
                    AND wicp.product_id = wicpc.warehouse_store_in_challan_product_id
                    AND wic.request_date <= :date
                    GROUP BY wicpc.product_combination_id, wicp.product_id, wicpc.quantity
                )oi
                LEFT JOIN (
                    SELECT wocp.product_id, wocpc.product_combination_id, IFNULL( sum( wocpc.quantity ) , 0 ) quantity
                    FROM warehouse_store_out_challan_product_combinations wocpc, warehouse_store_out_challan_products wocp, warehouse_store_out_challans woc
                    WHERE woc.id = wocp.warehouse_store_out_challan_id
                    AND wocp.product_id = wocpc.warehouse_store_out_challan_product_id
                    AND woc.request_date <=  :odate
                    GROUP BY wocpc.product_combination_id, wocp.product_id, wocpc.quantity
                )oo ON oi.product_id = oo.product_id, products p, product_combinations pc, product_combination_attributes pca
                WHERE p.id = pc.product_id
                AND p.id = oi.product_id
                AND pc.id = pca.product_combination_id
                GROUP BY product_combination_id, product_id, oi.quantity, oo.quantity
                Having qty > 0', ['date' => $to_date, 'odate' => $to_date]);


            $pdf = new TCPDF();
            $pdf::SetTitle('Warehouse Inventory '.$to_date->format('Y-m-d'));
            $pdf::AddPage();
            $pdf::writeHTML(view('warehouse_inventory.pdf', compact('inventory'))->render());
            $pdf::Output('Warehouse Inventory '.$to_date->format('Y-m-d').'.pdf');
        }
    */
}
