<?php

namespace App\Http\Controllers;

use App\OutLet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class OutletApiController extends Controller
{
    public function index()
    {
        //
    }

    public function getOutlet($companyId)
    {
        return OutLet::where('company_id', $companyId)->orderBy('id', 'ASC')->get();
    }
}
