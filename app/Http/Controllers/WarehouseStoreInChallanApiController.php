<?php

namespace App\Http\Controllers;

use App\Company;
use App\ProductCombination;
use App\WarehouseStoreInChallan;
use App\WarehouseStoreInChallanProduct;
use App\WarehouseStoreInChallanProductCombination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Psy\Util\Json;
use Carbon\Carbon;

class WarehouseStoreInChallanApiController extends Controller
{
    public function saveWarehouseStoreInChallan(Request $request)
    {
        $product_challan_infos = json_decode($request->get('product_challan_infos'));
        $user = json_decode($request->get('user'));

        try {
            $challan_data['company_id'] = $user->company_id;
            $challan_data['warehouse_id'] = $product_challan_infos[0]->warehouse_id;
            $challan_data['status'] = 'draft';
            $challan_data['request_date'] = $product_challan_infos[0]->request_date;
            $challan_data['created_by'] = $user->id;
            $warehouse_store_in_challan = Company::find($challan_data['company_id'])->warehouseStoreInChallans()->create($challan_data);

            $now = Carbon::now();
            $challan_no_data['challan_no'] = $now->year . $warehouse_store_in_challan->id;
            $warehouse_store_in_challan->update($challan_no_data);
            foreach ($product_challan_infos as $product_challan_info) {
                $challan_product_data['supplier_id'] = $product_challan_info->supplier_id;
                $challan_product_data['product_id'] = $product_challan_info->product_id;
                $warehouse_store_in_challan_product = $warehouse_store_in_challan->challanInProducts()->create($challan_product_data);

                foreach ($product_challan_info->combination as $combination) {
                    if ($combination->quantity) {
                        $product_combination_data['product_combination_id'] = $combination->id;
                        $product_combination_data['quantity'] = $combination->quantity;
                        $warehouse_store_in_challan_product->storeInCombinations()->create($product_combination_data);
                    }
                }
            }
            return json_encode(['success' => 'Challan in was successful']);
        } catch(\Exception $e) {
            return json_encode(['error' => 'Error in challan in']);
        }
    }

    public function updateWarehouseStoreInChallan(Request $request)
    {
        $product_challan_infos = json_decode($request->get('product_challan_infos'));

        if ($product_challan_infos) {
            $warehouse_store_in_challan = WarehouseStoreInChallan::find($request->challan_id);
            $warehouse_store_in_challan->update(['warehouse_id' => $product_challan_infos[0]->warehouse_id, 'request_date' => $product_challan_infos[0]->request_date]);

            foreach ($product_challan_infos as $product_challan_info) {
                $warehouse_store_in_challan_product = WarehouseStoreInChallanProduct::updateOrCreate(
                    ['warehouse_store_in_challan_id' => $request->challan_id, 'product_id' => $product_challan_info->product_id],
                    ['supplier_id' => $product_challan_info->supplier_id]
                );
                foreach ($product_challan_info->combination as $combination) {
                    if ($combination->quantity) {
                        $warehouse_store_in_challan_product_combination = WarehouseStoreInChallanProductCombination::updateOrCreate(
                            ['warehouse_store_in_challan_product_id' => $warehouse_store_in_challan_product->id, 'product_combination_id' => $combination->id],
                            ['quantity' => $combination->quantity]
                        );
                    }
                }
            }
            if ($warehouse_store_in_challan_product_combination) {
                $return_array['success'] = 'Challan Updated successfully';
            } else {
                $return_array['error'] = 'Error: Challan Update Error';
            }
        }
        return $return_array;
    }

    public function getChallanData(Request $request)
    {
        return WarehouseStoreInChallan::where('id', $request->challan_id)->with(
            ['challanInProducts' => function ($query) {
                $query->with(['product', 'storeInCombinations' => function ($query) {
                    $query->with(['combination' => function ($query) {
                        $query->with(['combinationAttributeValues' => function ($query) {
                            $query->with(['attributeValue' => function ($query) {
                                $query->with('attribute');
                            }]);
                        }]);
                    }]);
                }]);
            }])->first();
    }
}
