<?php

namespace App\Http\Controllers;

use App\Company;
use App\Attribute;
use Illuminate\Http\Request;
use App\Http\Requests\AttributeRequest;
use Illuminate\Support\Facades\Auth;

class AttributeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->can('access-product-attributes')) {
            abort(401);
        }
        $attributes = Attribute::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
        return view('attribute.index', compact('attributes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->can('access-product-attributes')) {
            abort(401);
        }
        $type_lists = ['drop-down-list' => 'Drop-down list', 'radio-buttons' => 'Radio buttons', 'color-or-texture' => 'Color or texture'];
        return view('attribute.create', compact('type_lists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttributeRequest $request)
    {
        if (!Auth::user()->can('access-product-attributes')) {
            abort(401);
        }
        Company::find(Auth::user()->company_id)->attributes()->create($request->all());
        flash('Attribute added successfully', 'success');
        return redirect('attribute');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Attribute $attribute)
    {
        if (!Auth::user()->can('access-product-attributes')) {
            abort(401);
        }
        $type_lists = ['drop-down-list' => 'Drop-down list', 'radio-buttons' => 'Radio buttons', 'color-or-texture' => 'Color or texture'];
        return view('attribute.edit', compact('attribute', 'type_lists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AttributeRequest|Request $request
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(AttributeRequest $request, Attribute $attribute)
    {
        if (!Auth::user()->can('access-product-attributes')) {
            abort(401);
        }
        $attribute->update($request->all());
        flash('Attribute updated successfully', 'success');
        return redirect('attribute');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attribute $attribute)
    {
        if (!Auth::user()->can('access-product-attributes')) {
            abort(401);
        }
        if (!empty($attribute)) {
            Attribute::destroy($attribute->id);
            flash('Attribute deleted successfully', 'success');
        }
        return redirect('attribute');
    }

    /**
     * Remove specified resources from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function removeAttributes(Request $request)
    {
        if (!Auth::user()->can('access-product-attributes')) {
            abort(401);
        }
        if (!empty($request->attribute_ids)) {
            Attribute::destroy($request->attribute_ids);
            flash('Attribute deleted successfully', 'success');
        }
        return redirect('attribute');
    }
}
