<?php

namespace App\Http\Controllers;


use App\DiscountAmountType;
use App\Product;
use App\ProductDiscount;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductDiscountRequest;
use Illuminate\Support\Facades\Auth;

class ProductDiscountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        if (!Auth::user()->can('access-product-prices')) {
            abort(401);
        }
        $tab_active = 'discount';
        return view('product_discount.index', compact('product', 'tab_active'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        if (!Auth::user()->can('access-product-prices')) {
            abort(401);
        }
        $amount_lists = DiscountAmountType::pluck('type_name', 'id');
        $status_lists = ['1' => 'Active', '0' => 'Inactive'];
        $tab_active = 'discount';
        return view('product_discount.create', compact('amount_lists', 'product', 'status_lists', 'tab_active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductDiscountRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductDiscountRequest $request, Product $product)
    {
        //dd($request);

        if (!Auth::user()->can('access-product-prices')) {
            abort(401);
        }
        $product->discount()->create($request->all());

        flash('Product Discount added successfully', 'success');
        return redirect()->back();
    }

    /**
     * Toggle Status for the specified resource.
     *
     * @param  $data ,int  $price_id
     * @return \Illuminate\Http\Response
     */
    protected function toggleStatus($data, $discount_id)
    {
        ProductDiscount::where('id', '!=', $discount_id)->update($data);
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product, $id)
    {
        if (!Auth::user()->can('access-product-prices')) {
            abort(401);
        }

        $product_discount = ProductDiscount::find($id);
        $amount_lists = DiscountAmountType::pluck('type_name', 'id');
        $status_lists = ['1' => 'Active', '0' => 'Inactive'];
        $tab_active = 'discount';
        return view('product_discount.edit', compact('amount_lists', 'product', 'product_discount', 'status_lists', 'tab_active'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductDiscountRequest $request, Product $product, ProductDiscount $product_discount)
    {
        if (!Auth::user()->can('access-product-prices')) {
            abort(401);
        }
        $product_discount->update($request->all());
        if ($request->active) {
            $data['active'] = '0';
            $this->toggleStatus($data, $product_discount->id);
        }
        flash('Product Discount updated successfully', 'success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Product $product, ProductDiscount $product_discount)
    {
        if (!Auth::user()->can('access-product-prices')) {
            abort(401);
        }
        if (!empty($product_discount)) {
            ProductDiscount::destroy($product_discount->id);
            flash('Product discount deleted successfully', 'success');
        }
        return redirect('product/' . $product->id . '/product_discount_price');
    }

    /**
     * Remove specified resources from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function removeProductDiscounts(Request $request, Product $product)
    {
        if (!Auth::user()->can('access-product-prices')) {
            abort(401);
        }
        if (!empty($request->product_price_ids)) {
            ProductDiscount::destroy($request->product_price_ids);
            flash('Product discount deleted successfully', 'success');
        }
        return redirect('product/' . $product->id . '/product_discount_price');
    }

}
