<?php

namespace App\Http\Controllers;

use App\Company;
use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->can('access-category')) {
            abort(401);
        }
        $categories = Category::where('company_id', Auth::user()->company_id)->orderBy('id', 'ASC')->get();
        return view('category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->can('access-category')) {
            abort(401);
        }
        $category = '';
        $category_parent_data = Category::where('parent_id', 0)->where('company_id', Auth::user()->company_id)->first();
        $category_parent = $category_parent_data->id;
        return view('category.create', compact('category_parent', 'category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        if (!Auth::user()->can('access-category')) {
            abort(401);
        }
        $category_data = $request->all();
        $companies = Company::find(Auth::user()->company_id);
        $current_position = DB::table('categories')->where('company_id', Auth::user()->company_id)->max('position');
        $parent_category = Category::find($request->parent);
        $current_level_depth = $parent_category->level_depth;
        $category_data['parent_id'] = $request->parent;
        $category_data['level_depth'] = $current_level_depth + 1;
        $category_data['active'] = $request->active ? 1 : 0;
        $category_data['position'] = $current_position + 1;
        $companies->categories()->create($category_data);
        flash('Category added successfully', 'success');
        return redirect('category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->can('access-category')) {
            abort(401);
        }
        $category = Category::find($id);
        $category_parent = $category->parent_id;
        return view('category.edit', compact('category', 'category_parent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        if (!Auth::user()->can('access-category')) {
            abort(401);
        }
        $categories = Category::find($id);
        $parent_category = Category::find($request->parent);
        $current_level_depth = $parent_category->level_depth;
        $category_data['parent_id'] = $request->parent;
        $category_data['level_depth'] = $current_level_depth + 1;
        $category_data['active'] = $request->active ? 1 : 0;
        $categories->update(array_merge($request->all(), $category_data));
        flash('Category updated successfully', 'success');
        return redirect('category');
    }

    /**
     * Remove multiple resources from storage.
     *
     * @param  $category object
     * @return \Illuminate\Http\Response
     */
    public function removeChildCategories($categories)
    {
        foreach ($categories as $category) {
            Category::destroy($category->id);
            if (count($category->childs)) {
                $this->removeChildCategories($category->childs);
            }
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!Auth::user()->can('access-category')) {
            abort(401);
        }
        if (!empty($request->category_ids)) {
            foreach ($request->category_ids as $category_id) {
                $categories = Category::where('id', '=', $category_id)->get();
                $this->removeChildCategories($categories);
            }
            flash('Category deleted successfully', 'success');
        }
        return redirect('category');
    }
}
