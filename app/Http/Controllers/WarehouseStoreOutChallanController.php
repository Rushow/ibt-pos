<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Company;
use App\Http\Requests\WarehouseStoreOutChallanProductRequest;
use App\Warehouse;
use App\Supplier;
use App\Category;
use App\Product;
use App\WarehouseStoreOutChallan;
use Illuminate\Support\Facades\Auth;

class WarehouseStoreOutChallanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->can('access-warehouse-store-out')) {
            abort(401);
        }
        $store_out_challans = WarehouseStoreOutChallan::with('challanoutProducts')->where('company_id', Auth::user()->company_id)->orderBy('id', 'DESC')->get();

        return view('warehouse_storeout_challan.index', compact('store_out_challans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->can('access-warehouse-store-out')) {
            abort(401);
        }

        return view('warehouse_storeout_challan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(WarehouseStoreOutChallanRequest $request)
    {
        if (!Auth::user()->can('access-warehouse-store-out')) {
            abort(401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(WarehouseStoreOutChallan $store_out_challan)
    {
        if (!Auth::user()->can('access-warehouse-store-out')) {
            abort(401);
        }
        return view('warehouse_storeout_challan.show', compact('store_out_challan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(WarehouseStoreOutChallan $store_out_challan)
    {
        if (!Auth::user()->can('access-warehouse-store-out')) {
            abort(401);
        }

        return view('warehouse_storeout_challan.edit', compact('store_out_challan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AttributeRequest|Request $request
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(AttributeRequest $request, Attribute $attribute)
    {
        if (!Auth::user()->can('access-warehouse-store-out')) {
            abort(401);
        }
        $attribute->update($request->all());
        flash('Attribute updated successfully', 'success');
        return redirect('attribute');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(WarehouseStoreOutChallan $warehouseStoreOutChallan)
    {
        if (!Auth::user()->can('access-warehouse-store-out')) {
            abort(401);
        }
//        echo($id);
        if (!empty($warehouseStoreOutChallan->id)) {
            try {
                WarehouseStoreOutChallan::destroy($warehouseStoreOutChallan->id);
                flash('Warehouse store out challan deleted successfully', 'success');
            } catch (\Exception $ex) {
                flash($ex, 'error');
            }
        }
        return redirect('warehouse_store_out_challan');
    }

    /**
     * Remove specified resources from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function removeAttributes(Request $request)
    {
        if (!Auth::user()->can('access-warehouse-store-out')) {
            abort(401);
        }
        if (!empty($request->attribute_ids)) {
            Attribute::destroy($request->attribute_ids);
            flash('Attribute deleted successfully', 'success');
        }
        return redirect('attribute');
    }
}
