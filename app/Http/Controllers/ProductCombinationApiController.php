<?php

namespace App\Http\Controllers;

use App\Company;
use App\Attribute;
use App\AttributeValue;
use App\Product;
use App\Category;
use App\CategoryProduct;
use App\ProductCombination;
use App\ProductCombinationAttribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductCombinationApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    protected function getCombinationData(Request $request)
    {
        $combination_data = Product::where('id', $request->product_id)->with(
            ['combinations' => function ($query) {
                $query->with([
                    'combinationAttributeValues' => function ($query) {
                        $query->with(['attributeValue' => function ($query) {
                            $query->with('attribute');
                        }]);
                    }
                ]);
            }
            ]
        )->first();
        return $combination_data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
