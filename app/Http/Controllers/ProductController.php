<?php

namespace App\Http\Controllers;

use App\Product;
use App\Company;
use App\Category;
use App\CategoryProduct;
use App\ProductCombination;
use App\ProductImage;
use App\ProductPrice;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Input;
use File;
use DB;

use Elibyy\TCPDF\Facades\TCPDF;

//use \Milon\Barcode\DNS1D;
//use \Milon\Barcode\DNS2D;


class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->can('create-view-products')) {
            abort(401);
        }

        $products = Product::where('company_id', Auth::user()->company_id)->orderBy('id', 'ASC')->get();
        return view('product.index', compact('products'));
    }

    /**
     * Generate and Display Barcode Printable
     *
     * @return \Illuminate\Http\Response
     */
    public function generateBarcode()
    {
        //QrCode::generate('Make me into a QrCode!');
        $categories = Category::where('company_id', Auth::user()->company_id)->orderBy('name')->pluck('name', 'id');
        $products = Product::where('company_id', Auth::user()->company_id)->orderBy('name')->pluck('name', 'id');
        $product_combinations = ProductCombination::orderBy('name')->pluck('name', 'id');
        return view('product.barcode', compact('categories', 'products', 'product_combinations'));
    }

    public function generateBarcodePdf(Request $request)
    {
        $barcode_generate_infos = json_decode($request->get('barcode_generate_infos'));
//        if( $barcode_generate_infos ) {
//
//        }
//        $category_id = 0;
//        if($request->get('category_id')){
//            $category_id = $request->get('category_id');
//        }
//        $product_id = 0;
//        if($request->get('product_id')){
//            $product_id = $request->get('product_id');
//        }
//        $product_combination_id = 0;
//        if($request->get('product_combination_id')){
//            $product_combination_id = $request->get('product_combination_id');
//        }
        $pdf = new TCPDF();
        $pdf::SetTitle('Product Barcode');
        $pdf::AddPage();
        $pdf::writeHTML(view('product.barcode_pdf', compact('barcode_generate_infos'))->render());
        $pdf::Output($_SERVER['DOCUMENT_ROOT'] . '/Product Barcode.pdf', 'I');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->can('create-view-products')) {
            abort(401);
        }

        $category = '';
        $product = '';
        $tab_active = 'product';
        $category_parent_data = Category::where('parent_id', 0)->where('company_id', Auth::user()->company_id)->first();
        $category_parent = $category_parent_data->id;
        return view('product.create', compact('category_parent', 'category', 'product', 'tab_active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        if (!Auth::user()->can('create-view-products')) {
            abort(401);
        }
        $product_data = $request->all();
        $last_prime_key = Product::select("id")->orderBy("id", "DESC")->first();
        if ($last_prime_key) {
            $product_data['item_code'] = str_pad(($last_prime_key->id + 1), 6, '0', STR_PAD_LEFT);
        } else {
            $last_prime_key = 0;
            $product_data['item_code'] = str_pad(($last_prime_key + 1), 6, '0', STR_PAD_LEFT);
        }

        $product = Company::find(Auth::user()->company_id)->products()->create($product_data);
        $product->categories()->sync(explode(',', $request->product_categories));
        flash('Product added successfully', 'success');
        return redirect('product/' . $product->id . '/product_image');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        if (!Auth::user()->can('edit-update-products')) {
            abort(401);
        }
        $category = '';
        $tab_active = 'product';
        $category_parent = '';
        return view('product.edit', compact('category_parent', 'category', 'product', 'tab_active'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        if (!Auth::user()->can('edit-update-products')) {
            abort(401);
        }
        $product->update($request->all());
        $product->categories()->sync(explode(',', $request->product_categories));
        flash('Product updated successfully', 'success');
        return redirect()->back();
    }

    /**
     * Remove multiple resources from storage.
     *
     * @param  $category object
     * @return \Illuminate\Http\Response
     */
    public function removeChildCategories($categories)
    {
        foreach ($categories as $category) {
            Category::destroy($category->id);
            if (count($category->childs)) {
                $this->removeChildCategories($category->childs);
            }
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if (!Auth::user()->can('delete-products')) {
            abort(401);
        }
        if (!empty($product)) {
            Product::destroy($product->id);
            ProductPrice::where('product_id', '=', $product->id)->delete();
            ProductImage::where('product_id', '=', $product->id)->delete();
            CategoryProduct::where('product_id', '=', $product->id)->delete();
            flash('Product deleted successfully', 'success');
        }
        return redirect('product');
    }

    /**
     * Remove specified resources from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function removeProducts(Request $request)
    {
        if (!Auth::user()->can('delete-products')) {
            abort(401);
        }
        if (!empty($request->product_ids)) {
            Product::destroy($request->product_ids);
            flash('Product deleted successfully', 'success');
        }
        return redirect('product');
    }
}
