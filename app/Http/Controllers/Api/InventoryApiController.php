<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OutletIn;
use Illuminate\Support\Facades\Auth;
use Response;
use DB;

class InventoryApiController extends Controller
{
    public function getInventoryChallans($id)
    {
        if (!Auth::user()->can('access-inventory-api')) {
            abort(401);
        }
        $challans = OutletIn::with('product')
            ->with('combination')
            ->with('warehouseChallan')
            ->where('outlet_id', '=', $id)
            ->where('deleted_at', null)
            ->paginate(10);

        return Response::json(array(
            'error' => false,
            'id' => $id,
            'inventory_challans' => $challans,
            'status_code' => 200
        ));
    }

    public function getInventoryProducts($id)
    {
        if (!Auth::user()->can('access-inventory-api')) {
            abort(401);
        }
        $inventoryProducts = $this->getProductsInInventory($id)
            ->groupBy('outlet_ins.product_id', 'outlet_ins.product_combination_id', 'outlet_ins.outlet_id', 'sold')
            ->paginate(10);

        return Response::json(array(
            'error' => false,
            'id' => $id,
            'inventory_products' => $inventoryProducts,
            'status_code' => 200
        ));
    }

    public function searchInventory($id, Request $request)
    {
        if (!Auth::user()->can('access-inventory-api')) {
            abort(401);
        }
        $inventorySearchResult = $this->getProductsInInventory($id);

        if (!empty($request->input('search'))) {
            $inventorySearchResult = $inventorySearchResult->where('products.name', 'LIKE', '%' . $request->input('search') . '%')
                ->orWhere('products.item_code', 'LIKE', '%' . $request->input('search') . '%')
                ->orWhere('products.reference_code', 'LIKE', '%' . $request->input('search') . '%')
                ->orWhere('product_combinations.name', 'LIKE', '%' . $request->input('search') . '%')
                ->orWhere('product_combinations.barcode', 'LIKE', '%' . $request->input('search') . '%');
        }

        $inventorySearchResult = $inventorySearchResult->groupBy('outlet_ins.product_id', 'outlet_ins.product_combination_id', 'outlet_ins.outlet_id', 'sold')
            ->paginate(10);

        return Response::json(array(
            'error' => false,
            'id' => $id,
            'inventory_products' => $inventorySearchResult,
            'status_code' => 200
        ));
    }

    public function getProductsInInventory($id)
    {
        return OutletIn::select('outlet_ins.product_id', 'products.name as product_name', 'products.item_code', 'products.reference_code', 'products.active', 'product_combinations.name as product_attribute',
            'product_combinations.barcode as barcode', 'outlet_ins.product_combination_id', 'outlet_ins.outlet_id')
            ->selectRaw('SUM(`quantity`) AS total_product')
            ->selectRaw('`sold_items`.`sold` AS product_sold')
            ->selectRaw('(SUM(`quantity`) - `sold_items`.`sold`) AS product_left')
            ->leftJoin(DB::raw('(SELECT SUM(`quantity`) AS `sold`, `outlet_id`, `product_combination_id`, `product_id` FROM outlet_outs GROUP BY `product_combination_id`, `product_id`, `outlet_id`) sold_items'), function ($join) {
                $join->on('sold_items.outlet_id', '=', 'outlet_ins.outlet_id');
                $join->on('sold_items.product_id', '=', 'outlet_ins.product_id');
                $join->on('sold_items.product_combination_id', '=', 'outlet_ins.product_combination_id');
            })
            ->join('products', 'outlet_ins.product_id', '=', 'products.id')
            ->join('product_combinations', 'outlet_ins.product_combination_id', '=', 'product_combinations.id')
            ->where('outlet_ins.outlet_id', '=', $id)
            ->where('outlet_ins.deleted_at', null)
            ->where('products.active', true);
    }
}
