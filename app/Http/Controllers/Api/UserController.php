<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Response;
use Input;


class UserController extends Controller
{
    public function __construct()
    {
        $this->content = array();
    }

    public function login(Request $request)
    {


        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
            $user = Auth::user();
            $this->content['token'] = $user->createToken('ibt-pos')->accessToken;
            $status = 200;
        } else {
            $this->content['error'] = "Unauthorised";
            $status = 401;
        }
        return response()->json($this->content, $status);
    }

    public function details()
    {
        return response()->json(['user' => Auth::user()]);
    }
}
