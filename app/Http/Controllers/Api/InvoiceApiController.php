<?php

namespace App\Http\Controllers\Api;

use App\InvoiceProduct;
use App\OutletOut;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Response;
use App\Invoice;
use Validator;
use App\Customer;
use App\OutLetUser;

class InvoiceApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($invoice_no)
    {
        if (!Auth::user()->can('access-invoice-api')) {
            abort(401);
        }
        if ($invoice_no == null) {
            $invoice = Invoice::with('InvoiceProduct')->get();
        } else {
            $invoice = Invoice::with('InvoiceProduct')->where("invoice_no", $invoice_no)->get();
        }
        return Response::json(array(
            'error' => false,
            'message' => "Successful",
            'invoice' => $invoice,
            'status_code' => 200
        ));
    }


    public function postInvoice(Request $request)
    {
        if (!Auth::user()->can('access-invoice-api')) {
            abort(401);
        }

        $validator = Validator::make($request->all(), [
            'outlet_id' => 'required|integer',
            'customer_name' => 'required|string',
            'customer_cell_phone' => 'required|numeric',
            'invoice_no' => 'required|integer',
            'product_price_exclusive_vat' => 'required|numeric',
            'vat_amount' => 'required|numeric',
            'product_discount_amount' => 'required|numeric',
            'total_received' => 'required|numeric',
            'credit_card_type' => 'sometimes|string',
            'card_received' => 'required|numeric',
            'cash_received' => 'required|numeric',
            'total_received' => 'required|numeric',
            'sales_note' => 'sometimes|string',

        ]);

        if ($validator->fails()) {
            return Response::json(array(
                'error' => true,
                'message' => $validator->errors(),
                'status_code' => 200
            ));

        } else {
            $invoice_data = $request->all();
            //check if customer exist
            $customer = Customer::where("cell_phone", $request->get("customer_cell_phone"))->first();
            if ($customer) {
                $invoice_data["customer_id"] = $customer->id;
            } else {
                //create customer `
                $c = new Customer();
                $c->first_name = $request->get("customer_name");
                $c->last_name = $request->get("customer_name");
                $c->cell_phone = $request->get("customer_cell_phone");
                $c->out_let_id = $request->get("outlet_id");
                $c->save();
                $invoice_data["customer_id"] = $c->id;
            }

            $invoice = new Invoice();
            $invoice_data["sales_by"] = Auth::user()->id;
            $flag = $invoice->create($invoice_data);
            if ($flag) {
                return Response::json(array(
                    'error' => false,
                    'message' => "Successful",
                    'invoice_id' => $flag->id,
                    'status_code' => 200
                ));
            } else {
                return Response::json(array(
                    'error' => true,
                    'message' => "failed!",
                    'status_code' => 200
                ));
            }

        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postInvoiceProduct(Request $request)
    {
        if (!Auth::user()->can('access-invoice-api')) {
            abort(401);
        }
        $validator = Validator::make($request->all(), [
            'invoice_id' => 'required|integer',
            'product_id' => 'required|integer',
            'product_combination_id' => 'required|integer',
            'quantity' => 'required|integer',
            'product_discounts_id' => 'sometimes|integer',
            'discount_rules_id' => 'sometimes|integer',
            'discount_code_id' => 'sometimes|integer',

        ]);

        if ($validator->fails()) {
            return Response::json(array(
                'error' => true,
                'message' => $validator->errors(),
                'status_code' => 200
            ));

        } else {

            $invoice_product_data = $request->all();
            $invoice = new InvoiceProduct();
            $result = $invoice->create($invoice_product_data);
            if ($result) {
                //put product in outlet_out table
                //get outlet id for user
                $outletuser = OutLetUser::where("user_id", Auth::user()->id)->first();
                if ($outletuser) {

                    $outlet_out = array();
                    $outlet_out['outlet_id'] = $outletuser->out_let_id;
                    $outlet_out['product_id'] = $request->get("product_id");
                    $outlet_out['product_combination_id'] = $request->get("product_id");
                    $outlet_out['quantity'] = $request->get("product_id");
                    $outlet_out['invoice_id'] = $request->get("invoice_id");
                    $outlet_out_entry = new OutletOut();
                    $result = $outlet_out_entry->create($outlet_out);

                    if ($result) {
                        return Response::json(array(
                            'error' => false,
                            'message' => "Successful",
                            'invoice' => $result,
                            'status_code' => 200
                        ));

                    } else {

                        return Response::json(array(
                            'error' => false,
                            'message' => "Invoice Successful. But coudn't able to add to outlet_out! Call admin!",
                            'invoice' => $result,
                            'status_code' => 200
                        ));
                    }


                } else {

                    return Response::json(array(
                        'error' => true,
                        'message' => "Outlet_id didn't match with user id",
                        'invoice' => $result,
                        'status_code' => 200
                    ));

                }

            }

        }
    }


}
