<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Response;
use Validator;
use App\Discount;
use App\DiscountCodeType;
use App\DiscountCode;
use App\DiscountTypeConfigurationCategory;
use App\DiscountTypeConfigurationCode;
use App\DiscountTypeConfigurationOutlet;
use App\DiscountTypeConfigurationDay;


class DiscountRuleApiController extends Controller
{
    public function getDiscountRules(Request $request, $id = null)
    {
        if (!Auth::user()->can('access-discount-rules-api')) {
            abort(401);
        }
        $validator = Validator::make($request->all(), [
            'outlet_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return Response::json(array(
                'error' => true,
                'message' => $validator->errors(),
                'status_code' => 200
            ));

        } else {
            $outlet_id = $request->get("outlet_id");
            if ($id == null) {
                $discounts = Discount::with('DiscountType', 'DiscountAmountType', 'DiscountTypeConfigurationCategory', 'DiscountTypeConfigurationOutlet', 'DiscountTypeConfigurationDay')->where("active", 1)->get();
            } else {
                $discounts = Discount::with('DiscountType', 'DiscountAmountType', 'DiscountTypeConfigurationCategory', 'DiscountTypeConfigurationOutlet', 'DiscountTypeConfigurationDay')->where("active", 1)->find($id);
            }
            return Response::json(array(
                'error' => false,
                'message' => "Successful",
                'discounts' => $discounts,
                'status_code' => 200
            ));
        }

    }

    public function validateCoupon(Request $request)
    {
        if (!Auth::user()->can('access-validatecoupon-api')) {
            abort(401);
        }
        $validator = Validator::make($request->all(), [
            'code' => 'required|string',
        ]);

        if ($validator->fails()) {
            return Response::json(array(
                'error' => true,
                'message' => $validator->errors(),
                'status_code' => 200
            ));

        } else {
            $couponcode = $request->get("code");
            if ($couponcode) {
                $code = DiscountCode::with('Discount', 'Discount.DiscountAmountType', 'DiscountCodeType')->where("code", $couponcode)->where("active", 1)->get();
                return Response::json(array(
                    'error' => false,
                    'message' => "Successful",
                    'code' => $code,
                    'status_code' => 200
                ));
            } else {
                return Response::json(array(
                    'error' => true,
                    'message' => "No coupon code is not valid!",
                    'status_code' => 200
                ));
            }
        }
    }
}
