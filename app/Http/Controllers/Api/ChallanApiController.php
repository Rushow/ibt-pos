<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\WarehouseStoreoutChallan;
use Response;
use Validator;


class ChallanApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id = null)
    {
        if (!Auth::user()->can('access-challan-api')) {
            abort(401);
        }
        if ($id == null) {
            $challan = WarehouseStoreoutChallan::with('challanoutProducts', 'challanoutProducts.storeOutCombinations')->where("status", "final")->get();
        } else {
            $challan = WarehouseStoreoutChallan::with('challanoutProducts', 'challanoutProducts.storeOutCombinations')->where("status", "final")->find($id);
        }
        return Response::json(array(
            'error' => false,
            'challan' => $challan,
            'message' => "Successful",
            'status_code' => 200
        ));
    }

    public function challanRecievedByOutlet(Request $request, $id = null)
    {
//        return Response::json(array(
//            'error' => false,
//            'message' => "Successful",
//            'challan' => $challan,
//            'status_code' => 200
//        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // param
        //outlet_id,challan_id

        return Response::json(array(
            'error' => false,
            //'challan' => $challan,
            'status_code' => 200
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
