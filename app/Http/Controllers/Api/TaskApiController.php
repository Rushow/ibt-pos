<?php

namespace App\Http\Controllers\Api;

use App\OutletIn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Task;
use App\TaskType;
use App\WarehouseStoreoutChallan;
use Response;
use Validator;

class TaskApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!Auth::user()->can('access-task-api')) {
            abort(401);
        }
        $validator = Validator::make($request->all(), [
            'outlet_id' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return Response::json(array(
                'error' => true,
                'message' => $validator->errors(),
                'status_code' => 200
            ));
        } else {

            $outlet_id = $request->get('outlet_id');
            $status = $request->get('status');
            if ($outlet_id == null) {
                $tasks = Task::with("TaskType")->where("outlet_id", $outlet_id)->where("status", $status)->get();
            } else {
                $tasks = Task::with("TaskType")->where("outlet_id", $outlet_id)->where("status", $status)->get();
            }

            return Response::json(array(
                'error' => false,
                'message' => "Successful",
                'tasks' => $tasks,
                'status_code' => 200
            ));
        }

    }

    public function taskStatusSyncd(Request $request)
    {
        if (!Auth::user()->can('sync-task-api')) {
            abort(401);
        }
        $validator = Validator::make($request->all(), [
            'task_id' => 'required',
            'outlet_id' => 'required',
        ]);

        if ($validator->fails()) {
            return Response::json(array(
                'error' => true,
                'message' => $validator->errors(),
                'status_code' => 200
            ));

        } else {
            $task_id = $request->get("task_id");
            if ($task_id != null) {
                $tasks = Task::find($task_id);
                if ($tasks->outlet_id != $request->get("outlet_id")) {
                    return Response::json(array(
                        'error' => true,
                        'message' => "Sorry! task id doesn't match for outlet_id",
                        'tasks' => "Status updated to synced!",
                        'status_code' => 200
                    ));

                } else {

                    if ($tasks->status == 1) {
                        return Response::json(array(
                            'error' => true,
                            'message' => "Successful",
                            'tasks' => "Task synced previously! Nothing to sync!",
                            'status_code' => 200
                        ));

                    } else {
                        //put product in outlet_in table for those challan
                        $rtn = $this->putChallaninOutletIn($tasks->warehouse_store_out_challan_id, $tasks->outlet_id);
                        if ($rtn) {
                            $tasks->status = 1;
                            $result = $tasks->save();
                            if ($result) {
                                return Response::json(array(
                                    'error' => false,
                                    'message' => "Successful",
                                    'tasks' => "Status updated to synced!",
                                    'status_code' => 200
                                ));
                            }

                        } else {
                            return Response::json(array(
                                'error' => false,
                                'message' => "Successful",
                                'tasks' => "Fail to put in outlet in! Call admin!",
                                'status_code' => 200
                            ));

                        }

                    }

                }


            }
        }
    }

    private function putChallaninOutletIn($challan_id, $outlet_id)
    {

        //get challan
        $challan = WarehouseStoreoutChallan::find($challan_id);
        //get product
        foreach ($challan->challanoutProducts as $challanoutProductlist) {
            //get product combination
            foreach ($challanoutProductlist->storeOutCombinations as $pcombination) {
                $outletin = new OutletIn();
                $outletin->outlet_id = $outlet_id;
                $outletin->product_id = $challanoutProductlist->product_id;
                $outletin->product_combination_id = $pcombination->product_combination_id;
                $outletin->quantity = $pcombination->quantity;
                $outletin->warehouse_store_out_challans_id = $challan_id;
                $outletin->save();

            }
        }

        return true;

    }

    public function getTaskType()
    {
        if (!Auth::user()->can('access-task-api')) {
            abort(401);
        }
        $taskType = TaskType::all();
        if ($taskType) {
            return Response::json(array(
                'error' => false,
                'message' => "Successful",
                'taskType' => $taskType,
                'status_code' => 200
            ));
        } else {
            return Response::json(array(
                'error' => false,
                'message' => "No Result",
                'taskType' => $taskType,
                'status_code' => 200
            ));
        }
    }

}
