<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Invoice;
use Carbon\Carbon;
use Response;
use DB;
use Auth;

class SalesApiController extends Controller
{
    public function dailySalesReportStore(Request $request)
    {
        if (!Auth::user()->can('access-sales-api')) {
            abort(401);
        }

        $query = Invoice::select(DB::raw('GROUP_CONCAT(`i_p`.`name`) AS name'), DB::raw('GROUP_CONCAT(`i_p`.`quantity`) AS quantity'),
            'invoices.id', 'invoices.invoice_no','i_p.invoice_id', 'invoices.created_at', 'invoices.product_price_exclusive_vat as total_without_vat',
            'invoices.card_received', 'invoices.cash_received', 'invoices.total_received', 'invoices.vat_amount', 'invoices.product_discount_amount as discount',
            DB::raw('GROUP_CONCAT(`product_combinations`.`barcode`) AS barcode'), DB::raw('GROUP_CONCAT(`i_p`.`price`) AS price'))
            ->join(DB::raw('(SELECT `invoice_products`.`invoice_id`, `invoice_products`.`product_id`, `products`.`name`, `invoice_products`.`quantity`, 
                            `product_prices`.`price` FROM `invoice_products` JOIN `products` ON `invoice_products`.`product_id` = `products`.`id` 
                            JOIN `product_prices` ON `products`.`id` = `product_prices`.`product_id`) AS i_p'), 'invoices.id', '=', 'i_p.invoice_id')
            ->join('product_combinations', 'product_combinations.product_id', '=', 'i_p.product_id');

        if(!empty($request->get('start_date'))) {
            $query->where('invoices.created_at', '>=', $this->start_date);
        } else {
            $query->where('invoices.created_at', '>=', Carbon::today()->subDays(7)->toDateString());
        }

        if(!empty($request->get('end_date'))) {
            $query->where('invoices.created_at', '<=', $this->end_date);
        } else {
            $query->where('invoices.created_at', '<=', Carbon::today()->toDateString());
        }

        $query->groupBy('i_p.invoice_id')->paginate(10);

        return Response::json(array(
            'error' => false,
            'sold_products' => $query,
            'status_code' => 200
        ));
    }

    public function salesByDateStore($id, Request $request)
    {
        if (!Auth::user()->can('access-sales-api')) {
            abort(401);
        }

        $salesThroughDates = Invoice::select(DB::raw('SUM(`total_received`) AS total_sold'), DB::raw('DATE(`created_at`) AS date'))
            ->where('outlet_id', '=', $id)
            ->where('deleted_at', '=', null)
            ->where('created_at', '>=', $request->query('starting_date') . ' 00:00:00')
            ->where('created_at', '<=', $request->query('ending_date') . ' 11:59:59')
            ->groupBy(DB::raw('DATE(`created_at`)'))
            ->get();

        return Response::json(array(
            'error' => false,
            'sales' => $salesThroughDates,
            'status_code' => 200
        ));
    }

    public function makeSale(Request $request)
    {
        $salesData = json_decode($request->input('items'), true);
        var_dump($salesData);
        die();
    }
}
