<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductCombination;
use Response;
use Validator;

class ProductInfoApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
//        if (!Auth::user()->can('access-product-api')) {
//            abort(401);
//        }

        if ($id == null) {
            $products = Product::with('combinations', 'pricesActive', 'discountActive', 'categories')->select("id", "item_code", "name")->get();
        } else {
            $products = Product::with('combinations', 'pricesActive', 'discountActive', 'categories')->select("id", "item_code", "name")->find($id);
        }
        return Response::json(array(
            'error' => false,
            'message' => "Successful",
            'products' => $products,
            'status_code' => 200
        ));
    }

    public function productCombinations($id = null)
    {
        if (!Auth::user()->can('access-product-api')) {
            abort(401);
        }
        if ($id == null) {
            $products = ProductCombination::with('product', 'product.pricesActive', 'product.discountActive', 'product.categories')->select("id", "product_id", "barcode", "name")->get();
        } else {
            $products = ProductCombination::with('product', 'product.pricesActive', 'product.discountActive', 'product.categories')->select("id", "product_id", "barcode", "name")->find($id);
        }
        return Response::json(array(
            'error' => false,
            'message' => "Successful",
            'products' => $products,
            'status_code' => 200
        ));

    }

    public function productBarcode(Request $request, $id = null)
    {
        if (!Auth::user()->can('access-product-api')) {
            abort(401);
        }
        $validator = Validator::make($request->all(), [
            'barcode' => 'required|string',
        ]);

        if ($validator->fails()) {
            return Response::json(array(
                'error' => true,
                'message' => $validator->errors(),
                'status_code' => 200
            ));

        } else {
            $barcode = $request->get("barcode");
            if ($barcode == null) {
                return Response::json(array(
                    'error' => true,
                    'message' => "Barcode can't be empty!",
                    'status_code' => 200
                ));
            } else {
                $products = ProductCombination::with('product', 'product.pricesActive', 'product.discountActive', 'product.discountActive.DiscountAmountType', 'product.categories')->where("barcode", "=", $barcode)->select("id", "product_id", "barcode", "name")->get();
                return Response::json(array(
                    'error' => false,
                    'message' => "Successful",
                    'products' => $products,
                    'status_code' => 200
                ));
            }
        }

    }
}
