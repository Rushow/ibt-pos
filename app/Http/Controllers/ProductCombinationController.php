<?php

namespace App\Http\Controllers;

use App\Product;
use App\Attribute;
use App\ProductCombination;
use Illuminate\Http\Request;
use App\Http\Requests\ProductCombinationRequest;
use Illuminate\Support\Facades\Auth;

class ProductCombinationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        if (!Auth::user()->can('access-product-combination')) {
            abort(401);
        }
        $tab_active = 'combination';
//        $combinations = ProductCombination::orderBy('id', 'ASC')->get();
//        echo '<pre>';
//        print_r($combinations);die;
        return view('product_combination.index', compact('product', 'tab_active'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        if (!Auth::user()->can('access-product-combination')) {
            abort(401);
        }
        $product_lists = Product::orderBy('name', 'ASC')->pluck('name', 'id');
        $attributes = Attribute::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
        $tab_active = 'combination';
        return view('product_combination.create', compact('product', 'tab_active', 'attributes', 'product_lists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->can('access-product-combination')) {
            abort(401);
        }
        $product = Product::findorfail($request->product_id);
        $barcode = str_pad($request->product_id, 6, '0', STR_PAD_LEFT);
        $last_prime_key = ProductCombination::select("id")->orderBy("id", "DESC")->first();
        if ($last_prime_key) {
            $barcode = $barcode . str_pad(($last_prime_key->id + 1), 6, '0', STR_PAD_LEFT);
        } else {
            $last_prime_key = 0;
            $barcode = $barcode . str_pad(($last_prime_key + 1), 6, '0', STR_PAD_LEFT);
        }


        $combination = $product->combinations()->create(['product_id' => $request->product_id, 'name' => $request->name, 'barcode' => $barcode]);
        $combination_id = $combination->id;


        $combination_attributes = explode(',', $request->product_combination);

        foreach ($combination_attributes as $attribute) {
            $combination = ProductCombination::findorfail($combination_id);
            $combination->combinationAttributeValues()->create(['attribute_value_id' => $attribute, 'product_combination_id' => $combination_id]);

        }
        flash('Product Combination added successfully', 'success');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product, ProductCombination $product_combination)
    {
        if (!Auth::user()->can('access-product-combination')) {
            abort(401);
        }
        $product_lists = Product::orderBy('name', 'ASC')->pluck('name', 'id');
        $attributes = Attribute::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
//        $combination = ProductCombination::findorfail( $id );
        $tab_active = 'combination';
        return view('product_combination.edit', compact('product', 'tab_active', 'product_combination', 'attributes', 'product_lists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->can('access-product-combination')) {
            abort(401);
        }
        $product = Product::findorfail($request->product_id);
        $combination = $product->combinations()->create(['product_id' => $request->product_id, 'name' => $request->name]);
        $combination_id = $combination->id;

        $combination_attributes = explode(',', $request->product_combination);

        foreach ($combination_attributes as $attribute) {
            $combination = ProductCombination::findorfail($combination_id);
            $combination->attributeValues()->create(['attribute_value_id' => $attribute, 'product_combination_id' => $combination_id]);

        }
        flash('Product Combination added successfully', 'success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
