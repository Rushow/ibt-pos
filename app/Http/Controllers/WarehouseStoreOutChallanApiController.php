<?php

namespace App\Http\Controllers;

use App\Task;
use App\TaskType;
use Illuminate\Http\Request;
use App\Company;
use App\ProductCombination;
use App\WarehouseStoreOutChallan;
use App\WarehouseStoreOutChallanProduct;
use App\WarehouseStoreOutChallanProductCombination;

//use Illuminate\Support\Facades\Auth;
//use Psy\Util\Json;
use Carbon\Carbon;
use DB;

class WarehouseStoreOutChallanApiController extends Controller
{
    public function saveWarehouseStoreOutChallan(Request $request)
    {
        $product_challan_infos = json_decode($request->get('product_challan_infos'));
        if ($product_challan_infos) {
            $challan_data['company_id'] = $product_challan_infos[0]->company_id;
            $challan_data['warehouse_id'] = $product_challan_infos[0]->warehouse_id;
            $challan_data['outlet_id'] = $product_challan_infos[0]->outlet_id;
            $challan_data['status'] = 'draft';
            $challan_data['warehouse_store_out_type_id'] = 1;
            $challan_data['challan_no'] = 1;
            $challan_data['request_date'] = $product_challan_infos[0]->request_date;
            $challan_data['user_id'] = $product_challan_infos[0]->user_id;
            $warehouse_store_out_challan = Company::find($challan_data['company_id'])->WarehouseStoreOutChallans()->create($challan_data);
            //print_r($warehouse_store_out_challan);die();

            $now = Carbon::now();
            $challan_no_data['challan_no'] = $now->year . $warehouse_store_out_challan->id;
            $warehouse_store_out_challan->update($challan_no_data);

            foreach ($product_challan_infos as $product_challan_info) {

                $challan_product_data['outlet_id'] = $product_challan_info->outlet_id;
                $challan_product_data['product_id'] = $product_challan_info->product_id;
                $warehouse_store_out_challan_product = $warehouse_store_out_challan->challanoutProducts()->create($challan_product_data);

                foreach ($product_challan_info->combination as $combination) {
                    if ($combination->quantity) {
                        $product_combination_data['outlet_id'] = $product_challan_info->outlet_id;
                        $product_combination_data['product_combination_id'] = $combination->id;
                        $product_combination_data['quantity'] = $combination->quantity;
                        $warehouse_store_out_challan_product->storeOutCombinations()->create($product_combination_data);
                    }
                }
            }
            if ($warehouse_store_out_challan_product) {
                $return_array['success'] = 'Challan Created successfully';
            } else {
                $return_array['error'] = 'Error: Challan Creation Error';
            }
        }
        return $return_array;
    }

    public function updateWarehouseStoreOutChallan(Request $request)
    {
        $product_challan_infos = json_decode($request->get('product_challan_infos'));

        if ($product_challan_infos) {
            $warehouse_store_out_challan = WarehouseStoreOutChallan::find($request->challan_id);
            $warehouse_store_out_challan->update(['warehouse_id' => $product_challan_infos[0]->warehouse_id, 'request_date' => $product_challan_infos[0]->request_date]);

            foreach ($product_challan_infos as $product_challan_info) {
                $warehouse_store_out_challan_product = WarehouseStoreOutChallanProduct::updateOrCreate(
                    ['warehouse_store_out_challan_id' => $request->challan_id, 'product_id' => $product_challan_info->product_id],
                    ['outlet_id' => $product_challan_info->outlet_id]
                );
                foreach ($product_challan_info->combination as $combination) {
                    if ($combination->quantity) {
                        $warehouse_store_out_challan_product_combination = WarehouseStoreOutChallanProductCombination::updateOrCreate(
                            ['warehouse_store_out_challan_product_id' => $warehouse_store_out_challan_product->id, 'product_combination_id' => $combination->id, 'outlet_id' => $product_challan_info->outlet_id],
                            ['quantity' => $combination->quantity]

                        );
                    }
                }
            }
            if ($warehouse_store_out_challan_product_combination) {
                $return_array['success'] = 'Challan Updated successfully';
            } else {
                $return_array['error'] = 'Error: Challan Update Error';
            }
        }
        return $return_array;
    }

    public function getChallanOutData(Request $request)
    {
        return WarehouseStoreOutChallan::where('id', $request->challan_id)->with(
            ['challanoutProducts' => function ($query) {
                $query->with(['product', 'storeOutCombinations' => function ($query) {
                    $query->with(['combination' => function ($query) {
                        $query->with(['combinationAttributeValues' => function ($query) {
                            $query->with(['attributeValue' => function ($query) {
                                $query->with('attribute');
                            }]);
                        }]);
                    }]);
                }]);
            }])->first();

    }

    public function makefinal($id)
    {
        $draftedWarehouseOutProductCombinations = $this->getWarehouseOutProductCombinations($id);
        $availabilityCheck = $this->checkIfQuantityAvailableAtWarehouse($draftedWarehouseOutProductCombinations);
        if ($availabilityCheck === 'success') {
            try {
                DB::beginTransaction();
                $processedArrayForTask = array(
                    'id' => $id,
                    'task' => $this->findTaskType('Challan'),
                    'product_combinations' => $draftedWarehouseOutProductCombinations
                );
                $this->makeTasks($processedArrayForTask);
                WarehouseStoreOutChallan::find($id)->update(array("status" => "final"));
                DB::commit();
                return json_encode(['message' => $availabilityCheck]);
            } catch (\Exception $ex) {
                DB::rollBack();
                return json_encode(['message' => $ex]);
            }

        } else {
            return json_encode(['message' => $availabilityCheck]);
        }
    }

    public function makeTasks($arrayForTask)
    {
        foreach ($arrayForTask['product_combinations'] as $combination) {
            Task::insert([
                'name' => $arrayForTask['task']->task_name,
                'task_type_id' => $arrayForTask['task']->id,
                'outlet_id' => $combination->outlet_id,
                'warehouse_store_out_challan_id' => $arrayForTask['id'],
                'product_id' => $this->getProductDetail($combination->id)->product->id,
                'status' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }

    public function getWarehouseOutProductCombinations($id)
    {
        $productCombinations = WarehouseStoreOutChallan::select('warehouse_store_out_challan_product_combinations.product_combination_id as id',
            'warehouse_store_out_challan_product_combinations.quantity', 'warehouse_store_out_challans.outlet_id')
            ->join('warehouse_store_out_challan_products', 'warehouse_store_out_challans.id', '=',
                'warehouse_store_out_challan_products.warehouse_store_out_challan_id')
            ->join('warehouse_store_out_challan_product_combinations', 'warehouse_store_out_challan_products.id', '=',
                'warehouse_store_out_challan_product_combinations.warehouse_store_out_challan_product_id')
            ->where('warehouse_store_out_challans.id', '=', $id)
            ->get();

        return $productCombinations;
    }

    public function checkIfQuantityAvailableAtWarehouse($warehouseProductCombinations)
    {
        foreach ($warehouseProductCombinations as $combination) {
            $query = $this->buildQuantityCheckQuery($combination);

            $availableInventory = DB::select($query);

            foreach ($availableInventory as $product) {
                if ($combination->quantity > $product->qty) {
                    $productInfo = $this->getProductDetail($combination->id);
                    return $returnArray['message'] = $productInfo->product->name . ", (" . $productInfo->name . ") is not available for quantity " . $combination->quantity;
                }
            }
        }
        return $returnArray['message'] = 'success';
    }

    public function buildQuantityCheckQuery($combination)
    {
        $query = 'SELECT oi.*,oi.quantity as q1,oo.quantity as q2, (oi.quantity - IFNULL( oo.quantity, 0 ))qty
                        FROM (
                            SELECT wicpc.product_combination_id, IFNULL( sum( wicpc.quantity ) , 0 ) quantity
                            FROM warehouse_store_in_challan_product_combinations wicpc, warehouse_store_in_challan_products wicp, warehouse_store_in_challans wic
                            WHERE wic.id = wicp.warehouse_store_in_challan_id
                            AND wicp.id = wicpc.warehouse_store_in_challan_product_id

                            GROUP BY wicpc.product_combination_id
                        )oi
                        LEFT JOIN (
                            SELECT wocpc.product_combination_id, IFNULL( sum( wocpc.quantity ) - ' . $combination->quantity . ' , 0 ) quantity
                            FROM warehouse_store_out_challan_product_combinations wocpc, warehouse_store_out_challan_products wocp, warehouse_store_out_challans woc
                            WHERE woc.id = wocp.warehouse_store_out_challan_id
                            AND wocp.id = wocpc.warehouse_store_out_challan_product_id

                            GROUP BY wocpc.product_combination_id
                        )oo ON oi.product_combination_id = oo.product_combination_id
                        WHERE oi.product_combination_id = ' . $combination->id . '
                        GROUP BY product_combination_id';

        return $query;
    }

    public function getProductDetail($id)
    {
        $productInfo = ProductCombination::with('product')
            ->where('id', '=', $id)
            ->first();

        return $productInfo;
    }

    public function findTaskType($name)
    {
        return TaskType::where('task_name', '=', $name)
            ->first();
    }
}
