<?php

namespace App\Http\Controllers;

use App\OutletIn;
use App\OutletOut;
use Illuminate\Http\Request;
use App\OutLet;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;

class OutletInventoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->can('access-warehouse')) {
            abort(401);
        }

        if (request('to_date')) {
            $to_date = Carbon::parse(request('to_date'));
        } else {
            $to_date = new Carbon();
        }
        $outlet_id = 0;
        if ($outlet_id) {
            $outlets = OutLet::where('id', $outlet_id)->orderBy('name')->pluck('name', 'id');
        } else {
            $outlets = OutLet::where('company_id', Auth::user()->company_id)->orderBy('name')->pluck('name', 'id');
        }
        //inventory
        $query = "SELECT outlet_ins.product_combination_id,outlet_ins.outletinsQuantity- outlet_outs.outletoutsQuantity as remaining FROM ( SELECT product_combination_id, SUM(quantity) as outletinsQuantity FROM outlet_ins Group BY product_combination_id) as outlet_ins LEFT OUTER JOIN ( SELECT product_combination_id, SUM(quantity) as outletoutsQuantity FROM outlet_outs Group BY product_combination_id) as outlet_outs ON outlet_ins.product_combination_id=outlet_outs.product_combination_id";
        $inventory = DB::select($query);

        //dd($inventory);

        return view('outlet_inventory.index', compact('to_date', 'outlet_id', 'outlets', 'inventory'));

    }


}
