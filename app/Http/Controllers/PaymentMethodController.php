<?php

namespace App\Http\Controllers;

use App\PaymentMethod;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Requests\PaymentMethodRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaymentMethodController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->can('access-global-payment-method-settings')) {
            abort(401);
        }
        $payment_methods = PaymentMethod::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
        return view('payment_method.index', compact('payment_methods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->can('access-global-payment-method-settings')) {
            abort(401);
        }
        return view('payment_method.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaymentMethodRequest $request)
    {
        if (!Auth::user()->can('access-global-payment-method-settings')) {
            abort(401);
        }
        $companies = Company::find(Auth::user()->company_id);
        $companies->paymentMethods()->create($request->all());
        flash('Payment method added successfully', 'success');
        return redirect('payment_method');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->can('access-global-payment-method-settings')) {
            abort(401);
        }
        $payment_methods = PaymentMethod::find($id);
        return view('payment_method.edit', compact('payment_methods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PaymentMethodRequest $request, $id)
    {
        if (!Auth::user()->can('access-global-payment-method-settings')) {
            abort(401);
        }
        $payment_methods = PaymentMethod::find($id);
        $payment_methods->update($request->all());
        flash('Payment method updated successfully', 'success');
        return redirect('payment_method');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!Auth::user()->can('access-global-payment-method-settings')) {
            abort(401);
        }
        if (!empty($request->payment_method_ids)) {
            PaymentMethod::destroy($request->payment_method_ids);
            flash('Payment method deleted successfully', 'success');
        }
        return redirect('payment_method');
    }
}
