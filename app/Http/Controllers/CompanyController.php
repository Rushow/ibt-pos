<?php

namespace App\Http\Controllers;

use App\User;
use App\Company;
use App\Country;
use App\BusinessCategory;
use App\Currency;
use App\TimeZone;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->can('access-company-info')) {
            abort(401);
        }
        $company = Company::findorfail($id);
        $countries = Country::orderBy('name', 'ASC')->pluck('name', 'id');
        $currencies = Currency::orderBy('id', 'ASC')->pluck('name', 'id');
        $time_zones = TimeZone::orderBy('id', 'ASC')->pluck('location', 'id');
        $business_categories = BusinessCategory::orderBy('id', 'Desc')->pluck('name', 'id');
        return view('company.edit', compact('company', 'countries', 'business_categories', 'currencies', 'time_zones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, $id)
    {
        if (!Auth::user()->can('access-company-info')) {
            abort(401);
        }
        $companies = Company::find($id);
        $companies->update($request->all());
        flash('Company updated successfully', 'success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
