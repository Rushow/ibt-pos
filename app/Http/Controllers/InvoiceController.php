<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->can('access-discount-module')) {
            abort(401);
        }
        $data = array();
        $data['invoices'] = Invoice::get();//dd( $data['discounts']);
        return view('invoice.index', $data);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->can('access-discount-module')) {
            abort(401);
        }
        Invoice::destroy($id);
        flash('Discount deleted successfully', 'success');

        return redirect('discount');
    }
}
