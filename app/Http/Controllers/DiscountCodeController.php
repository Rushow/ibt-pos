<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Discount;
use App\DiscountCodeType;
use App\DiscountCode;
use Illuminate\Support\Facades\Auth;

class DiscountCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->can('access-discount-module')) {
            abort(401);
        }
        $data = array();
        $data['discount'] = Discount::pluck('discount_name', 'id');
        $data['discountcodes'] = DiscountCode::get();
        return view('discount_code.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->can('access-discount-module')) {
            abort(401);
        }
        $data = array();
        $data['discount'] = Discount::pluck('discount_name', 'id');
        $data['discount_code_types'] = DiscountCodeType::pluck('type_name', 'id');


        return view('discount_code.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->can('access-discount-module')) {
            abort(401);
        }
        $discount_code_data = $request->all();
        if ($request->get("active") == 0) {
            $discount_code_data["active"] = 1;
        } else {
            $discount_code_data["active"] = 0;
        }
        $discountcode = new DiscountCode();
        $discountcode->create($discount_code_data);


        flash('Code added successfully', 'success');
        return redirect('discountcode');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(DiscountCode $discountcode)
    {
        $data = array();
        $data['discount'] = Discount::pluck('discount_name', 'id');
        $data['discount_code_types'] = DiscountCodeType::pluck('type_name', 'id');;

        return view('discount_code.edit', $data, compact('discountcode', $discountcode));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->can('access-discount-module')) {
            abort(401);
        }

        $discount_code_data = $request->all();
        if ($request->input("active", 1) == 0) {
            $discount_code_data["active"] = 1;
        } else {
            $discount_code_data["active"] = 0;
        }
        DiscountCode::find($id)->update($discount_code_data);

        flash('Code updated successfully', 'success');
        return redirect('discountcode');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->can('access-discount-module')) {
            abort(401);
        }
        DiscountCode::destroy($id);
        flash('Code deleted successfully', 'success');

        return redirect('discountcode');
    }

    public function remove_discount_code(Request $request)
    {

        if (!Auth::user()->can('access-discount-module')) {
            abort(401);
        }
        if (!empty($request->discountcode_ids)) {
            DiscountCode::destroy($request->discountcode_ids);
            flash('Discount code deleted successfully', 'success');
        }
        //return redirect('discountcode');
    }
}
