<?php

namespace App\Http\Controllers;

use App\Category;
use App\DiscountDay;
use App\OutLet;
use Illuminate\Http\Request;
use App\Discount;
use App\DiscountType;
use App\DiscountCodeType;
use App\DiscountTypeConfigurationOutlet;
use App\DiscountTypeConfigurationCategory;
use App\DiscountTypeConfigurationCode;
use App\DiscountAmountType;
use Illuminate\Support\Facades\Auth;
use DB;


class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->can('access-discount-module')) {
            abort(401);
        }
        $data = array();
        $data['discounts'] = Discount::get();//dd( $data['discounts']);
        return view('discount.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->can('access-discount-module')) {
            abort(401);
        }
        $data = array();
        $data['amount_types'] = DiscountAmountType::pluck('type_name', 'id');
        $data['discount_types'] = DiscountType::pluck('discount_type_name', 'id');
        $data['discount_code_types'] = DiscountCodeType::pluck('type_name', 'id');
        $data["day_lists"] = DiscountDay::pluck('day_name', 'id');

        $data['outlets'] = array('0' => 'All Outlet') + OutLet::pluck('name', 'id')->toArray();
        $data['categories'] = array('0' => 'All Categories') + Category::pluck('name', 'id')->toArray();

        return view('discount.create', $data)->with('discount', null);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (!Auth::user()->can('access-discount-module')) {
            abort(401);
        }
        $discount_data = $request->all();
        if ($request->input("active", 1) == 0) {
            $discount_data["active"] = 1;
        } else {
            $discount_data["active"] = 0;
        }
        $discount = new Discount();
        $records = $discount->create($discount_data);

        $category_data = array();
        $category_ids = $request->get('categories');


        foreach ($category_ids as $cat) {
            $category_data['category_id'] = $cat;
            $records->DiscountTypeConfigurationCategory()->create($category_data);
        }
        //outlet
        $outlet_data = array();
        $outlet_ids = $request->get('outlets');
        foreach ($outlet_ids as $out) {
            $outlet_data['outlet_id'] = $out;
            $records->DiscountTypeConfigurationOutlet()->create($outlet_data);

        }
        //discount day
        $day_data = array();
        $day_ids = $request->get("discount_day_id");
        foreach ($day_ids as $day) {
            $day_data["day_name"] = DiscountDay::find($day)->day_name;
            $day_data["discount_day_id"] = $day;
            $records->DiscountTypeConfigurationDay()->create($day_data);
        }


        flash('Discount added successfully', 'success');
        return redirect('discount');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Discount $discount)
    {
        $data = array();
        $data['amount_types'] = DiscountAmountType::pluck('type_name', 'id');
        $data['discount_types'] = DiscountType::pluck('discount_type_name', 'id');
        $data['discount_code_types'] = DiscountCodeType::pluck('type_name', 'id');
        $data['outlets'] = array('0' => 'All Outlet') + OutLet::pluck('name', 'id')->toArray();
        $data['categories'] = array('0' => 'All Categories') + Category::pluck('name', 'id')->toArray();

        $data["day_lists"] = DiscountDay::pluck('day_name', 'id');

//        print_r($discount->DiscountTypeConfigurationDay->pluck('day_name','discount_day_id'));
//        dd($data["day_lists"]);
        return view('discount.edit', compact('discount', $discount), $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $discount)
    {
        if (!Auth::user()->can('access-discount-module')) {
            abort(401);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        $discount_data = $request->all();

        if ($request->input("active", 1) == 0) {
            $discount_data["active"] = 1;
        } else {
            $discount_data["active"] = 0;
        }
        Discount::find($discount->id)->update($discount_data);

        $category_data = array();
        $category_ids = $request->get('categories');
        Discount::find($discount->id)->DiscountTypeConfigurationCategory()->delete();
        foreach ($category_ids as $cat) {
            $category_data['category_id'] = $cat;
            Discount::find($discount->id)->DiscountTypeConfigurationCategory()->create($category_data);

        }
        //outlet
        $outlet_data = array();
        $outlet_ids = $request->get('outlets');
        Discount::find($discount->id)->DiscountTypeConfigurationOutlet()->delete();
        foreach ($outlet_ids as $out) {
            $outlet_data['outlet_id'] = $out;
            Discount::find($discount->id)->DiscountTypeConfigurationOutlet()->create($outlet_data);

        }

        //discount day
        $day_data = array();
        $day_ids = $request->get("discount_day_id");
        Discount::find($discount->id)->DiscountTypeConfigurationDay()->delete();
        foreach ($day_ids as $day) {
            if($day !== null) {
                $day_data["day_name"] = DiscountDay::find($day)->day_name;
                $day_data["discount_day_id"] = $day;
                Discount::find($discount->id)->DiscountTypeConfigurationDay()->create($day_data);
            }
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        flash('Discount edited successfully', 'success');
        return redirect('discount/'.$discount->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Discount $discount)
    {
        if (!Auth::user()->can('access-discount-module')) {
            abort(401);
        }
        Discount::destroy($discount->id);
        flash('Discount deleted successfully', 'success');

        return redirect('discount');
    }
}
