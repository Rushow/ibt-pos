<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\UserDetailRequest;
use App\Http\Requests\UserRequest;
use App\OutLet;
use App\User;
use App\UserDetail;
use App\UserTitle;
use App\RoleUser;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->can('create-view-users')) {
            abort(401);
        }
        $users = User::where('company_id', Auth::user()->company_id)->orderBy('id', 'ASC')->get();
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->can('create-view-users')) {
            abort(401);
        }
        $role_lists = Role::where('company_id', Auth::user()->company_id)->orderBy('id', 'ASC')->pluck('name', 'id');
        $out_let_lists = OutLet::where('company_id', Auth::user()->company_id)->orderBy('id', 'ASC')->pluck('name', 'id');
        return view('user.create', compact('role_lists', 'out_let_lists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if (!Auth::user()->can('create-view-users')) {
            abort(401);
        }
        $user_data = $request->all();
        $user_data['password'] = bcrypt($request->password);
        $companies = Company::find(Auth::user()->company_id);
        $user = $companies->users()->create($user_data);
        $role = Role::find($request->role_id);
        $role->users()->attach($user->id);
        $user->out_lets()->sync($request->out_let_ids);
        flash('User added successfully', 'success');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if (!Auth::user()->can('edit-update-user')) {
            abort(401);
        }
        $user_detail = $user->userDetail;
        $role_lists = Role::where('company_id', Auth::user()->company_id)->orderBy('id', 'ASC')->pluck('name', 'id');
        $title_lists = UserTitle::where('company_id', Auth::user()->company_id)->orderBy('id', 'ASC')->pluck('title', 'id');
        return view('user.edit', compact('user', 'user_detail', 'role_lists', 'title_lists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, UserDetailRequest $detail_request, User $user)
    {
        if (!Auth::user()->can('edit-update-user')) {
            abort(401);
        }
        $user->update($request->all());
        if ($user->userDetail) {
            $user_detail = $user->userDetail;
            $user_detail->update($request->all());
        } else {
            $user->userDetail()->create($request->all());
        }
        if ($user->roles) {
            foreach ($user->roles as $user_role) {
                $user_role = Role::find($user_role->id);
                $user->roles()->detach($user_role->id);
            }
        }
        $user->roles()->attach($request->role_id);
        flash('User updated successfully', 'success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (!empty($user)) {
            if ($user->roles) {
                foreach ($user->roles as $user_role) {
                    $user_role = Role::find($user_role->id);
                }
            }
        }

        if (!Auth::user()->can('delete-user') || $user_role->slug == 'administrator') {
            abort(401);
        }
        if (!empty($user_role)) {
            $user->roles()->detach($user_role->id);
        }
        if ($user->userDetail) {
            $user->userDetail()->delete();
        }
        User::destroy($user->id);
        flash('User deleted successfully', 'success');
        return redirect('user');
    }

    /**
     * Remove specified resources from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function removeAttributes(Request $request)
    {
        if (!Auth::user()->can('access-product-attributes')) {
            abort(401);
        }
        if (!empty($request->attribute_ids)) {
            Attribute::destroy($request->attribute_ids);
            flash('Attribute deleted successfully', 'success');
        }
        return redirect('attribute');
    }
}
