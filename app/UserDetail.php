<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDetail extends Model
{
    use SoftDeletes;
    protected $table = 'user_details';
    protected $fillable = ['user_id', 'company_id', 'country_id', 'user_title_id', 'photo', 'first_name',
        'last_name', 'date_of_birth', 'user_code', 'cell_phone', 'work_phone', 'address', 'address2',
        'city', 'zip'];
    protected $dates = ['deleted_at'];

}
