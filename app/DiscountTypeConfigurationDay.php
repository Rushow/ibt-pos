<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountTypeConfigurationDay extends Model
{
    use SoftDeletes;
    protected $table = 'discount_type_configuration_days';
    protected $fillable = ['discount_id', 'discount_day_id', 'day_name'];
    protected $dates = ['deleted_at'];
}
