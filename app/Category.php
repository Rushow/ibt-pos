<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $table = 'categories';
    protected $fillable = ['company_id', 'parent_id', 'name', 'is_shop_default', 'level_depth', 'nleft', 'nright', 'active', 'position', 'is_root_category'];
    protected $dates = ['deleted_at'];

    public function childs()
    {
        return $this->hasMany('App\Category', 'parent_id', 'id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
