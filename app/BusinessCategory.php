<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessCategory extends Model
{
    use SoftDeletes;
    protected $table = "business_categories";
    protected $fillable = ['name'];
    protected $dates = ['deleted_at'];
}
