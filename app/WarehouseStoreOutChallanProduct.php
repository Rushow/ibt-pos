<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseStoreOutChallanProduct extends Model
{
    use SoftDeletes;
    protected $table = 'warehouse_store_out_challan_products';
    protected $fillable = ['warehouse_store_out_challan_id', 'product_id', 'outlet_id'];
    protected $dates = ['deleted_at'];

    public function storeOutCombinations()
    {
        return $this->hasMany('App\WarehouseStoreOutChallanProductCombination', 'warehouse_store_out_challan_product_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
