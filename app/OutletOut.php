<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutletOut extends Model
{
    use SoftDeletes;
    protected $fillable = ['outlet_id', 'product_id', 'product_combination_id', 'quantity', 'invoice_id'];
    protected $dates = ['deleted_at'];

}
