<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductDiscount extends Model
{
    use SoftDeletes;
    protected $table = 'product_discounts';
    protected $fillable = ['product_id', 'discount_amount', 'discount_type', 'active_date', 'expire_date', 'status'];
    protected $dates = ['deleted_at'];

    public function DiscountAmountType()
    {
        return $this->hasOne('App\DiscountAmountType', 'id', 'discount_type');
    }

}
