<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;

    protected $table = 'invoices';
    protected $fillable = ['outlet_id', 'customer_id', 'sales_by', 'invoice_no', 'product_price_exclusive_vat', 'vat_amount', 'product_discount_amount', 'total_amount', 'card_received',
        'cash_received', 'total_received'];
    protected $dates = ['deleted_at'];

    public function InvoiceProduct()
    {
        return $this->hasMany('App\InvoiceProduct', 'invoice_id');
    }

    public function Outlet()
    {
        return $this->belongsTo('App\Outlet', 'outlet_id', "id");
    }

    public function User()
    {
        return $this->belongsTo('App\User', "sales_by", "id");
    }

}
