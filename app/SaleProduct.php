<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaleProduct extends Model
{
    use SoftDeletes;
    protected $table = 'sale_products';
    protected $fillable = ['outlet_product_id', 'sale_id', 'product_discount_id', 'quantity', 'unit_price',
        'vat_percent', 'product_discount_amount', 'product_discount_percent', 'coupon_discount_amount', 'coupon_discount_percent'];
    protected $dates = ['deleted_at'];

}
