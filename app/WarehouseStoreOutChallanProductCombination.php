<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseStoreOutChallanProductCombination extends Model
{
    use SoftDeletes;
    protected $table = 'warehouse_store_out_challan_product_combinations';
    protected $fillable = ['warehouse_store_out_challan_product_id', 'outlet_id', 'product_combination_id', 'quantity'];
    protected $dates = ['deleted_at'];

    public function combination()
    {
        return $this->belongsTo('App\ProductCombination', 'product_combination_id');
    }
}
