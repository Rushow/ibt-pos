<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountTypeConfigurationProduct extends Model
{
    use SoftDeletes;
    protected $table = 'discount_type_configuration_products';
    protected $fillable = ['discount_id', 'product_id'];
    protected $dates = ['deleted_at'];
}
