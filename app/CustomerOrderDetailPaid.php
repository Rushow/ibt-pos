<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerOrderDetailPaid extends Model
{
    use SoftDeletes;
    protected $table = 'customer_order_detail_paids';
    protected $fillable = ['paid_amount', 'paid_date'];
    protected $dates = ['deleted_at'];
}
