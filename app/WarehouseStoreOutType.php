<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseStoreOutType extends Model
{
    use SoftDeletes;
    protected $table = 'warehouse_store_out_types';
    protected $fillable = ['company_id', 'name'];
    protected $dates = ['deleted_at'];

}
