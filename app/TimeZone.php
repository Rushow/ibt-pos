<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TimeZone extends Model
{
    use SoftDeletes;
    protected $table = "time_zones";
    protected $fillable = ['location', 'gmt'];
    protected $dates = ['deleted_at'];

}
