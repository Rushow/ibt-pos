<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountCode extends Model
{
    use SoftDeletes;
    protected $table = 'discount_codes';
    protected $fillable = ['discount_id', 'code', 'discount_code_type_id', 'active'];
    protected $dates = ['deleted_at'];

    public function DiscountCodeType()
    {
        return $this->belongsTo('App\DiscountCodeType', 'discount_code_type_id');
    }

    public function Discount()
    {

        return $this->belongsTo('App\Discount', 'discount_id');
    }
}
