<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    protected $table = "companies";
    protected $fillable = [
        'country_id', 'time_zone_id', 'business_category_id', 'currency_id', 'name', 'phone', 'email', 'address', 'city',
        'zip', 'receipt_address', 'receipt_text', 'signature_line_text', 'receipt_logo'
    ];

    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function roles()
    {
        return $this->hasMany('App\Role');
    }

    public function vats()
    {
        return $this->hasMany('App\Vat');
    }

    public function paymentMethods()
    {
        return $this->hasMany('App\PaymentMethod');
    }

    public function outlets()
    {
        return $this->hasMany('App\OutLet');
    }

    public function userTitles()
    {
        return $this->hasMany('App\UserTitle');
    }

    public function categories()
    {
        return $this->hasMany('App\Category');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function suppliers()
    {
        return $this->hasMany('App\Supplier');
    }

    public function attributes()
    {
        return $this->hasMany('App\Attribute');
    }

    public function warehouses()
    {
        return $this->hasMany('App\Warehouse');
    }

    public function warehouseStoreInChallans()
    {
        return $this->hasMany('App\WarehouseStoreInChallan');
    }

    public function warehouseStoreOutChallans()
    {
        return $this->hasMany('App\WarehouseStoreOutChallan');
    }
}
