<?php

namespace App\DataTables;

use Yajra\DataTables\Services\DataTable;
use App\Invoice;
use DB;
use Carbon\Carbon;

class DaywiseSalesDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query);
//            ->addColumn('action', 'daywisesalesdatatable.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $startDate = !empty($this->request()->get('start_date')) ? $this->request()->get('start_date') : Carbon::today()->subDays(7)->toDateString();
        $endDate = !empty($this->request()->get('end_date')) ? $this->request()->get('end_date') : Carbon::today()->toDateString();
        $query = Invoice::select(DB::raw('IFNULL(SUM(`invoices`.`product_price_exclusive_vat`), 0) AS product_price'),
            DB::raw('IFNULL(SUM(`invoices`.`vat_amount`), 0) AS vat'),
            DB::raw('IFNULL(SUM(`invoices`.`product_discount_amount`), 0) AS discount'),
            DB::raw('IFNULL(SUM(`invoices`.`card_received`), 0) AS card'),
            DB::raw('IFNULL(SUM(`invoices`.`cash_received`), 0) AS cash'),
            DB::raw('IFNULL(SUM(`invoices`.`total_received`), 0) AS total_price'),
            DB::raw('`calendar`.`datefield` AS date'))
            ->join('calendar', DB::raw('DATE(`invoices`.`created_at`)'), '=', 'calendar.datefield', 'right');

        $query->whereBetween('calendar.datefield', [$startDate, $endDate])
            ->groupBy('calendar.datefield');
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('', null, request()->only(['start_date', 'end_date']))
//                    ->addAction(['width' => '80px'])
                    ->parameters([
                        'dom'          => 'ltiprB',
                        'pageLength'   => 10
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'date' => ['name' => 'date'],
            'product_price' => ['name' => 'product_price'],
            'vat' => ['name' => 'vat'],
            'discount' => ['name' => 'discount'],
            'card' => ['name' => 'card'],
            'cash' => ['name' => 'cash'],
            'total_price' => ['name' => 'total_price']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'DaywiseSales_' . date('YmdHis');
    }
}
