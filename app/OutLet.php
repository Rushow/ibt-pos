<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutLet extends Model
{
    use SoftDeletes;
    protected $table = "out_lets";
    protected $fillable = ['country_id', 'company_id', 'name', 'outlet_type_id',
        'space', 'duration', 'capacity', 'display_products', 'advance_amount',
        'monthly_rent', 'trade_license_no', 'trade_license_photo', 'start_date',
        'end_date', 'location', 'phone', 'address', 'city', 'zip', 'responsible_person', 'notes'];
    protected $dates = ['deleted_at'];

    public function type()
    {
        return $this->hasOne('App\OutLetType');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}