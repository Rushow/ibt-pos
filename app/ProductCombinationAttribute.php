<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCombinationAttribute extends Model
{
    protected $table = 'product_combination_attributes';
    protected $fillable = ['attribute_value_id', 'product_combination_id'];
    protected $dates = ['deleted_at'];

    public function combination()
    {
        return $this->belongsTo('App\ProductCombination');
    }

    public function attributeValue()
    {
        return $this->belongsTo('App\AttributeValue', 'attribute_value_id');
    }

    public function warehouseStoreInChallanProductCombination()
    {
        return $this->belongsTo('App\WarehouseStoreInChallanProductCombination');
    }
}
