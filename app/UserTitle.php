<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTitle extends Model
{
    use SoftDeletes;
    protected $table = 'user_titles';
    protected $fillable = ['title', 'company_id'];
    protected $dates = ['deleted_at'];
}
