<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductDiscountOutlet extends Model
{
    use SoftDeletes;
    protected $table = 'product_discount_outlets';
    protected $fillable = ['product_discount_id', 'outlet_id'];
    protected $dates = ['deleted_at'];

}
