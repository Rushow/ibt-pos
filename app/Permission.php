<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
    use SoftDeletes;
    protected $table = "permissions";
    protected $fillable = ['name', 'slug'];
    protected $dates = ['deleted_at'];

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

}
