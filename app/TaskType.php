<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskType extends Model
{
    public function TaskType()
    {

        return $this->hasMany('App\Task', 'id', 'task_type_id');

    }
}
