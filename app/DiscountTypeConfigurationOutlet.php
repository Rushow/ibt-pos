<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountTypeConfigurationOutlet extends Model
{
    use SoftDeletes;
    protected $table = 'discount_type_configuration_outlets';
    protected $fillable = ['discount_id', 'outlet_id'];
    protected $dates = ['deleted_at'];
}
