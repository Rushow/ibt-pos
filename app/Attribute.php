<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Attribute extends Model
{
    use SoftDeletes;
    protected $table = 'attributes';
    protected $fillable = ['name', 'type'];
    protected $dates = ['deleted_at'];

    public function attributeValues()
    {
        return $this->hasMany('App\AttributeValue');
    }
}
