<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseStoreInChallanProductCombination extends Model
{
    use SoftDeletes;
    protected $table = 'warehouse_store_in_challan_product_combinations';
    protected $fillable = ['warehouse_store_in_challan_product_id', 'product_combination_id', 'quantity'];
    protected $dates = ['deleted_at'];

    public function combination()
    {
        return $this->belongsTo('App\ProductCombination', 'product_combination_id');
    }

    public function combinationAttributeValues()
    {
        return $this->hasMany('App\ProductCombinationAttribute', 'product_combination_id');
    }
}
