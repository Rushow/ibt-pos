<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseStoreoutChallan extends Model
{
    use SoftDeletes;
    protected $table = 'warehouse_store_out_challans';
    protected $fillable = ['company_id', 'warehouse_id', 'user_id', 'outlet_id', 'warehouse_store_out_type_id', 'challan_no', 'request_date', 'status', 'deleted_at'];
    protected $dates = ['deleted_at'];

    public function challanoutProducts()
    {
        return $this->hasMany('App\WarehouseStoreOutChallanProduct', 'warehouse_store_out_challan_id');
    }

    public function challanOutProductsCombinations()
    {
        return $this->hasMany('App\WarehouseStoreOutChallanProductCombination');
    }

}
