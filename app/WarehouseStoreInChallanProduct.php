<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseStoreInChallanProduct extends Model
{
    use SoftDeletes;
    protected $table = 'warehouse_store_in_challan_products';
    protected $fillable = ['warehouse_store_in_challan_id', 'supplier_id', 'product_id'];
    protected $dates = ['deleted_at'];

    public function storeInCombinations()
    {
        return $this->hasMany('App\WarehouseStoreInChallanProductCombination');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
