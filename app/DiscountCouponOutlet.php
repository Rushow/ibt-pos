<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountCouponOutlet extends Model
{
    use SoftDeletes;
    protected $table = 'discount_coupon_outlets';
    protected $fillable = ['discount_coupon_id', 'outlet_id'];
    protected $dates = ['deleted_at'];

}
