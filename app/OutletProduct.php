<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutletProduct extends Model
{
    use SoftDeletes;
    protected $table = "outlet_products";
    protected $fillable = ['company_id', 'outlet_challanin_id', 'product_attribute_combination_id', 'product_id',
        'outlet_id', 'quantity', 'quantity_left'];
    protected $dates = ['deleted_at'];

}
