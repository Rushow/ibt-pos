<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $table = 'products';
    protected $fillable = ['company_id', 'name', 'item_code', 'bar_code', 'reference_code'];
    protected $dates = ['deleted_at'];

    public function combinations()
    {
        return $this->hasMany('App\ProductCombination');
    }

    public function prices()
    {
        return $this->hasMany('App\ProductPrice');
    }

    public function discount()
    {
        return $this->hasMany('App\ProductDiscount');
    }

    public function pricesActive()
    {
        return $this->hasMany('App\ProductPrice')->where("active", 1);
    }

    public function discountActive()
    {
        return $this->hasMany('App\ProductDiscount')->with('DiscountAmountType')->where("status", 1);
    }

    public function images()
    {
        return $this->hasMany('App\ProductImage');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
