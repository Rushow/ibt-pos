<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Warehouse extends Model
{
    use SoftDeletes;
    protected $table = 'warehouses';
    protected $fillable = ['company_id', 'name', 'description'];
    protected $dates = ['deleted_at'];

    public function challanIn()
    {
        return $this->hasMany('App\WarehouseStoreInChallan');
    }

    public function challanOut()
    {
        return $this->hasMany('App\WarehouseStoreOutChallan');
    }

    public function challanInProducts()
    {
        return $this->hasManyThrough('App\WarehouseStoreInChallanProduct', 'App\WarehouseStoreInChallan');
    }
}
