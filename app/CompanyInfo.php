<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyInfo extends Model
{
    use SoftDeletes;
    protected $table = "company_infos";
    protected $fillable = ['test', 'country_id', 'business_category_id', 'company_name', 'company_phone', 'company_email', 'company_address', 'company_city',
        'company_zip', 'company_receipt_address', 'company_receipt_text', 'company_signatur_line_text', 'company_receipt_logo'];
    protected $dates = ['deleted_at'];

    public function employees()
    {
        return $this->hasMany('App\Employee');
    }

}
