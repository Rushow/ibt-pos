<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discount extends Model
{
    use SoftDeletes;
    protected $table = 'discounts';
    protected $fillable = ['discount_name', 'discount', 'amount_type', 'start_date', 'end_date', 'discount_type_id', 'active'];
    protected $dates = ['deleted_at'];

    public function DiscountType()
    {
        return $this->hasOne('App\DiscountType', 'id', 'discount_type_id');
    }

    public function DiscountAmountType()
    {
        return $this->hasOne('App\DiscountAmountType', 'id', 'amount_type');
    }

    public function DiscountTypeConfigurationCategory()
    {

        return $this->hasMany('App\DiscountTypeConfigurationCategory', 'discount_id');
    }

    public function DiscountTypeConfigurationOutlet()
    {

        return $this->hasMany('App\DiscountTypeConfigurationOutlet', 'discount_id');
    }

    public function DiscountTypeConfigurationDay()
    {

        return $this->hasMany('App\DiscountTypeConfigurationDay', 'discount_id');
    }

    public function DiscountTypeConfigurationProduct()
    {

        return $this->hasMany('App\DiscountTypeConfigurationProduct', 'discount_id');
    }
}

