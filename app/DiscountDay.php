<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountDay extends Model
{
    use SoftDeletes;
    protected $table = 'discount_days';
    protected $fillable = ['day_name', 'discount_day_id'];
    protected $dates = ['deleted_at'];
}
