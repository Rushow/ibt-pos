<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountType extends Model
{
    public function DiscountType()
    {
        return $this->hasOne('App\Discount', 'discount_type_id');
    }

}
