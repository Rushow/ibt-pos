// Mix's webpack.mix.js
const { mix } = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public/js').version();


mix.copy(
    'node_modules/jquery.fancytree/dist/skin-win8/ui.fancytree.min.css','resources/assets/css/libs'
).copy(
    'node_modules/jquery.fancytree/dist/jquery.fancytree-all-deps.min.js','resources/assets/js/libs'
);


<!-- BEGIN VENDOR JS -->
mix.combine(
    [
        'resources/assets/plugins/pace/pace.min.js',
        'resources/assets/plugins/jquery/jquery-1.11.1.min.js',
        'resources/assets/plugins/modernizr.custom.js',
        'resources/assets/plugins/jquery-ui/jquery-ui.min.js',
        'resources/assets/plugins/boostrapv3/js/bootstrap.min.js',

        'resources/assets/plugins/jquery/jquery-easy.js',
        'resources/assets/plugins/jquery-unveil/jquery.unveil.min.js',
        'resources/assets/plugins/jquery-bez/jquery.bez.min.js',
        'resources/assets/plugins/jquery-ios-list/jquery.ioslist.min.js',

        'resources/assets/plugins/jquery-actual/jquery.actual.min.js',
        'resources/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js',
        'resources/assets/plugins/bootstrap-select2/select2.min.js',
        'resources/assets/plugins/classie/classie.js',
        'resources/assets/plugins/switchery/js/switchery.min.js',
        'resources/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
        'resources/pages/js/pages.min.js',

        'resources/assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js',
        'resources/assets/plugins/jquery-validation/js/jquery.validate.min.js',
        'resources/assets/js/scripts.js',

    ],  'public/theme/backend/assets/js/plugins.js'
).version();

<!-- END VENDOR JS -->

<!-- BEGIN Data-Tables JS -->
mix.combine(
    [

        // 'resources/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js',
        // 'resources/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js',
        // 'resources/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js',
        'resources/assets/plugins/jquery-datatable/DataTables/DataTables-1.10.16/js/jquery.dataTables.min.js',
        'resources/assets/plugins/jquery-datatable/DataTables/DataTables-1.10.16/js/dataTables.bootstrap.min.js',
        'resources/assets/plugins/jquery-datatable/DataTables/buttons/dataTables.buttons.min.js',

        'resources/assets/plugins/plugins/datatables-responsive/js/datatables.responsive.js',
        'resources/assets/plugins/plugins/datatables-responsive/js/lodash.min.js',

        'resources/assets/plugins/jquery-datatable/DataTables/buttons/buttons.bootstrap.min.js',
        'resources/assets/plugins/jquery-datatable/DataTables/buttons/jszip.min.js',
        'resources/assets/plugins/jquery-datatable/DataTables/buttons/pdfmake.min.js',
        'resources/assets/plugins/jquery-datatable/DataTables/buttons/vfs_fonts.js',
        'resources/assets/plugins/jquery-datatable/DataTables/buttons/buttons.html5.min.js',
        'resources/assets/plugins/jquery-datatable/DataTables/buttons/buttons.print.min.js',
        'resources/assets/plugins/jquery-datatable/DataTables/buttons/buttons.colVis.min.js',
        'resources/assets/js/datatableinit.js',
    ],  'public/theme/backend/assets/js/datatables.js'
).version();

<!-- BEGIN Data-Tables JS -->

<!-- BEGIN CORE TEMPLATE JS -->
// mix.combine(
//     [
//         'resources/pages/js/pages.calendar.js',
//         'resources/pages/js/pages.email.js',
//         'resources/pages/js/pages.js',
//         'resources/pages/js/pages.social.js',
//     ],  'public/theme/backend/assets/js/pages.js'
// ).version();
<!-- END CORE TEMPLATE JS -->

<!-- BEGIN PAGE LEVEL JS -->
mix.combine(
    [
        // 'resources/assets/js/dashboard.js',
        'resources/assets/js/form_layouts.js',
    ],  'public/theme/backend/assets/js/page-level.js'
).version();
<!-- END PAGE LEVEL JS -->

<!-- BEGIN Dyna-tree JS -->
mix.combine(
    [
        // 'resources/assets/plugins/jquery-dynatree/jquery.dynatree.min.js',
        'resources/assets/js/libs/jquery.fancytree-all-deps.min.js',
    ],  'public/theme/backend/assets/js/fancytree.js'
).version();
<!-- END Dyna-tree JS -->

<!-- BEGIN Thirt Party -->
mix.combine(
    [
        'resources/thirt-party/bootstrap-colorpicker/bootstrap-colorpicker.js',
    ],  'public/theme/backend/assets/js/thirt-party.js'
).version();
<!-- END Thirt Party -->

<!-- BEGIN Custom JS For This Project -->
mix.combine(
    [
        'resources/customs/js/common.js',
    ],  'public/theme/backend/assets/js/customs.js'
).version();
<!-- END Custom JS For This Project -->

<!-- BEGIN Multi-select JS -->
mix.combine(
    [
        'resources/customs/js/multiselect.js',
    ],  'public/theme/backend/assets/js/multiselect.js'
).version();
<!-- END Multi-select JS -->

<!-- BEGIN Tree-category JS For This Project -->
mix.combine(
    [
        'resources/customs/js/tree_category_script.js',
    ],  'public/theme/backend/assets/js/tree_category_script.js'
).version();

<!-- END Tree-category JS For This Project -->

<!-- BEGIN Plugins CSS -->
mix.combine(
    [
        'resources/assets/plugins/pace/pace-theme-flash.css',
        'resources/assets/plugins/boostrapv3/css/bootstrap.min.css',
        'resources/assets/plugins/font-awesome/css/font-awesome.css',
        'resources/assets/plugins/jquery-scrollbar/jquery.scrollbar.css',
        'resources/assets/plugins/bootstrap-select2/select2.css',
        'resources/assets/plugins/switchery/css/switchery.min.css',
        'resources/assets/plugins/nvd3/nv.d3.min.css',
        'resources/assets/plugins/mapplic/css/mapplic.css',
        'resources/assets/plugins/rickshaw/rickshaw.min.css',
        'resources/assets/plugins/bootstrap-datepicker/css/datepicker3.css',
        'resources/assets/plugins/jquery-metrojs/MetroJs.css',
        'resources/assets/plugins/bootstrap-tag/bootstrap-tagsinput.css',

        'resources/assets/plugins/jquery-datatable/media/css/jquery.dataTables.css',
        'resources/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css',
        'resources/assets/plugins/datatables-responsive/css/datatables.responsive.css',
    ],  'public/theme/backend/assets/css/plugins.css'
).version();
<!-- END Plugins CSS -->

<!-- BEGIN Data-Tables CSS -->
mix.combine(
    [
        'resources/assets/plugins/jquery-datatable/media/css/jquery.dataTables.css',
        'resources/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css',
        'resources/assets/plugins/datatables-responsive/css/datatables.responsive.css',
    ],  'public/theme/backend/assets/css/datatables.css'
).version();
<!-- END Data-Tables CSS -->


mix.combine(
    [
        'resources/pages/css/pages-icons.css',
        'resources/pages/css/pages.css',
    ],  'public/theme/backend/assets/css/pages.css'
).version();

<!-- BEGIN Custom CSS For This Project -->
mix.combine(
    [
        'resources/customs/css/common.css',
        'resources/customs/css/overwrite.css',
        'resources/customs/css/ibtpos.css',
    ],  'public/theme/backend/assets/css/customs.css'
).version();
<!-- END Custom CSS For This Project -->

<!-- BEGIN Dyna-tree CSS For This Project -->
mix.combine(
    [
        // 'resources/assets/plugins/jquery-dynatree/skin/ui.dynatree.css',
        'resources/assets/css/libs/ui.fancytree.min.css',
    ],  'public/theme/backend/assets/css/fancytree/fancytree.css'
).version();
<!-- END Dyna-tree CSS For This Project -->

<!-- BEGIN LTE-IE CSS For This Project -->
mix.combine(
    [
        'resources/pages/css/ie9.css',
    ],  'public/theme/backend/assets/css/lte-ie9.css'
).version();
<!-- END LTE-IE CSS For This Project -->

<!-- BEGIN LT-IE CSS For This Project -->
mix.combine(
    [
        'resources/assets/plugins/mapplic/css/mapplic-ie.css',
    ],  'public/theme/backend/assets/css/lt-ie9.css'
).version();
<!-- END LT-IE CSS For This Project -->
// mix.browserSync();


