<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::bind('attribute',function($id){
    return \App\Attribute::findorfail($id);
});
Route::bind('attribute_value',function($id){
    return \App\AttributeValue::findorfail($id);
});

Route::bind('product',function($id){
    return \App\Product::findorfail($id);
});
Route::bind('product_image',function($id){
    return \App\ProductImage::findorfail($id);
});

Route::bind('role',function($id){
    return \App\Role::findorfail($id);
});

Route::bind('user',function($id){
    return \App\User::findorfail($id);
});
Route::bind('warehouse',function($id){
    return \App\Warehouse::findorfail($id);
});
Route::bind('warehouse_store_in_challan',function($id){
    return \App\WarehouseStoreInChallan::findorfail($id);
});
Route::bind('warehouse_store_out_challan',function($id){
    return \App\WarehouseStoreOutChallan::findorfail($id);
});


Route::bind('discount',function($id){
    return \App\Discount::findorfail($id);
});
Route::bind('discount_codes',function($id){
    return \App\Discount::findorfail($id);
});

Route::bind('customer',function($id){
    return \App\Customer::findorfail($id);
});

Route::name('home')->get('home', function(){
    return redirect('/');
});
Route::name('home')->get('/', 'HomeController@index');
Route::name('frontend')->get('/frontend', 'FrontendController@index');
Route::name('frontend')->get('/frontend/login', 'FrontendController@login');
Route::name('frontend')->get('/frontend/dashboard', 'FrontendController@dashboard');

Route::middleware('auth')->group(function () {
    Route::name('dashboard')->get('dashboard', 'DashboardController@index');
});
Route::middleware('auth')->group(function () {
    Route::name('warehouse')->delete('warehouse/remove_warehouse', 'WarehouseController@removeWarehouse');
});
Route::middleware('auth')->group(function () {
    Route::name('attribute')->delete('attribute/remove_attributes', 'AttributeController@removeAttributes');
});
Route::middleware('auth')->group(function () {
    Route::name('attribute_value')->delete('attribute/{attribute}/attribute_value/remove_attribute_values', 'AttributeValueController@removeAttributeValues');
    Route::name('attribute_value')->get('attribute/{attribute}/attribute_value/get_attribute_details', 'AttributeValueController@getAttributeDetails');
    Route::name('attribute_value')->get('attribute/{attribute}/attribute_value/{attribute_value}/get_attribute_value_details', 'AttributeValueController@getAttributeValueDetails');
});
Route::middleware('auth')->group(function () {
    Route::name('product')->post('product/{{product}}/product_image/upload-photo', 'ProductController@uploadPhoto');
    Route::name('product')->delete('product/remove_products', 'ProductController@removeProducts');
    Route::name('product')->get('product/generate_barcode', 'ProductController@generateBarcode');

});
Route::middleware('auth')->group(function () {
    Route::name('product_price')->delete('product/{product}/product_price/remove_product_prices', 'ProductPriceController@removeProductPrices');
});
Route::middleware('auth')->group(function () {
    Route::name('product_price')->delete('role/remove_roles', 'RoleController@removeRoles');
});
//Route::middleware('auth')->group(function () {
//    Route::name('product_discount_price')->delete('product/{product}/product_discount_price/remove_product_discount_prices', 'ProductDiscountController@removeProductDiscount');
//});
Route::middleware('auth')->group(function () {
    Route::name('permission_role')->put('role/{role}/update_permission_role', 'PermissionRoleController@updatePermissionRole');
    Route::get('sales_report', 'ReportsController@salesReport');
    Route::get('daywise_sales_report', 'ReportsController@daywiseSalesReport');
    Route::get('store_daywise_cumulative_sales_report', 'ReportsController@storeDaywiseCumulativeSalesReport');
    Route::get('store_monthly_sales_report', 'ReportsController@storeMonthlySalesReport');
});

Route::get('get_sales_report_total', 'ReportsController@getSalesReportTotal');
Route::get('get_daywise_report_total', 'ReportsController@getDaywiseSalesReportTotal');

Route::name('discountcode')->delete('discountcode/remove_discount_code', 'DiscountCodeController@remove_discount_code');


Route::resource('user', 'UserController');
Route::resource('dashboard', 'DashboardController');
Route::resource('company', 'CompanyController');
Route::resource('vat', 'VatController');
Route::resource('payment_method', 'PaymentMethodController');
Route::resource('out_let', 'OutLetController');
Route::resource('user_title', 'UserTitleController');
Route::resource('category', 'CategoryController');
Route::resource('supplier', 'SupplierController');
Route::resource('attribute', 'AttributeController');
Route::resource('attribute.attribute_value', 'AttributeValueController');
Route::resource('product', 'ProductController');
Route::resource('product.product_image', 'ProductImageController');
Route::resource('product.product_price', 'ProductPriceController');
Route::resource('product.product_discount_price', 'ProductDiscountController');
Route::resource('product.product_combination', 'ProductCombinationController');
Route::resource('role', 'RoleController');
Route::resource('role.permission_role', 'PermissionRoleController');
Route::resource('warehouse_store_in_challan', 'WarehouseStoreInChallanController');
Route::resource('warehouse_store_out_challan', 'WarehouseStoreOutChallanController');
Route::resource('warehouse', 'WarehouseController');
Route::resource('customer', 'CustomerController');
Route::resource('discount', 'DiscountController');
Route::resource('discountcode', 'DiscountCodeController');

Route::get('check', function (){
    $combination_data = App\Product::where('id', 1 )->with(
        ['combinations' => function($query){
            $query->with([
                'combinationAttributeValues' => function($query){
                    $query->with(['attributeValue' => function($query){
                        $query->with('attribute');
                    }]);
                }
            ]);
        }
        ]
    )->first();
    dd( $combination_data) ;
});
Route::get('test', function(){
    $return_challan_data = [];
    $challan_info = App\WarehouseStoreInChallan::find(1);
    $return_challan_data = App\WarehouseStoreInChallan::find(1);
    $challan_info_return_data['challan_info'] = $return_challan_data;

    if( $challan_info ){
        if( $challan_info->challanProducts ) {
            $product_index = 0;
            foreach ($challan_info->challanProducts as $challan_product) {
                $combinations = [];
                $product[$product_index]['warehouse_store_in_challan_product_id'] = $challan_product->id;
                $product[$product_index]['warehouse_store_in_challan_id'] = $challan_product->warehouse_store_in_challan_id;
                $product[$product_index]['supplier_id'] = $challan_product->supplier_id;
                $product[$product_index]['product_id'] = $challan_product->product_id;
                $product[$product_index]['company_id'] = $challan_product->product->company_id;
                $product[$product_index]['name'] = $challan_product->product->name;
                $product[$product_index]['bar_code'] = $challan_product->product->bar_code;
                $product[$product_index]['item_code'] = $challan_product->product->item_code;
                $product[$product_index]['active'] = $challan_product->product->active;

                $combination_index = 0;
                if($challan_product->storeInCombinations){
                    foreach ($challan_product->storeInCombinations as $combination) {
                        $attribute_values = [];
                        $attribute_values_index = 0;
                        $combinations[$combination_index]['warehouse_store_in_challan_product_id'] = $combination->warehouse_store_in_challan_product_id;
                        $combinations[$combination_index]['product_combination_id'] = $combination->product_combination_id;
                        $combinations[$combination_index]['quantity'] = $combination->quantity;

                        $product_combinations = App\ProductCombination::with('combinationAttributeValues')->where('id', $combination->product_combination_id)->get();
                        if( $product_combinations ){
                            foreach ($product_combinations as $product_combination){
                                foreach ( $product_combination->combinationAttributeValues as $attribute_value){
                                    if($attribute_value->attributeValue){
                                        $attribute_values[$attribute_values_index]['name']  =  $attribute_value->attributeValue->attribute->name;
                                        $attribute_values[$attribute_values_index]['type']  =  $attribute_value->attributeValue->attribute->type;
                                        $attribute_values[$attribute_values_index]['value'] = $attribute_value->attributeValue->value;
                                        $attribute_values[$attribute_values_index]['color'] = $attribute_value->attributeValue->color;
                                        $attribute_values_index++;
                                    }
                                }
                            }
                            $combinations[$combination_index]['attribute_values'] = $attribute_values;
                            $product[$product_index]['combinations'] = $combinations;
                            $combination_index++;
                        }
                    }
                }
                $challan_info_return_data['challan_info']['products'] = $product;
                $product_index++;
            }
        }
    }
    dd($challan_info_return_data);
});


Route::get('cat', function(){
    $categories = App\Category::where('company_id', 1)->orderBy('id', 'ASC')->get();
    $dynaConfig = treeCategories( $categories, $index = 0, $parent_node = 0, $this_category = 0, $product_id = 0);
    $return_array['data'] = $dynaConfig;
    dd($return_array['data']);

});

function treeCategories($categories, $index, $parent_node = 0, $this_category = 0, $product_id = 0 )
{
    $tree_categories = [];
    foreach( $categories as $category) {
        $index++;
        $tree_category = [];
        $tree_category['title'] = $category->name;
        $tree_category['key'] = $category->id;
        if( $product_id ){
            $product_category = App\CategoryProduct::where('product_id', $product_id )->where('category_id', $category->id )->first();
            if( $product_category ){
                $tree_category['select'] = true;
            }
        }
        if( $parent_node == $category->id ) {
            $tree_category['select'] = true;
        }
        if( $this_category == $category->id || ( $this_category !=0 && $this_category == $category->parent_id )) {
            $tree_category['unselectable'] = true;
        }
        if(count($category->childs)){
            $tree_category['folder'] = true;
            if( $this_category != $category->id )
            {
                $tree_category['children'] = treeCategories( $category->childs, $index, $parent_node, $this_category, $product_id );
            }
        }else{
            if( $index == 1 ){
                $tree_category['folder'] = true; // Make default category as 'Folder'
            }
            else{
                $tree_category['folder'] = false;
            }
        }
        $tree_categories[] = $tree_category;
    }
    return $tree_categories;

}
Route::get('warehouse_store_in_challan/{id}/final', 'WarehouseStoreInChallanController@makefinal');

Route::get('warehouse_challan_in_pdf/{id}','WarehouseStoreInChallanController@warehouse_challan_in_pdf');
Route::get('warehouse_inventory','WarehouseInventoryController@index');
Route::get('warehouse_inventory_pdf','WarehouseInventoryController@warehouse_inventory_pdf');
Route::post('product/generate_barcode_pdf','ProductController@generateBarcodePdf');

Route::get('outlet_inventory','OutletInventoryController@index');
Route::get('invoice','InvoiceController@index');

//check token





