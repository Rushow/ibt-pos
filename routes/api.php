<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




Route::get('category/get_tree_data', 'CategoryApiController@getTreeData');//->middleware('auth:api');
Route::get('category/get_categories', 'CategoryApiController@getCategories');//->middleware('auth:api');
Route::get('supplier/get_suppliers', 'SupplierApiController@getSuppliers');
Route::get('warehouse/get_warehouses', 'WarehouseApiController@getWarehouses');
Route::get('outlets/get_outlet/{companyId}', 'OutletApiController@getOutlet');//->middleware('auth:api');
Route::post('product_combination/get_combination_data', 'ProductCombinationApiController@getCombinationData');//->middleware('auth:api');
Route::get('product/remove_image', 'ProductApiController@removeImage');
Route::get('module/get_modules', 'ModuleApiController@getModules');
Route::post('product/search_products', 'ProductApiController@searchProducts');
Route::post('warehouse_store_in_challan/save_warehouse_store_in_challan', 'WarehouseStoreInChallanApiController@saveWarehouseStoreInChallan');//->middleware('auth:api');
Route::post('warehouse_store_in_challan/update_warehouse_store_in_challan', 'WarehouseStoreInChallanApiController@updateWarehouseStoreInChallan');//->middleware('auth:api');
Route::post('warehouse_store_out_challan/save_warehouse_store_out_challan', 'WarehouseStoreOutChallanApiController@saveWarehouseStoreOutChallan');//->middleware('auth:api');
Route::post('warehouse_store_out_challan/update_warehouse_store_out_challan', 'WarehouseStoreOutChallanApiController@updateWarehouseStoreOutChallan');//->middleware('auth:api');


Route::post('challan_list/get_challan_data', 'WarehouseStoreInChallanApiController@getChallanData');//->middleware('auth:api');
Route::post('challan_list/get_challan_out_data', 'WarehouseStoreOutChallanApiController@getChallanOutData');//->middleware('auth:api');

Route::get('warehouse_store_out_challan/{id}/final', 'WarehouseStoreOutChallanApiController@makefinal');
Route::post('warehouse_store_out_challan/quantity_available', 'WarehouseStoreOutChallanApiController@checkIfQuantityAvailableAtWarehouse');//->middleware('auth:api');

Route::resource('customer', 'CustomerApiController');
Route::resource('login', 'LoginApiController');
Route::resource('dashboard', 'DashboardApiController');



//************************Api for andorid **********************************************************//

// API routes...
Route::prefix('v1')->group(function (){

    //log in and get token
    Route::get('login', 'Api\UserController@login');
    Route::post('login', 'Api\UserController@login');
    // get product info
    Route::get('products/{id?}', 'Api\ProductInfoApiController@index')->middleware('auth:api');
    Route::get('product_combinations/{id?}', 'Api\ProductInfoApiController@productCombinations')->middleware('auth:api');
    //param barcode
    Route::get('product_barcode', 'Api\ProductInfoApiController@productBarcode')->middleware('auth:api');
    Route::get('categories/{id?}', 'Api\CategoryInfoApiController@index')->middleware('auth:api');

    //get list of task, pending task & post to make status synced
    Route::get('getTaskType/', 'Api\TaskApiController@getTaskType')->middleware('auth:api');
    Route::get('getTask/', 'Api\TaskApiController@index')->middleware('auth:api');//pass outlet_id
    Route::post('taskStatusSyncd', 'Api\TaskApiController@taskStatusSyncd')->middleware('auth:api');//post task id

    //getChallan info
    Route::get('getChallanforOutlet/{id?}', 'Api\ChallanApiController@index')->middleware('auth:api');
    //Route::post('challanRecievedByOutlet/{id?}', 'Api\ChallanApiController@store')->middleware('auth:api');
    
    //get invoice info
    Route::get('getInvoice/{id?}', 'Api\InvoiceApiController@index')->middleware('auth:api');
    Route::post('postInvoice', 'Api\InvoiceApiController@postInvoice')->middleware('auth:api');
    Route::post('postInvoiceProduct', 'Api\InvoiceApiController@postInvoiceProduct')->middleware('auth:api');
    
    //discount rules api
    // if id not given all rules will be return
    // param to pass outlet_id
    Route::get('getDiscountRules/{id?}', 'Api\DiscountRuleApiController@getDiscountRules')->middleware('auth:api');
    Route::post('validateCoupon/', 'Api\DiscountRuleApiController@validateCoupon')->middleware('auth:api');//param code
    
    /*
     Inventory Apis
    */
    Route::get('outlet/{id}/inventory/challans', 'Api\InventoryApiController@getInventoryChallans')->middleware('auth:api');
    Route::get('outlet/{id}/inventory/products', 'Api\InventoryApiController@getInventoryProducts')->middleware('auth:api');
    Route::post('outlet/{id}/inventory/search/', 'Api\InventoryApiController@searchInventory')->middleware('auth:api');

    /*
     Sales Report Apis
    */
    Route::get('sales_report/stores/{id}', 'Api\SalesApiController@salesByDateStore')->middleware('auth:api');
    Route::get('sales_report/stores/{id}/details', 'Api\SalesApiController@dailySalesReportStore')->middleware('auth:api');
//    Route::post('make_sale', 'Api\SalesApiController@makeSale')->middleware('auth:api');
});

